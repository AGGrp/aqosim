# Author: Kelsey C. Justis
# Date: 08/21/2015
# Summary: Program creates a sparse Hp matrix which has the number of clauses each assignment did not satisfy along the diagonal
# such that Hp(i,i) = number of clauses not satisfied by assignment,i.
# Performs pruning of particles appearing in only one clause or appears with the same state in all of its involved clauses.
# Input: 
#-n: integer number of particles
#-m: integer number of clauses
# Output:
#-Sparse Hp

import random
import numpy as np
import scipy.sparse as sp
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from collections import Counter
# Create clauses to test.
def clauseListGen(clauseList, numParts):

     # Generate required number of clauses.
     for i in range(len(clauseList)):
          goodClause = 0

          # Generate a clause without redundancy/contradicting variables.
          while (goodClause == 0):
              clauseList[i] = ([((-1)**random.randint(0,1))*(random.randint(1,numParts)),\
                            ((-1)**random.randint(0,1))*(random.randint(1,numParts))])

              # Check if clause has duplicate state assignments to same particle .
              if (abs(clauseList[i][0])!=abs(clauseList[i][1])):

                 # Make sure particles are ordered in clause by number.
                 if (abs(clauseList[i][0]) > abs(clauseList[i][1])):
                     temp = clauseList[i][0]
                     clauseList[i][0] = clauseList[i][1]
                     clauseList[i][1] = temp

                 # Make sure clause is not already in list of clauses.
                 if ((clauseList[i] not in clauseList[0:i])):
                    goodClause = 1 # Clause makes sense.
     return clauseList
# Prune Clause List.
def pruneClauseList(clauseList,numClauses,numParticles):

    # Build clause matrix from clause list.
    clauseMatrix = np.zeros((numClauses,numParticles),dtype = int)
    for clauseNum in range(numClauses):
        var1 = clauseList[clauseNum][0]
        var2 = clauseList[clauseNum][1]
        clauseMatrix[clauseNum,abs(var1)-1] = var1
        clauseMatrix[clauseNum,abs(var2)-1] = var2

    # Prune matrix if necessary
    donePruning = False
    initialClauseMatrix = clauseMatrix

    # Keep pruning until we can't prune anymore.
    while (donePruning == False):

        currParticle= 0
        prunedClauseMatrix = initialClauseMatrix

        # While there are still multiple particle columns in matrix and we are not in the last particle column, and there are multiple clauses.
        while ((prunedClauseMatrix.shape[1]!=0) and (currParticle < prunedClauseMatrix.shape[1])and (prunedClauseMatrix.shape[0]!=1)):

            # The frequency of a particle number regardless of state.
            partFreq = sum(abs(np.sign(prunedClauseMatrix[:,currParticle])))

            # The frequency of a particle with state accounted for.
            partFreqWithState = sum(np.sign(prunedClauseMatrix[:,currParticle]))

            # If the particle appears in only one clause.
            if (partFreq == 1):

                # Find the clause it appears in.
                clauseRow = np.where(abs(prunedClauseMatrix[:,currParticle])!=0)[0][0]

                # Delete that clause.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,clauseRow, axis=0)

                # Delete the column for that particle number.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)

            # The current particle is not present in any clause.
            elif (partFreq == 0):

                # Delete the column for that particle number.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)

            # If the particle appear more than once and always the same state.
            elif (partFreq == abs(partFreqWithState) != 0):

                    # Find the clauses it appears in.
                    clauseRows = np.where(abs(prunedClauseMatrix[:,currParticle])!=0)[0]


                    # Delete those clauses.
                    prunedClauseMatrix = np.delete(prunedClauseMatrix,clauseRows, axis=0)

                    # Delete the column for that particle number.
                    prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)


            # Go on to the next particle in the matrix.
            currParticle +=1

        # Check to see if anything has been pruned off, if not we must be finished, if so check to see if can prune anymore.
        if (initialClauseMatrix.size == prunedClauseMatrix.size):
            donePruning = True
        else:
            initialClauseMatrix = prunedClauseMatrix

    numClauses = prunedClauseMatrix.shape[0]
    if (numClauses==1):
        prunedClauseMatrix = np.delete(prunedClauseMatrix,0, axis=0)
        numParts = 0
        numClauses  =0
    else:
        prunedClauseMatrix = prunedClauseMatrix[np.nonzero(prunedClauseMatrix)]
        prunedClauseMatrix = np.reshape(prunedClauseMatrix,(numClauses,2))
        numParts = len(np.unique(abs(prunedClauseMatrix)))
        # Reformat for use with rest of code.
        prunedClauseMatrix = np.hstack([prunedClauseMatrix,np.zeros((numClauses,1),dtype = int)])
        for clauseNum in range(numClauses):
            var1 = prunedClauseMatrix[clauseNum][0]
            var2 = prunedClauseMatrix[clauseNum][1]
            var1State = np.sign(var1)
            var2State = np.sign(var2)

            if (var1State == 1):
                if (var2State == 1):
                    prunedClauseMatrix[clauseNum][2] = 1
                else:
                    prunedClauseMatrix[clauseNum][1] = var2State*var2
                    prunedClauseMatrix[clauseNum][2] = 2
            else:
                prunedClauseMatrix[clauseNum][0] = var1State*var1
                if (var2State == 1):
                    prunedClauseMatrix[clauseNum][2] = 3
                else:
                    prunedClauseMatrix[clauseNum][1] = var2State*var2
                    prunedClauseMatrix[clauseNum][2] = 4

    return  numClauses,numParts#, prunedClauseMatrix,

def charKernel(numInstances):
    particleNumList = [200]#particleNumList = [10,50,100,200,500,1000]
    # Clear current figures.
    plt.cla()
    plt.close("all")
    plt.hold(1)
    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(len(particleNumList))
    ys = [i+x+(i*x)**2 for i in range(len(particleNumList))]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))

    # For each particle number up to n.
    for particleNum in particleNumList:
 
        # Get new pretty color for this number of particles.
        newColor = next(colors)

        # Maximum number of unique clauses depending on n.
        mMax = 5*particleNum
        if (mMax < 10):
            clauseNumList = np.linspace(1,mMax,10,dtype = int)
        else:
            clauseNumList = np.linspace(1,mMax,30,dtype = int)
        # For each clause up to mMax.
        for numClauses in clauseNumList:
            alpha  = float(float(numClauses)/float(particleNum)) # Alpha
            newMs = []
            newNs = []
            newAlphas = []
            avgNewM = 0.
            avgNewAlpha = 0.
            avgNewN = 0.
            for sample in range(numInstances):
                clauseList = clauseListGen([0]*numClauses, particleNum)
                newM,newN = pruneClauseList(clauseList,numClauses,particleNum)
                if (newN == 0):
                    newM = 0.
                    newAlpha = 0.
                else:
                    newAlpha = float(newM)/newN
                newMs = np.hstack([newMs,newM])
                newNs = np.hstack([newNs,newN])
                newAlphas = np.hstack([newAlphas,newAlpha])
                avgNewM += newM
                avgNewN += newN
                avgNewAlpha += newAlpha
            avgNewM = avgNewM/numInstances
            avgNewN = avgNewN/numInstances
            newMsSTD = np.std(newMs)/(numInstances**(1/2))
            newNsSTD = np.std(newNs)/(numInstances**(1/2))
            avgNewAlpha = avgNewAlpha/numInstances
            newAlphaSTD = np.std(newAlphas)/(numInstances**(1/2))
            print '\n#Particles = ' + str(particleNum) + '\n#Clauses = ' + str(numClauses)+\
                  '\nAvg New m = '+str(avgNewM) +' with STD ' + str(newMsSTD)+\
                  '\nAvg New n = '+str(avgNewM) +' with STD ' + str(newNsSTD)+\
                  '\nAvg New alpha = '+str(avgNewAlpha) +' with STD ' + str(newAlphaSTD)

            if (numClauses==1):
                # All
                figNum = 1
                plt.figure(figNum)
                plt.scatter(alpha, avgNewM, color=newColor,label = str(particleNum)+' Particles')
                plt.errorbar(alpha,avgNewM,yerr = newMsSTD, color=newColor, fmt ='o') # Plot variances.
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, avgNewAlpha, color=newColor,label = str(particleNum)+' Particles')
                plt.errorbar(alpha,avgNewAlpha,yerr = newAlphaSTD, color=newColor, fmt ='o') # Plot variances.
                figNum +=1

                plt.figure(figNum)
                plt.scatter(numClauses, avgNewN, color=newColor,label = str(particleNum)+' Particles')
                plt.errorbar(numClauses,avgNewN,yerr = newNsSTD, color=newColor, fmt ='o') # Plot variances.
                figNum +=1

                plt.figure(figNum)
                plt.scatter(numClauses, avgNewM, color=newColor,label = str(particleNum)+' Particles')
                plt.errorbar(numClauses,avgNewM,yerr = newMsSTD, color=newColor, fmt ='o') # Plot variances.
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, avgNewM/numClauses, color=newColor,label = str(particleNum)+' Particles')
                plt.errorbar(alpha,avgNewM/numClauses,yerr = newMsSTD/numClauses, color=newColor, fmt ='o') # Plot variances.




            else:
                figNum = 1
                plt.figure(figNum)
                plt.scatter(alpha, avgNewM, color=newColor)
                plt.errorbar(alpha,avgNewM,yerr = newMsSTD, color=newColor, fmt ='o') # Plot variances.
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, avgNewAlpha, color=newColor)
                plt.errorbar(alpha,avgNewAlpha,yerr = newAlphaSTD, color=newColor, fmt ='o') # Plot variances.
                figNum +=1

                plt.figure(figNum)
                plt.scatter(numClauses, avgNewN, color=newColor)
                plt.errorbar(numClauses,avgNewN,yerr = newNsSTD, color=newColor, fmt ='o') # Plot variances.
                figNum +=1

                plt.figure(figNum)
                plt.scatter(numClauses, avgNewM, color=newColor)
                plt.errorbar(numClauses,avgNewM,yerr = newMsSTD, color=newColor, fmt ='o') # Plot variances.
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, avgNewM/numClauses, color=newColor)
                plt.errorbar(alpha,avgNewM/numClauses,yerr = newMsSTD/numClauses, color=newColor, fmt ='o') # Plot variances.

    # All
    figNum = 1
    plt.figure(figNum)
    plt.title('Average(out of '+str(numInstances)+') Kernel Size vs. Old Alpha')
    plt.xlabel('Old Alpha (From Entered n and m)')
    plt.ylabel('Kernel Size(New#Clauses')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    plt.figure(figNum)
    plt.title('Average(out of '+str(numInstances)+') New Alpha vs. Old Alpha')
    plt.xlabel('Old Alpha(Entered n and m)')
    plt.ylabel('Average New Alpha(Pruned n and pruned m')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    plt.figure(figNum)
    plt.title('Average(out of '+str(numInstances)+') New n vs. Old m')
    plt.xlabel('Old m')
    plt.ylabel('New n')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    plt.figure(figNum)
    plt.title('Average(out of '+str(numInstances)+') New m vs. Old m')
    plt.xlabel('Old m')
    plt.ylabel('New m')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum += 1

    plt.figure(figNum)
    plt.title('Average(out of '+str(numInstances)+') New m ratio over old m vs. Old alpha')
    plt.xlabel('Old alpha')
    plt.ylabel('New m/old m')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    # Show all plots.
    plt.show()

numInstances =  100
charKernel(numInstances)
