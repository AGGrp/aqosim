# Author: Kelsey C. Justis
# Date: 08/21/2015
# Summary: Program creates a sparse Hp matrix which has the number of clauses each assignment did not satisfy along the diagonal
# such that Hp(i,i) = number of clauses not satisfied by assignment,i.
# Performs pruning of particles appearing in only one clause or appears with the same state in all of its involved clauses.
# Input: 
#-n: integer number of particles
#-m: integer number of clauses
# Output:
#-Sparse Hp

import random
import numpy as np
import scipy.sparse as sp
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D

# Create clauses to test.
def clauseListGen(clauseList, numParts):

     # Generate required number of clauses.
     for i in range(len(clauseList)):
          goodClause = 0

          # Generate a clause without redundancy/contradicting variables.
          while (goodClause == 0):
              clauseList[i] = ([random.randint(1,numParts),random.randint(1,numParts),random.randint(1,4) ])

              # Check if clause has duplicate state assignments to same particle .
              if (clauseList[i][0] != clauseList[i][1]):

                 # Make sure particles are ordered in clause by number.
                 if (clauseList[i][0] > clauseList[i][1]):
                     temp = clauseList[i][0]
                     clauseList[i][0] = clauseList[i][1]
                     clauseList[i][1] = temp

                 # Make sure clause is not already in list of clauses.
                 if ((clauseList[i] not in clauseList[0:i])):
                    goodClause = 1 # Clause makes sense.
     return clauseList
def pruneClauseList(clauseList,numClauses,numParticles):

    # Build clause matrix.
    clauseMatrix = np.zeros((numClauses,numParticles),dtype = int)
    for clauseNum in range(numClauses):
        var1 = clauseList[clauseNum][0]
        var2 = clauseList[clauseNum][1]

        clausePartState = clauseList[clauseNum][2]         # Get state of particle in clause.

        if (clausePartState == 1):
            clauseMatrix[clauseNum,var1-1] = var1
            clauseMatrix[clauseNum,var2-1] = var2
        elif (clausePartState == 2):
            clauseMatrix[clauseNum,var1-1] = var1
            clauseMatrix[clauseNum,var2-1] = -var2
        elif (clausePartState == 3):
            clauseMatrix[clauseNum,var1-1] = -var1
            clauseMatrix[clauseNum,var2-1] = var2
        elif (clausePartState == 4):
            clauseMatrix[clauseNum,var1-1] = -var1
            clauseMatrix[clauseNum,var2-1] = -var2

    # Prune matrix if necessary
    donePruning = False
    initialClauseMatrix = clauseMatrix

    # Keep pruning until we can't prune anymore.
    while (donePruning == False):

        currParticle= 0
        prunedClauseMatrix = initialClauseMatrix


        # While there are still multiple particle columns in matrix and we are not in the last particle column, and there are multiple clauses.
        while ((prunedClauseMatrix.shape[1]!=0) and (currParticle < prunedClauseMatrix.shape[1])and (prunedClauseMatrix.shape[0]!=1)):

            # The frequency of a particle number regardless of state.
            partFreq = sum(abs(np.sign(prunedClauseMatrix[:,currParticle])))

            # The frequency of a particle with state accounted for.
            partFreqWithState = sum(np.sign(prunedClauseMatrix[:,currParticle]))

            # If the particle appears in only one clause.
            if (partFreq == 1):

                # Find the clause it appears in.
                clauseRow = np.where(abs(prunedClauseMatrix[:,currParticle])!=0)[0][0]

                # Delete that clause.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,clauseRow, axis=0)

                # Delete the column for that particle number.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)

            # The current particle is not present in any clause.
            elif (partFreq == 0):

                # Delete the column for that particle number.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)

            # If the particle appear more than once and always the same state.
            elif (partFreq == abs(partFreqWithState) != 0):

                    # Find the clauses it appears in.
                    clauseRows = np.where(abs(prunedClauseMatrix[:,currParticle])!=0)[0]


                    # Delete those clauses.
                    prunedClauseMatrix = np.delete(prunedClauseMatrix,clauseRows, axis=0)

                    # Delete the column for that particle number.
                    prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)


            # Go on to the next particle in the matrix.
            currParticle +=1

        # Check to see if anything has been pruned off, if not we must be finished, if so check to see if can prune anymore.
        if (initialClauseMatrix.size == prunedClauseMatrix.size):
            donePruning = True
        else:
            initialClauseMatrix = prunedClauseMatrix

    numClauses = prunedClauseMatrix.shape[0]
    prunedClauseMatrix = prunedClauseMatrix[np.nonzero(prunedClauseMatrix)]
    prunedClauseMatrix = np.reshape(prunedClauseMatrix,(numClauses,2))

    # # Reformat for use with rest of code.
    # prunedClauseMatrix = np.hstack([prunedClauseMatrix,np.zeros((numClauses,1),dtype = int)])
    # for clauseNum in range(numClauses):
    #     var1 = prunedClauseMatrix[clauseNum][0]
    #     var2 = prunedClauseMatrix[clauseNum][1]
    #     var1State = np.sign(var1)
    #     var2State = np.sign(var2)
    #     clausePartState = clauseList[clauseNum][2]         # Get state of particle in clause.
    #
    #     if (var1State == 1):
    #         if (var2State == 1):
    #             prunedClauseMatrix[clauseNum][2] = 1
    #         else:
    #             prunedClauseMatrix[clauseNum][1] = var2State*var2
    #             prunedClauseMatrix[clauseNum][2] = 2
    #     else:
    #         prunedClauseMatrix[clauseNum][0] = var1State*var1
    #         if (var2State == 1):
    #             prunedClauseMatrix[clauseNum][2] = 3
    #         else:
    #             prunedClauseMatrix[clauseNum][1] = var2State*var2
    #             prunedClauseMatrix[clauseNum][2] = 4

    return  numClauses #prunedClauseMatrix,

def charKernal(numParticles,numInstances):

     # Clear current figures.
    plt.cla()
    plt.close("all")
    plt.hold(1)
    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(numParticles)
    ys = [i+x+(i*x)**2 for i in range(numParticles)]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))

    # For each particle number up to n.
    for particleNum in range(2,numParticles + 1):

        # Get new pretty color for this number of particles.
        newColor = next(colors)

        # Maximum number of unique clauses depending on n.
        mMax = 2*particleNum

        # For each clause up to mMax.
        for numClauses in range(1, mMax+1):
            alpha  = np.ones((numInstances,1))*float(float(numClauses)/float(particleNum)) # Alpha
            ms = numClauses*np.ones((numInstances,1))
            newMs = []
            avgNewM = 0.
            for sample in range(numInstances):
                clauseList = clauseListGen([0]*numClauses, particleNum)
                newM = pruneClauseList(clauseList,numClauses,particleNum)
                newMs = np.hstack([newMs,newM])
                avgNewM += newM
            print '\n#Particles = ' + str(particleNum) + '\n#Clauses = ' + str(numClauses)+'\nAvg New m = '+str(round(avgNewM/numInstances))
            msDiff = ms.transpose()-newMs
            newMsQuotient = newMs/ms.transpose()
            newAlphas = newMs/particleNum

            if (numClauses==1):
                # All
                figNum = 1
                plt.figure(figNum)
                plt.scatter(alpha, newMs, color=newColor,label = str(particleNum)+' Particles')
                # plt.plot(alpha, newMs, color=newColor)
                figNum +=1
                # plt.figure(2)
                # plt.scatter(ms, newMs, color=newColor,label = str(particleNum)+' Particles')
                # # plt.plot(ms, newMs, color=newColor)

                plt.figure(figNum)
                plt.scatter(alpha, msDiff, color=newColor,label = str(particleNum)+' Particles')
                # plt.plot(alpha, msDiff, color=newColor)
                figNum +=1
                # plt.figure(4)
                # plt.scatter(ms, msDiff, color=newColor,label = str(particleNum)+' Particles')
                # # plt.plot(ms, msDiff, color=newColor)

                plt.figure(figNum)
                plt.scatter(alpha, newMsQuotient, color=newColor,label = str(particleNum)+' Particles')
                # plt.plot(alpha, newMsQuotient, color=newColor)
                figNum +=1
                # plt.figure(6)
                # plt.scatter(ms, newMsQuotient, color=newColor,label = str(particleNum)+' Particles')
                # # plt.plot(ms, newMsQuotient, color=newColor)

                plt.figure(figNum)
                plt.scatter(alpha, newAlphas, color=newColor,label = str(particleNum)+' Particles')
                # plt.plot(alpha, newAlphas, color=newColor)
                figNum +=1
                # plt.figure(8)
                # plt.scatter(newAlphas, alpha, color=newColor,label = str(particleNum)+' Particles')
                # # plt.plot(newAlphas,alpha, color=newColor)


            else:
                # All
                figNum = 1
                plt.figure(figNum)
                plt.scatter(alpha, newMs, color=newColor)
                # plt.plot(alpha, newMs, color=newColor)
                figNum +=1
                # plt.figure(2)
                # plt.scatter(ms, newMs, color=newColor)
                # # plt.plot(ms, newMs, color=newColor)

                plt.figure(figNum)
                plt.scatter(alpha, msDiff, color=newColor)
                # plt.plot(alpha, msDiff, color=newColor)
                figNum +=1
                # plt.figure(4)
                # plt.scatter(ms, msDiff, color=newColor)
                # # plt.plot(ms, msDiff, color=newColor)

                plt.figure(figNum)
                plt.scatter(alpha, newMsQuotient, color=newColor)
                # plt.plot(alpha, newMsQuotient, color=newColor)
                figNum +=1
                # plt.figure(6)
                # plt.scatter(ms, newMsQuotient, color=newColor)
                # # plt.plot(ms, newMsQuotient, color=newColor)

                plt.figure(figNum)
                plt.scatter(alpha, newAlphas, color=newColor)
                # plt.plot(alpha, newAlphas, color=newColor)
                figNum +=1
                # plt.figure(8)
                # plt.scatter(newAlphas, alpha, color=newColor)
                # # plt.plot(newAlphas,alpha, color=newColor)


    # All
    figNum = 1
    plt.figure(figNum)
    plt.title('BACK:Kernal Size vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernal Size(New#Clauses')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    # plt.figure(2)
    # plt.title('Kernal Size vs. m:# of Clauses')
    # plt.xlabel('Entered # Clauses ')
    # plt.ylabel('Kernal Size(New#Clauses)')
    # plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    plt.figure(figNum)
    plt.title('BACK:Kernal Size Difference vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernal Size Difference(Old size - New Size)')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    # plt.figure(4)
    # plt.title('Kernal Size Difference vs. m:# of Clauses')
    # plt.xlabel('Entered # Clauses ')
    # plt.ylabel('Kernal Size Difference(Old size - New Size)')
    # plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    plt.figure(figNum)
    plt.title('BACK:Kernal Size Quotient vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernal Size Quotient(New size/Old Size)')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    # plt.figure(6)
    # plt.title('Kernal Size Quotient vs. m:# of Clauses')
    # plt.xlabel('Entered # Clauses ')
    # plt.ylabel('Kernal Size Quotient(New size/Old Size)')
    # plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    plt.figure(figNum)
    plt.title('BACK:New Alpha vs. Old Alpha')
    plt.xlabel('Old Alpha(Entered n and m)')
    plt.ylabel('New Alpha(Entered n and pruned m')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1
    # plt.figure(8)
    # plt.title('Old Alpha vs. new Alpha')
    # plt.xlabel('New Alpha(Entered n and pruned m')
    # plt.ylabel('Old Alpha(Entered n and m)')
    # plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)


    # Show all plots.
    plt.show()

# Get from user the number of particles, clauses, and random assignments to try.
numParticles = input('Enter number of particles/spins in the system: \n')
numInstances =  input('Enter number of instances to try: \n')
charKernal(numParticles,numInstances)
