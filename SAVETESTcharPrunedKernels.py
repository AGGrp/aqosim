# Author: Kelsey C. Justis
# Date: 08/28/2015
# Summary: Program creates a sparse Hp matrix which has the number of clauses each assignment did not satisfy along the diagonal
# such that Hp(i,i) = number of clauses not satisfied by assignment,i.
# Performs pruning of particles appearing in only one clause or appears with the same state in all of its involved clauses.
# Input: 
#-n: integer number of particles
#-m: integer number of clauses
# Output:
#-Sparse Hp

import random
import numpy as np
import scipy.sparse as sp
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import datetime
import os

# Create clauses to test.
def clauseListGen(clauseList, numParts):

     # Generate required number of clauses.
     for i in range(len(clauseList)):
          goodClause = 0

          # Generate a clause without redundancy/contradicting variables.
          while (goodClause == 0):
              clauseList[i] = ([((-1)**random.randint(0,1))*(random.randint(1,numParts)),\
                            ((-1)**random.randint(0,1))*(random.randint(1,numParts))])

              # Check if clause has duplicate state assignments to same particle .
              if (abs(clauseList[i][0])!=abs(clauseList[i][1])):

                 # Make sure particles are ordered in clause by number.
                 if (abs(clauseList[i][0]) > abs(clauseList[i][1])):
                     temp = clauseList[i][0]
                     clauseList[i][0] = clauseList[i][1]
                     clauseList[i][1] = temp

                 # Make sure clause is not already in list of clauses.
                 if ((clauseList[i] not in clauseList[0:i])):
                    goodClause = 1 # Clause makes sense.
     return clauseList
# Prune Clause List.
def pruneClauseList(clauseList,numClauses,numParticles):

    # Build clause matrix from clause list.
    clauseMatrix = np.zeros((numClauses,numParticles),dtype = int)
    for clauseNum in range(numClauses):
        var1 = clauseList[clauseNum][0]
        var2 = clauseList[clauseNum][1]
        clauseMatrix[clauseNum,abs(var1)-1] = var1
        clauseMatrix[clauseNum,abs(var2)-1] = var2

    # Prune matrix if necessary
    donePruning = False
    initialClauseMatrix = clauseMatrix

    # Keep pruning until we can't prune anymore.
    while (donePruning == False):

        currParticle= 0
        prunedClauseMatrix = initialClauseMatrix

        # While there are still multiple particle columns in matrix and we are not in the last particle column, and there are multiple clauses.
        while ((prunedClauseMatrix.shape[1]!=0) and (currParticle < prunedClauseMatrix.shape[1])and (prunedClauseMatrix.shape[0]!=1)):

            # The frequency of a particle number regardless of state.
            partFreq = sum(abs(np.sign(prunedClauseMatrix[:,currParticle])))

            # The frequency of a particle with state accounted for.
            partFreqWithState = sum(np.sign(prunedClauseMatrix[:,currParticle]))

            # If the particle appears in only one clause.
            if (partFreq == 1):

                # Find the clause it appears in.
                clauseRow = np.where(abs(prunedClauseMatrix[:,currParticle])!=0)[0][0]

                # Delete that clause.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,clauseRow, axis=0)

                # Delete the column for that particle number.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)

            # The current particle is not present in any clause.
            elif (partFreq == 0):

                # Delete the column for that particle number.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)

            # If the particle appear more than once and always the same state.
            elif (partFreq == abs(partFreqWithState) != 0):

                    # Find the clauses it appears in.
                    clauseRows = np.where(abs(prunedClauseMatrix[:,currParticle])!=0)[0]


                    # Delete those clauses.
                    prunedClauseMatrix = np.delete(prunedClauseMatrix,clauseRows, axis=0)

                    # Delete the column for that particle number.
                    prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)


            # Go on to the next particle in the matrix.
            currParticle +=1

        # Check to see if anything has been pruned off, if not we must be finished, if so check to see if can prune anymore.
        if (initialClauseMatrix.size == prunedClauseMatrix.size):
            donePruning = True
        else:
            initialClauseMatrix = prunedClauseMatrix

    numClauses = prunedClauseMatrix.shape[0]
    if (numClauses==1):
        prunedClauseMatrix = [[]]#np.delete(prunedClauseMatrix,0, axis=0)
        numParts = 0
        numClauses  =0
    else:
        prunedClauseMatrix = prunedClauseMatrix[np.nonzero(prunedClauseMatrix)]
        prunedClauseMatrix = np.reshape(prunedClauseMatrix,(numClauses,2))
        numParts = len(np.unique(abs(prunedClauseMatrix)))
    return  prunedClauseMatrix, numClauses,numParts

def prepClauseList(clauseList,numClauses):
    clauseList = np.hstack([clauseList,np.zeros((numClauses,1),dtype = int)])
    for clauseNum in range(numClauses):
        var1 = clauseList[clauseNum][0]
        var2 = clauseList[clauseNum][1]
        var1State = np.sign(var1)
        var2State = np.sign(var2)

        if (var1State == 1):
            if (var2State == 1):
                clauseList[clauseNum][2] = 1
            else:
                clauseList[clauseNum][1] = var2State*var2
                clauseList[clauseNum][2] = 2
        else:
            clauseList[clauseNum][0] = var1State*var1
            if (var2State == 1):
                clauseList[clauseNum][2] = 3
            else:
                clauseList[clauseNum][1] = var2State*var2
                clauseList[clauseNum][2] = 4
    return clauseList

def charKernel(numInstances):
    particleNumList = [18,20,22]
    numVals = 50 # Number of alpha values in alpha range of given n to plot.

    # For each particle number up to n.
    for particleNum in particleNumList:

        # Maximum number of unique clauses depending on n.
        mMax = 2*particleNum

        # Make sure we check clauses spread out over range of possible alpha between 0 and 2 for the current particle number value, n.
        if (mMax <= numVals):
            clauseNumList = np.linspace(1,mMax,mMax,dtype = int)
        else:
            clauseNumList = np.linspace(1,mMax,numVals,dtype = int)

        # For each clause up to mMax.
        for numClauses in clauseNumList:

            # Initialize variables to hold values found.
            # Original info.
            alpha  = float(float(numClauses)/float(particleNum))

            # Pruned info.
            newMs = []
            newNs = []
            newAlphas = []
            avgNewM = 0.
            avgNewN = 0.
            avgNewAlpha = 0.

            # Generate the number of instances desired for stats requested.
            for sample in range(numInstances):

                # Original data.
                clauseList= clauseListGen([0]*numClauses, particleNum)

                # Pruned data.
                prunedClauseList,newM,newN = pruneClauseList(clauseList,numClauses,particleNum)

                # Original data.
                clauseList= prepClauseList(clauseList,numClauses)
                HpList, oldNumUnSatClauses, oldBestAssgs = makeHp(particleNum,numClauses,clauseList)
                oldNumSols = len(oldBestAssgs)

                # If no empty kernel.
                if (newN == 0):
                    newM = 0
                    newAlpha = 0.
                    prunedHpList = []
                    newNumSols= 0
                    newNumUnSatClauses = 0
                    newBestAssgs = []

                # Nonempty kernel
                else:
                    newAlpha = float(newM)/newN
                    prunedClauseList = prepClauseList(prunedClauseList,newM)
                    prunedHpList, newNumUnSatClauses, newBestAssgs = makePrunedHp(particleNum,newM,prunedClauseList)
                    newNumSols = len(newBestAssgs)

                # Append values found.
                # Pruned data.
                newMs = np.hstack([newMs,newM])
                newNs = np.hstack([newNs,newN])
                newAlphas = np.hstack([newAlphas,newAlpha])

                # Where to save results.
                path  = 'PrunedHpInstances/n'+str(particleNum)+ '/m'+str(numClauses)
                try:
                    np.savez(path + '/i'+str(sample),
                                                     clauseList=clauseList,
                                                     prunedClauseList=prunedClauseList,
                                                     HpList=HpList,
                                                     prunedHpList=prunedHpList,
                                                     oldNumSols=oldNumSols,
                                                     newNumSols=newNumSols,
                                                     newNumUnSatClauses = newNumUnSatClauses,
                                                     oldNumUnSatClauses = oldNumUnSatClauses,
                                                     oldBestAssgs=oldBestAssgs,
                                                     newBestAssgs=newBestAssgs)
                except IOError:
                    os.makedirs(str(path))
                    np.savez(path + '/i'+str(sample),
                                                     clauseList=clauseList,
                                                     prunedClauseList=prunedClauseList,
                                                     HpList=HpList,
                                                     prunedHpList=prunedHpList,
                                                     oldNumSols=oldNumSols,
                                                     newNumSols=newNumSols,
                                                     newNumUnSatClauses = newNumUnSatClauses,
                                                     oldNumUnSatClauses = oldNumUnSatClauses,
                                                     oldBestAssgs=oldBestAssgs,
                                                     newBestAssgs=newBestAssgs)
                except OSError:
                    os.makedirs(str(path))
                    np.savez(path + '/i'+str(sample),
                                                     clauseList=clauseList,
                                                     prunedClauseList=prunedClauseList,
                                                     HpList=HpList,
                                                     prunedHpList=prunedHpList,
                                                     oldNumSols=oldNumSols,
                                                     newNumSols=newNumSols,
                                                     newNumUnSatClauses = newNumUnSatClauses,
                                                     oldNumUnSatClauses = oldNumUnSatClauses,
                                                     oldBestAssgs=oldBestAssgs,
                                                     newBestAssgs=newBestAssgs)

                # Sum up values found.
                avgNewM += newM
                avgNewN += newN
                avgNewAlpha += newAlpha

            # Get averages of values found.
            avgNewM = avgNewM/numInstances
            avgNewN = avgNewN/numInstances
            avgNewAlpha = avgNewAlpha/numInstances

            # Get standard deviation scaled by number of samples/instances for values found.
            newMsSTD = np.std(newMs)/(numInstances**(1/2))
            newNsSTD = np.std(newNs)/(numInstances**(1/2))
            newAlphaSTD = np.std(newAlphas)/(numInstances**(1/2))

            # Where to save results.
            path  = 'PrunedHpInstances/n'+str(particleNum)
            try:
                np.savez(path + '/m'+str(numClauses)+'avgData',
                                                     alpha=alpha,
                                                     newMs=newMs,
                                                     newNs=newNs,
                                                     newAlphas=newAlphas,
                                                     avgNewM=avgNewM,
                                                     avgNewN=avgNewN,
                                                     avgNewAlpha=avgNewAlpha,
                                                     newMsSTD=newMsSTD,
                                                     newNsSTD=newNsSTD,
                                                     newAlphaSTD=newAlphaSTD)

            except IOError:
                os.makedirs(str(path))
                np.savez(path + '/m'+str(numClauses)+'avgData',
                                                     alpha=alpha,
                                                     newMs=newMs,
                                                     newNs=newNs,
                                                     newAlphas=newAlphas,
                                                     avgNewM=avgNewM,
                                                     avgNewN=avgNewN,
                                                     avgNewAlpha=avgNewAlpha,
                                                     newMsSTD=newMsSTD,
                                                     newNsSTD=newNsSTD,
                                                     newAlphaSTD=newAlphaSTD)
            except OSError:
                os.makedirs(str(path))
                np.savez(path + '/m'+str(numClauses)+'avgData',
                                                     alpha=alpha,
                                                     newMs=newMs,
                                                     newNs=newNs,
                                                     newAlphas=newAlphas,
                                                     avgNewM=avgNewM,
                                                     avgNewN=avgNewN,
                                                     avgNewAlpha=avgNewAlpha,
                                                     newMsSTD=newMsSTD,
                                                     newNsSTD=newNsSTD,
                                                     newAlphaSTD=newAlphaSTD)
# Practice loading data stored.
def loadCharKernel(numInstances):
    particleNumList = [18,20,22]
    numVals = 50 # Number of alpha values in alpha range of given n to plot.


    numParticles = len(particleNumList)
    # Clear current figures.
    plt.cla()
    plt.close("all")
    plt.hold(1)
    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(numParticles)
    ys = [i+x+(i*x)**2 for i in range(numParticles)]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))

    # For each particle number up to n.
    for particleNum in particleNumList:

        # Get new pretty color for this number of particles.
        newColor = next(colors)
        # Maximum number of unique clauses depending on n.
        mMax = 2*particleNum

        # Make sure we check clauses spread out over range of possible alpha between 0 and 2 for the current particle number value, n.
        if (mMax <= numVals):
            clauseNumList = np.linspace(1,mMax,mMax,dtype = int)
        else:
            clauseNumList = np.linspace(1,mMax,numVals,dtype = int)

        # For each clause up to mMax.
        for numClauses in clauseNumList:

            # Load average instance results.
            # Where to load results from.
            path  = 'PrunedHpInstances/n'+str(particleNum)
            data = np.load(path + '/m'+str(numClauses)+'avgData.npz') # Get data from this location.

            # Print current n and m to console.
            print '\n\n#Particles = ' + str(particleNum) + '\n#Clauses = ' + str(numClauses)+'\n'

            # Print all the average data for these instances.
            for fileName in data.files:
                print str(fileName) +':\n' + str(data[str(fileName)])

            # Store the current alpha value.
            alpha  = float(float(numClauses)/float(particleNum))

            # Each sample/instance.
            for sample in range(numInstances):

                # Where to load results from.
                path  = path + '/m'+str(numClauses)

                data = np.load(path + '/i'+str(sample)+'.npz')# Get data from this location.

                # Print current n, m, and instance number to console.
                print '\n\n#Particles = ' + str(particleNum) + '\n#Clauses = ' + str(numClauses)+'\nSample# = ' + str(sample)+'\n'

                # Print desired information/attributs from stored data.
                for fileName in data.files:
                    # print str(fileName) +':\n' + str(data[str(fileName)])

                    if (str(fileName) == 'newNumUnSatClauses'):
                        newNumUnSatClauses = data[str(fileName)]
                        print 'newNumUnSatClauses: '+ str(newNumUnSatClauses)

                        if (numClauses==1 and sample==1):
                            plt.figure(1)
                            plt.scatter(alpha, newNumUnSatClauses, color=newColor,marker = 'o',label = str(particleNum)+' Particles')
                        else:
                            plt.figure(1)
                            plt.scatter(alpha, newNumUnSatClauses, color=newColor,marker = 'o')

                    elif (str(fileName) == 'oldNumUnSatClauses'):
                        oldNumUnSatClauses = data[str(fileName)]
                        print 'oldNumUnSatClauses: '+ str(oldNumUnSatClauses)
                        if (numClauses==1 and sample==1):
                            plt.figure(1)
                            plt.scatter(alpha, oldNumUnSatClauses, color=newColor,marker = '^',label = str(particleNum)+' Particles')
                        else:
                            plt.figure(1)
                            plt.scatter(alpha, oldNumUnSatClauses, color=newColor,marker = '^')

                    elif (str(fileName) == 'oldNumSols'):
                        oldNumSols = data[str(fileName)]
                        print 'oldNumSols: '+ str(oldNumSols)
                        if (numClauses==1 and sample==1):
                            plt.figure(2)
                            plt.scatter(alpha, oldNumSols, color=newColor,marker = '^',label = str(particleNum)+' Particles')
                        else:
                            plt.figure(2)
                            plt.scatter(alpha, oldNumSols, color=newColor,marker = '^')

                    elif (str(fileName) == 'newNumSols'):
                        newNumSols = data[str(fileName)]
                        print 'newNumSols: '+ str(newNumSols)
                        if (numClauses==1 and sample==1):
                            plt.figure(2)
                            plt.scatter(alpha, newNumSols, color=newColor,marker = 'o',label = str(particleNum)+' Particles')
                        else:
                            plt.figure(2)
                            plt.scatter(alpha,newNumSols, color=newColor,marker = 'o')


                    elif (str(fileName) == 'oldBestAssgs'):
                        oldBestAssgs = data[str(fileName)]
                        print 'oldBestAssgs:\n'+ str(oldBestAssgs)
                    elif (str(fileName) == 'newBestAssgs'):
                        newBestAssgs = data[str(fileName)]
                        print 'newBestAssgs:\n'+ str(newBestAssgs)
                    elif (str(fileName) == 'clauseList'):
                        clauseList = data[str(fileName)]
                        print 'clauseList:\n'+str(clauseList)
                    elif (str(fileName) == 'prunedClauseList'):
                        prunedClauseList = data[str(fileName)]
                        print 'prunedClauseList:\n'+str(prunedClauseList)

    figNum = 1
    plt.figure(figNum)
    plt.title('Energy vs. alpha')
    plt.xlabel('alpha (oldm/oldn')
    plt.ylabel('Number of clauses not satisfied')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    figNum +=1
    plt.figure(figNum)
    plt.title('#Sols/degenracy vs. alpha')
    plt.xlabel('alpha (oldm/oldn')
    plt.ylabel('Number of "best" solutions')
    plt.legend()
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    plt.show()

# Check to see how many clauses each assignment satisfies.
def makeHp(numParticles,numClauses,clauseList):

    # Initialize vector for diagonal of Hp with each having no clauses satisfied.
    hp = [numClauses]*(2**numParticles)
    bestAssgs = [] #List to hold best assignments for given 2SAT problem instance.
    minNumClauseNotSat = numClauses

    # For each random assignment to test.
    for assgNum in range(2**numParticles):
        assg = bin(assgNum)[2:].zfill(numParticles) # Generate a new random assignment.

        # Test assignment against each clause.
        for clauseNum in range(numClauses):
            clauseSat = 0
            clausePartState = clauseList[clauseNum][2]         # Get state of particle in clause.

            if (clausePartState == 1):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 2):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1
            elif (clausePartState == 3):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 4):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1

            # Put number of clauses unsatisfied by assignment, i, on Hp diagonal entry, i.
            # If assignment satisfied this clause decrease the count for how many clauses it does not satisfy.
            hp[assgNum] -= clauseSat

        # Keep record of assignment(s) satisfying the most clauses.
        # Number of clauses satisfied has been seen before.
        if (hp[assgNum]  == minNumClauseNotSat):
            bestAssgs = np.hstack([bestAssgs, assgNum + 1])
        # New "low score".
        elif (hp[assgNum]  < minNumClauseNotSat):
            bestAssgs = [(assgNum + 1)]
            minNumClauseNotSat = hp[assgNum]

    # Sparsify the Hp Matrix.
    hp = sp.diags(hp,0)
    return hp, minNumClauseNotSat, bestAssgs

# Make Hp using the pruned clause list.
def makePrunedHp(numParticles,numClauses,clauseList):
    # Initialize vector for diagonal of Hp with each having no clauses satisfied.
    hp = [numClauses]*(2**numParticles)
    bestAssgs = [] #List to hold best assignments for given 2SAT problem instance.
    minNumClauseNotSat = numClauses # Store minim umber of clauses not satisfied.
    var1=var2 =[] # Make arrays to store particles involved.

    # Get particle numbers involved.
    for clauseNum in range(numClauses):
        var1 = np.hstack([var1,clauseList[clauseNum][0]])
        var2 = np.hstack([var2,clauseList[clauseNum][1]])
    particlesInvolved = np.unique(abs(np.hstack([var1,var2])))

    # # Get smallest and biggest number of particles involved.
    # minP = int(min(particlesInvolved))
    # maxP = int(max(particlesInvolved))
    #
    # # Get range of asssignment numbers to look at corresponding to smallest and largest particle numbers.
    # maxAssgNum = 2**(numParticles-minP)
    # minAssgNum = 2**(numParticles-maxP)-1

    # For each random assignment to test.
    for assgNum in range(2**numParticles):
        assg = bin(assgNum)[2:].zfill(numParticles) # Generate a new random assignment.

        # For each clause in the pruned clause list.
        for clauseNum in range(numClauses):

            # Clause is not yet satisfied.
            clauseSat = 0
            clausePartState = clauseList[clauseNum][2]         # Get state of particle in clause.

            # Check to see if assignment satisfies the clause.
            if (clausePartState == 1):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 2):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1
            elif (clausePartState == 3):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 4):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1

            # If clause is satisfied make sure it is a valid assignment to consider given that particles are now pruned/set.
            if(clauseSat == 1):
                partNum = 0 # Start with the first particle.
                validAssg = True # Innovent til proven guilty:)

                # While we have not checked all particles and have not encountered an indication of an invalid assignment.
                while ((partNum < numParticles) and (validAssg == True)):

                    # If the assignment is spin up and this particle is supposed to be set to spin down according to our pruning conditions.
                    if ((assg[partNum]=='1') and ((partNum+1) not in particlesInvolved)):
                        validAssg = False

                    # Move on to next particle.
                    partNum += 1

                # If this is a valid assignment.
                if (validAssg == True):
                    clauseSat = 1

                # Otherwise forget about this contribution.
                else:
                    clauseSat = 0
            # Put number of clauses unsatisfied by assignment, i, on Hp diagonal entry, i.
            # If assignment satisfied this clause decrease the count for how many clauses it does not satisfy.
            hp[assgNum] -= clauseSat

        # Keep record of assignment(s) satisfying the most clauses.
        # Number of clauses satisfied has been seen before.
        if (hp[assgNum] == minNumClauseNotSat):
            bestAssgs = np.hstack([bestAssgs, assgNum + 1])
        # New "low score".
        elif (hp[assgNum] < minNumClauseNotSat):
            bestAssgs = [(assgNum + 1)]
            minNumClauseNotSat = hp[assgNum]

    # Sparsify the Hp Matrix.
    hp = sp.diags(hp,0)
    return hp, minNumClauseNotSat, bestAssgs


numInstances =  100
charKernel(numInstances)
loadCharKernel(numInstances)

