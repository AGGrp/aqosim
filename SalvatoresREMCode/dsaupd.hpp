/*
  In this header file, I have defined a simplified function
  call to the ARPACK solver routine for a simple, symmetric
  eigenvalue problem Av = lv.  The looping procedure and
  final extraction of eigenvalues and vectors is handled
  automatically.  Most of the parameters to the FORTRAN
  functions are hidden from the user, since most of them are
  determined from user input anyway.
  
  The remaining parameters to the function calls are as follows:
  
    dsaupd(int n, int nev, double *Evals)
    dsaupd(int n, int nev, doubel *Evals, double **Evecs)

    n: the order of the square matrix A
    nev: the number of eigenvalues to be found, starting at the
         bottom.  Note that the highest eigenvalues, or some
	 other choices, can be found.  For now, the choice of
	 the lowest nev eigenvalues is hard-coded.
    Evals: a one-dimensional array of length nev to hold the
           eigenvalues.
    Evecs: a two-dimensional array of size nev by n to hold the
           eigenvectors.  If this argument is not provided, the
	   eigenvectors are not calculated.  Note that the
	   vectors are stored as columns of Evecs, so that the
	   elements of vector i are the values Evecs[j][i].

  The function is overdefined, so that you can call it in either
  fashion and the appropriate version will be run.

  To use these function calls, there must be a function
  defined in the calling program of the form

    av(int n, double *in, double *out)

  where the function av finds out = A.in, and n is the order of
  the matrix A.  This function must be defined before the
  statement that includes this header file, as it needs to know
  about this function.  It is used in the looping procedure.

  Scot Shaw
  30 August 1999
*/

#include <cstdlib>
#include <sys/times.h>
#include <math.h>
#include <iostream>

extern "C" void dsaupd_(int *ido, char *bmat, int *n, char *which,
			int *nev, double *tol, double *resid, int *ncv,
			double *v, int *ldv, int *iparam, int *ipntr,
			double *workd, double *workl, int *lworkl,
			int *info);

extern "C" void dseupd_(int *rvec, char *All, int *select, double *d,
			double *v1, int *ldv1, double *sigma, 
			char *bmat, int *n, char *which, int *nev,
			double *tol, double *resid, int *ncv, double *v2,
			int *ldv2, int *iparam, int *ipntr, double *workd,
			double *workl, int *lworkl, int *ierr);

extern double *resid;
extern double tollerance;

void dsaupd(int n, int nev, double *Evals)
{

  if(nev > n-1) nev = n - 1;

  int ido = 0; /* Initialization of the reverse communication
		  parameter. */

  char bmat[2] = "I"; /* Specifies that the right hand side matrix
			 should be the identity matrix; this makes
			 the problem a standard eigenvalue problem.
			 Setting bmat = "G" would have us solve the
			 problem Av = lBv (this would involve using
			 some other programs from BLAS, however). */

  char which[3] = "SA"; /* Ask for the nev eigenvalues of smallest
			   magnitude.  The possible options are
			   LM: largest magnitude
			   SM: smallest magnitude
			   LA: largest real component
			   SA: smallest real compoent
			   LI: largest imaginary component
			   SI: smallest imaginary component */

  double tol = tollerance; /* Sets the tolerance; tol<=0 specifies 
		       machine precision */

  int ncv = 4*nev; /* The largest number of basis vectors that will
		      be used in the Implicitly Restarted Arnoldi
		      Process.  Work per major iteration is
		      proportional to N*NCV*NCV. */
  if (ncv>n) ncv = n;

  double *v;
  int ldv = n;
  v = new double[ldv*ncv];

  int *iparam;
  iparam = new int[11]; /* An array used to pass information to the routines
			   about their functional modes. */
  iparam[0] = 1;   // Specifies the shift strategy (1->exact)
  iparam[2] = 3*n; // Maximum number of iterations
  iparam[6] = 1;   /* Sets the mode of dsaupd.
		      1 is exact shifting,
		      2 is user-supplied shifts,
		      3 is shift-invert mode,
		      4 is buckling mode,
		      5 is Cayley mode. */

  int *ipntr;
  ipntr = new int[11]; /* Indicates the locations in the work array workd
			  where the input and output vectors in the
			  callback routine are located. */

  double *workd;
  workd = new double[3*n];

  double *workl;
  workl = new double[ncv*(ncv+8)];

  int lworkl = ncv*(ncv+8); /* Length of the workl array */

  int info = 1; /* Passes convergence information out of the iteration
		   routine. */

  int rvec = 0; /* Specifies that eigenvectors should not be calculated */

  int *select;
  select = new int[ncv];
  double *d;
  d = new double[2*ncv]; /* This vector will return the eigenvalues from
			    the second routine, dseupd. */
  double sigma;
  int ierr;

  /* Here we enter the main loop where the calculations are
     performed.  The communication parameter ido tells us when
     the desired tolerance is reached, and at that point we exit
     and extract the solutions. */

  int time = 0;
  int status = 0;
  cerr << "I'm starting!" << endl;
  do {
    dsaupd_(&ido, bmat, &n, which, &nev, &tol, resid, 
	    &ncv, v, &ldv, iparam, ipntr, workd, workl,
	    &lworkl, &info);

    if(++time % 10 == 0) {
    	std::cerr << "\r                                                                      \r";
    	std::cerr << "Compute the Eigenvalues (" << time << "/" << iparam[2] << ") .. ";
    	switch(status % 8) {
    		case 0:
    			std::cerr << "|" << std::flush;
    			break;
    		case 1:
    			std::cerr << "/" << std::flush;
    			break;
    		case 2:
  	  		std::cerr << "-" << std::flush;
    			break;
    		case 3:
    			std::cerr << "\\" << std::flush;
    			break;
		    case 4:
    			std::cerr << "|" << std::flush;
    			break;
    		case 5:
    			std::cerr << "/" << std::flush;
    			break;
    		case 6:
	    		std::cerr << "-" << std::flush;
    			break;
    		case 7:
    			std::cerr << "\\" << std::flush;
    			break;
    	}
    	++status;
    }
    
    if ((ido==1)||(ido==-1)) av(n, workd+ipntr[0]-1, workd+ipntr[1]-1);
  } while ((ido==1)||(ido==-1));

  std::cerr << "\r                                                                      \r";
  std::cerr << "Compute the Eigenvalues .. Done";

  /* From those results, the eigenvalues and vectors are
     extracted. */

  if (info<0) {
         std::cerr << "Error with dsaupd, info = " << info << "\n";
         std::cerr << "Check documentation in dsaupd\n\n";
  } else {
    dseupd_(&rvec, "All", select, d, v, &ldv, &sigma, bmat,
	    &n, which, &nev, &tol, resid, &ncv, v, &ldv,
	    iparam, ipntr, workd, workl, &lworkl, &ierr);

    if (ierr!=0) {
      std::cerr << "Error with dseupd, info = " << ierr << "\n";
      std::cerr << "Check the documentation of dseupd.\n\n";
    } else if (info==1) {
      std::cerr << "Maximum number of iterations reached.\n\n";
    } else if (info==3) {
      std::cerr << "No shifts could be applied during implicit\n";
      std::cerr << "Arnoldi update, try increasing NCV.\n\n";
    }
    
    /* Before exiting, we copy the solution information over to
       the arrays of the calling program, then clean up the
       memory used by this routine.  For some reason, when I
       don't find the eigenvectors I need to reverse the order of
       the values. */

    int i;
    for (i=0; i<nev; i++) Evals[i] = d[nev-1-i];

    delete v;
    delete iparam;
    delete ipntr;
    delete workd;
    delete workl;
    delete select;
    delete d;
  }
}


void dsaupd(int n, int nev, double **Evals, double **Evecs, bool init_v = false)
{

  if(nev > n) nev = n;

  bool complete = false;
  if(nev == n) {
  	--nev;
	complete = true;
  }

  int ido = 0;
  char bmat[2] = "I";
  char which[3] = "SA";
  double tol = tollerance;

  // Here we can set the initial vector
  int info = 0;
  if(init_v) info = 1;

  int ncv = 4*nev;
  if (ncv>n) ncv = n;
  double *v;
  int ldv = n;
  int *iparam;
  iparam = new int[11];
  iparam[0] = 1;		// Shift
  iparam[2] = 3*n;
  iparam[6] = 1;
  int *ipntr;
  ipntr = new int[11];
  double *workd;
  workd = new double[3*n];
  double *workl;
  workl = new double[ncv*(ncv+8)];
  int lworkl = ncv*(ncv+8);
  int rvec = 1;  // Changed from above
  int *select;
  select = new int[ncv];
  double *d;

  //d = new double[2*ncv];
  //v = new double[ldv*ncv];

  d = *Evals;
  v = *Evecs;

  double sigma;
  int ierr;

  int time = 0;
  int status = 0;
  cerr << "I'm starting!" << endl;
  do {
    dsaupd_(&ido, bmat, &n, which, &nev, &tol, resid, 
	    &ncv, v, &ldv, iparam, ipntr, workd, workl,
	    &lworkl, &info);
    
    if(++time % 10 == 0) {
    	std::cerr << "\r                                                                      \r";
    	std::cerr << "Compute the Eigenvalues and Eigenvectors .. ";
	switch(status % 8) {
		case 0:
			std::cerr << "|" << std::flush;
			break;
		case 1:
			std::cerr << "/" << std::flush;
			break;
		case 2:
			std::cerr << "-" << std::flush;
			break;
		case 3:
			std::cerr << "\\" << std::flush;
			break;
		case 4:
			std::cerr << "|" << std::flush;
			break;
		case 5:
			std::cerr << "/" << std::flush;
			break;
		case 6:
			std::cerr << "-" << std::flush;
			break;
		case 7:
			std::cerr << "\\" << std::flush;
			break;
	}
	++status;
    }
    if ((ido==1)||(ido==-1)) av(n, workd+ipntr[0]-1, workd+ipntr[1]-1);
  } while ((ido==1)||(ido==-1));

  std::cerr << "\r                                                                      \r";
  std::cerr << "Compute the Eigenvalues and Eigenvectors .. Done";

  if (info<0) {
         std::cerr << "Error with dsaupd, info = " << info << "\n";
         std::cerr << "Check documentation in dsaupd\n\n";
  } else {
    dseupd_(&rvec, "All", select, d, v, &ldv, &sigma, bmat,
	    &n, which, &nev, &tol, resid, &ncv, v, &ldv,
	    iparam, ipntr, workd, workl, &lworkl, &ierr);

    if (ierr!=0) {
      std::cerr << "Error with dseupd, info = " << ierr << "\n";
      std::cerr << "Check the documentation of dseupd.\n\n";
    } else if (info==1) {
      std::cerr << "Maximum number of iterations reached.\n\n";
    } else if (info==3) {
      std::cerr << "No shifts could be applied during implicit\n";
      std::cerr << "Arnoldi update, try increasing NCV.\n\n";
    }

    //for (i=0; i<nev; i++) Evals[i] = d[i];
    //for (i=0; i<nev; i++) for (j=0; j<n; j++) Evecs[i][j] = v[i*n+j];
    //*Evals = d;
    //*Evecs = v;

    //delete d;
    //delete v;

    delete iparam;
    delete ipntr;
    delete workd;
    delete workl;
    delete select;

    if(!complete) return; 

    throw "To be done!";

    /*
    std::cerr << std::endl;
    std::cerr << "Compute the last eigenvector by completition ..";

    // Create a Random Configuration
    srand48(times(NULL));
    for(i=0; i<n; ++i) Evecs[nev][i] = drand48();

    // Gram-Schmidt Ortogonalization
    for(j=0; j<nev; ++j) {
    
    	double coeff = 0;
	for(i=0; i<n; ++i) coeff += Evecs[j][i] * Evecs[nev][i];
	for(i=0; i<n; ++i) Evecs[nev][i] -= coeff * Evecs[j][i];
    
    }

    // Normalization
    {
	double sum = 0;
    	for(i=0; i<n; ++i) sum += Evecs[nev][i] * Evecs[nev][i];
    	for(i=0; i<n; ++i) Evecs[nev][i] /= sqrt(sum);
    }

    {
    	std::vector<double> out(n, 0);
  	
        //for(u_int j = 0; j < tlen; ++j) out[T[j].i] += Evecs[nev][T[j].j] * T[j].val;
	av(n, Evecs[nev], &(out[0]));

	double sum = 0;
	for(u_int i = 0; i < n; ++i) sum += out[i]/Evecs[nev][i];

	Evals[nev] = sum/n;
    } 

    std::cerr << "Done!" << std::endl;
    */
  }
}




