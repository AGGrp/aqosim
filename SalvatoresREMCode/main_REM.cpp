#include <algorithm>
#include <cassert>
#include <iomanip>
#include <bitset>
#include <iostream>
#include <cstdlib>
#include <sys/times.h>
#include <vector>
#include <map>
using namespace std;

#define BISECTION

void av(int n, double *in, double *out);
u_int BitCount(u_int v);
u_int Assign(u_int i, u_int j);
u_int CheckEnergy(u_int conf, const vector<u_int> &mask);

#include "dsaupd.hpp"  

double *resid;
double tollerance;
double s;
u_int  L;
vector<pair<u_int, double> > xi;
vector<u_int>     conf;
vector<u_int> inv_conf;
vector<u_int>     cost;

double IPR(double *v, u_int N) {
 
	double p2 = 0;
	double p4 = 0;
	double temp;

	for(u_int i = N; i--; ) {
		p2 += (temp = v[i] * v[i]);
		p4 += temp * temp;
	}

	return sqrt(p4)/p2;
};

int main(int argc, char **argv) {

	cout << scientific << setprecision(10);

	const u_int n = atoi(argv[1]);
	const u_int k = 3;
	
	u_int seed;
	if(argc == 3) srandom(seed = atoi(argv[2]));
	else          srandom(seed = times(NULL)  );

	cost.resize(0x1u << (n-1));

  for(u_int i = (0x1u << (n-1)); i--; ) cost[i] = BitCount(i);

  // Random permutation of the energies
  random_shuffle(cost.begin(), cost.end());

	// ----------------------------------------------------------------------------------------------------
	cerr << "\r                          \r";

	const u_int nev = 2;
	const u_int N = 0x1u << (n-1);
	double *Evals = new double[2*4*nev];

	        resid = new double[N];
  	   tollerance = 0;

  for(u_int i = 0; i < N; ++i) resid[i] = rand();

	cerr << "### Used seed : " << seed << endl;
	cerr << "### Tollerance: " << tollerance << endl;


	// ------------------------------------ Create the xi vector -----------------------------------------
	{
		xi.clear();
		xi.push_back(make_pair(0, n-1));
		for(u_int n1 = n-1; n1--; ) xi.push_back(make_pair(0x1u << n1, -1.));
		sort(xi.begin(), xi.end());
	}
	// ----------------------------------------------------------------------------------------------------

#ifdef BISECTION
  {
    double s_min = 0;
    double s_max = 1;
    double ds = 0.10;

    double gap = 0;
    double min_gap = 1e100;
    double min_s = 0;

    for(u_int k = 10; k--; ) {

      for(u_int i = 0; i <= 10; ++i) { 
      
        s = s_min + i * ds; 

        cerr << endl << "### " << s << "; ";

        dsaupd(N, nev, Evals); 
        gap = Evals[1] - Evals[0];

  	    cout << n << " " << s << " " << gap << endl;
  
        if(gap < min_gap) {
          min_gap = gap;
          min_s = s;
        }

      }

      s_min = min_s - ds;
      s_max = min_s + ds;

      ds /= 5;

    }

    cerr << endl;
  
  }
#else
	for(s = 0.2; s <= 0.8; s += 0.01) {
		cerr << endl << "s : " << s << endl;
		dsaupd(N, nev, Evals);
	
		cout << s << " "; for(u_int i = 0; i < nev; ++i) cout << Evals[i] << " ";
		cout << " " << Evals[1] - Evals[0] << endl;
	}
#endif

	if(Evals != NULL) delete[] Evals;
	if(resid != NULL) delete[] resid;

	return 0;
};

u_int BitCount(u_int v) {

	v = v - ((v >> 1) & 0x55555555);                     		// reuse input as temporary
	v = (v & 0x33333333) + ((v >> 2) & 0x33333333);     		// temp
	return ((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24;
};

void av(int N, double *in, double *out) {

	for(u_int i = 0; i < N; ++i) {
		double sum = 0;
		u_int temp;

		for(vector<pair<u_int, double> >::const_iterator w = xi.begin(); w != xi.end(); ++w)
			sum += (w->second * in[w->first ^ i]);
			
		out[i] = 0.5 * s * sum + (1.-s) * cost[i] * in[i];
	}

};

inline u_int Assign(u_int i, u_int j) { return (0x1u << i) ^ (0x1u << j); };
u_int CheckEnergy(u_int conf, const vector<u_int> &mask) {

	u_int energy = 0;
	for(vector<u_int>::const_iterator m = mask.begin(); m != mask.end(); ++m)
		energy += (((conf & *m) == 0) || ((conf & *m) == *m));

	return energy;
};
