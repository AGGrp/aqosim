# Author: Kelsey C. Justis
# Date: 08/14/2015
# Summary: Program takes in a given number of clauses and
# Input: 
#-n: integer number of particles
#-m: integer number of clauses
#-i: integer number of instances to check
# Output: 
#- Break down of how many times a given number of solutions exists for the i number of instances of the given n and m.

# %matplotlib inline
import random
import numpy as np
from makeHp import makeHp
import math
import matplotlib.pyplot as plt
import matplotlib.cm as cm

# Characterizes Hp for entered number n of particles, number of clauses. and number of instances entered, i.
def charHp(numParticles, numClauses, numInstances = 100):

    # Initialize counter objects.
    uniqSolCtr = 0                  # Counts number of unique Max2Sat solutions.
    satSolCtr = 0                   # Counts number of unique 2Sat solutions.
    sumNumBestAssgs = 0.      # Number of "best" assignments for given Hp.
    sumNumClauseNotSat = 0.   # How many clauses are not satisfied for this Hp.
    alpha = float(float(numClauses)/float(numParticles))

    # For each instance.
    for i in range(numInstances):

        # Create a new Hp with given number of particles and clauses.
        results = makeHp(numParticles,numClauses)

        # Get smallest number of assignments not satisfied.
        numClausesNotSat = results[1]

        # Get number of assignments with this number of unsatisfied assignments.
        numBestAssgs = len(results[2])

        # Check to see if this Hp has a unique solution.
        if (numBestAssgs == 1):
            uniqSolCtr += 1
            if (numClausesNotSat == 0):
                satSolCtr += 1
        sumNumClauseNotSat += numClausesNotSat
        sumNumBestAssgs += numBestAssgs

    avgNumClausesNotSat = sumNumClauseNotSat/numInstances
    avgNumBestAssgs = sumNumBestAssgs/numInstances

    return avgNumClausesNotSat, avgNumBestAssgs, uniqSolCtr, satSolCtr, alpha

# Characterizes Hp from 2:entered number n of particles and from 1:mMax number of clauses with number of instances entered, i.
def charHpRange(numParticles, numInstances = 100):

    # Initialize dictionary of information for each number of particles.
    charData = {}

    # For each particle number up to n.
    for particleNum in range(2,numParticles + 1):

        # Maximum number of unique clauses depending on n.
        mMax = 2*particleNum*(particleNum - 1)

        # Intialize data matrix for this particle number.
        dataMatrix = np.zeros((mMax,6))
        # For each clause up to mMax.
        for numClauses in range(1, mMax+1):

            # Add row entry for current number of clauses. Column1: number of unique solutions to Max2Sat, Col2: number of unique solutions to 2Sat, Col3: Alpha = m/n.
            dataMatrix[numClauses-1, 0] = numClauses
            dataMatrix[numClauses-1, 1], dataMatrix[numClauses-1 , 2], dataMatrix[numClauses-1 , 3], dataMatrix[numClauses-1 , 4], dataMatrix[numClauses-1 , 5]= \
                charHp(particleNum,numClauses, 10)

        # Add data matrix to dictionary with corresponding particle number key.
        charData[str(particleNum)] = dataMatrix
    return charData,numParticles,numInstances

# Plots results of characteristics of Hps of concern.
def plotCharData(charDataResults):

    # Split Up results.
    charData,numParticles,numInstances = charDataResults

    # Clear current figures.
    plt.cla()
    plt.close("all")
    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(numParticles)
    ys = [i+x+(i*x)**2 for i in range(numParticles)]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))

    # Display results.
    for key in sorted(charData.keys()):

        # Print dictionary results to console.
        print '\nNumber of particles = ' + str(key) + '\n' + \
              '# Clauses | Average # of Clauses Not SAT | Average Degeneracy | #Uniq. Max2Sat Sol. | #Uniq. 2Sat Sol. | Alpha\n'\
              + str(charData[str(key)])

        numParticles = int(key)
        mMax = (2*numParticles)*(numParticles-1)
        alphaMax = (mMax)/(numParticles)
        numClauses = charData[key][0:,0]
        numClauseScaled = charData[key][0:,0]/mMax
        avgNumClauseNotSat = charData[key][0:,1]
        avgDeg = charData[key][0:,2]
        numUniMax2Sat = charData[key][0:,3]
        numUni2Sat = charData[key][0:,4]
        alpha = charData[key][0:,5]
        alphaScaled = charData[key][0:,5]/alphaMax

        # PLOT RESULTS.
        # Get new pretty color for this number of particles.
        newColor = next(colors)

        # Plot Degeneracy vs. Number of clauses (scaled by m/mMax).
        plt.hold(1)
        plt.figure(1)
        plt.scatter(numClauseScaled,avgDeg, color=newColor)
        plt.plot(numClauseScaled,avgDeg, color=newColor, label = '# Particles = ' + key)

        # Plot Degeneracy vs. Number of clauses.
        plt.figure(2)
        plt.scatter(numClauses,avgDeg, color=newColor)
        plt.plot(numClauses,avgDeg, color=newColor, label = '# Particles = ' + key)

        # Plot Degeneracy vs. Alpha (scaled by alphaMax).
        plt.figure(3)
        plt.scatter(alphaScaled, avgDeg, color=newColor)
        plt.plot(alphaScaled, avgDeg, color=newColor, label = '# Particles = ' + key)

        # Plot Degeneracy vs. Alpha.
        plt.figure(4)
        plt.scatter(alpha, avgDeg, color=newColor)
        plt.plot(alpha, avgDeg, color=newColor, label = '# Particles = ' + key)

        # Plot Unique Max2Sat solutions vs. Number of clauses (scaled by m/mMax).
        plt.figure(5)
        plt.scatter(numClauseScaled, numUniMax2Sat, color=newColor)
        plt.plot(numClauseScaled, numUniMax2Sat, color=newColor, label = '# Particles = ' + key)

        # Plot Unique Max2Sat solutions vs. Number of clauses.
        plt.figure(6)
        plt.scatter(numClauses, numUniMax2Sat, color=newColor)
        plt.plot(numClauses, numUniMax2Sat, color=newColor, label = '# Particles = ' + key)

        # Plot Unique 2Sat solutions vs. Number of clauses (scaled by m/mMax).
        plt.figure(7)
        plt.scatter(numClauseScaled, numUni2Sat, color=newColor)
        plt.plot(numClauseScaled, numUni2Sat, color=newColor, label = '# Particles = ' + key)

        # Plot Unique 2Sat solutions vs. Number of clauses.
        plt.figure(8)
        plt.scatter(numClauses, numUni2Sat, color=newColor)
        plt.plot(numClauses, numUni2Sat, color=newColor, label = '# Particles = ' + key)

        # Plot Unique Max2Sat solutions vs. Alpha (scaled by alphaMax).
        plt.figure(9)
        plt.scatter(alphaScaled, numUniMax2Sat, color=newColor)
        plt.plot(alphaScaled, numUniMax2Sat, color=newColor, label = '# Particles = ' + key)

        # Plot Unique Max2Sat solutions vs. Alpha.
        plt.figure(10)
        plt.scatter(alpha, numUniMax2Sat, color=newColor)
        plt.plot(alpha, numUniMax2Sat, color=newColor, label = '# Particles = ' + key)

        # Plot Unique 2Sat solutions vs. Alpha(scaled by alphaMax).
        plt.figure(11)
        plt.scatter(alphaScaled, numUni2Sat, color=newColor)
        plt.plot(alphaScaled, numUni2Sat, color=newColor, label = '# Particles = ' + key)

        # Plot Unique 2Sat solutions vs. Alpha.
        plt.figure(12)
        plt.scatter(alpha, numUni2Sat, color=newColor)
        plt.plot(alpha, numUni2Sat, color=newColor, label = '# Particles = ' + key)

        # Plot Average number of clauses not satisfied vs. Number of clauses.
        plt.figure(13)
        plt.scatter(numClauseScaled, avgNumClauseNotSat, color=newColor)
        plt.plot(numClauseScaled, avgNumClauseNotSat, color=newColor, label = '# Particles = ' + key)

        # Plot Average number of clauses not satisfied vs. Number of clauses (scaled by m/mMax).
        plt.figure(14)
        plt.scatter(numClauses, avgNumClauseNotSat, color=newColor)
        plt.plot(numClauses, avgNumClauseNotSat, color=newColor, label = '# Particles = ' + key)

        # Plot Average number of clauses not satisfied vs. Alpha (scaled by alphaMax)
        plt.figure(15)
        plt.scatter(alphaScaled, avgNumClauseNotSat, color=newColor)
        plt.plot(alphaScaled, avgNumClauseNotSat, color=newColor, label = '# Particles = ' + key)

        # Plot Average number of clauses not satisfied vs. Alpha.
        plt.figure(16)
        plt.scatter(alpha, avgNumClauseNotSat, color=newColor)
        plt.plot(alpha, avgNumClauseNotSat, color=newColor, label = '# Particles = ' + key)
    # Label and show plots.

    # Label plot for Degeneracy vs. Number of clauses (scaled by m/mMax).
    plt.figure(1)
    plt.title('Degeneracy vs. m:# of Clauses')
    plt.xlabel('Number of Clauses (scaled by m/mMax)')
    plt.ylabel('Average number of "best" solutions')
    plt.legend()

    # Label plot for Degeneracy vs. Number of clauses.
    plt.figure(2)
    plt.title('Degeneracy vs. m:# of Clauses')
    plt.xlabel('Number of Clauses')
    plt.ylabel('Average number of "best" solutions')
    plt.legend()

    # Label plot for Degeneracy vs. Alpha (scaled by alphaMax).
    plt.figure(3)
    plt.title('Degeneracy vs. Alpha')
    plt.xlabel('Alpha (scaled by alphaMax)')
    plt.ylabel('Average number of "best" solutions')
    plt.legend()

    # Label plot for Degeneracy vs. Alpha.
    plt.figure(4)
    plt.title('Degeneracy vs. Alpha')
    plt.xlabel('Alpha')
    plt.ylabel('Average number of "best" solutions')
    plt.legend()

    # Label plot Unique Max2Sat solutions vs. Number of clauses (scaled by m/mMax).
    plt.figure(5)
    plt.title('# of Unique Max2Sat solutions vs. m: # of clauses')
    plt.xlabel('Number of Clauses (scaled by m/mMax)')
    plt.ylabel('# of Hp Instances (out of ' + str(numInstances) + ') With Unique Max2Sat solutions')
    plt.legend()

    # Label plot Unique Max2Sat solutions vs. Number of clauses.
    plt.figure(6)
    plt.title('# of Unique Max2Sat solutions vs. m: # of clauses')
    plt.xlabel('Number of Clauses')
    plt.ylabel('# of Hp Instances (out of ' + str(numInstances) + ') With Unique Max2Sat solutions')
    plt.legend()

    # Label plot Unique 2Sat solutions vs. Number of clauses.
    plt.figure(7)
    plt.title('# of Unique 2Sat solutions vs. m: # of clauses')
    plt.xlabel('Number of Clauses (scaled by m/mMax)')
    plt.ylabel('# of Hp Instances (out of ' + str(numInstances) + ') With Unique 2Sat solutions')
    plt.legend()

    # Label plot Unique 2Sat solutions vs. Number of clauses (scaled by m/mMax).
    plt.figure(8)
    plt.title('# of Unique 2Sat solutions vs. m: # of clauses')
    plt.xlabel('Number of Clauses')
    plt.ylabel('# of Hp Instances (out of ' + str(numInstances) + ') With Unique 2Sat solutions')
    plt.legend()

    # Label plot Unique Max2Sat solutions vs. Alpha.
    plt.figure(9)
    plt.title('# of Unique Max2Sat solutions vs. Alpha')
    plt.xlabel('Alpha (scaled by alphaMax)')
    plt.ylabel('# of Hp Instances (out of ' + str(numInstances) + ') With Unique Max2Sat solutions')
    plt.legend()

    # Label plot Unique Max2Sat solutions vs. Alpha (scaled by alphaMax).
    plt.figure(10)
    plt.title('# of Unique Max2Sat solutions vs. Alpha')
    plt.xlabel('Alpha')
    plt.ylabel('# of Hp Instances (out of ' + str(numInstances) + ') With Unique Max2Sat solutions')
    plt.legend()


    # Label plot Unique 2Sat solutions vs. Alpha.
    plt.figure(11)
    plt.title('# of Unique 2Sat solutions vs. Alpha')
    plt.xlabel('Alpha (scaled by alphaMax)')
    plt.ylabel('# of Hp Instances (out of ' + str(numInstances) + ') With Unique 2Sat solutions')
    plt.legend()

    # Label plot Unique 2Sat solutions vs. Alpha (scaled by alphaMax).
    plt.figure(12)
    plt.title('# of Unique 2Sat solutions vs. Alpha')
    plt.xlabel('Alpha')
    plt.ylabel('# of Hp Instances (out of ' + str(numInstances) + ') With Unique 2Sat solutions')
    plt.legend()


    # Label Average number of clauses not satisfied vs. Number of clauses (scaled by m/mMax).
    plt.figure(13)
    plt.title('Avg # of clauses not satisfied vs. m: # of clauses')
    plt.xlabel('Number of Clauses (scaled by m/mMax)')
    plt.ylabel('Avg. Best Sol. (# of clauses not satisfied) for ' + str(numInstances) + ' instances of Hp')
    plt.legend()

    # Label Average number of clauses not satisfied vs. Number of clauses.
    plt.figure(14)
    plt.title('Avg # of clauses not satisfied vs. m: # of clauses')
    plt.xlabel('Number of Clauses')
    plt.ylabel('Avg. Best Sol. (# of clauses not satisfied) for ' + str(numInstances) + ' instances of Hp')
    plt.legend()

    # Label Average number of clauses not satisfied vs. Alpha (scaled by alphaMax).
    plt.figure(15)
    plt.title('Avg # of clauses not satisfied vs. Alpha')
    plt.xlabel('Alpha (scaled by alphaMax)')
    plt.ylabel('Avg. Best Sol. (# of clauses not satisfied) for ' + str(numInstances) + ' instances of Hp')
    plt.legend()

    # Label Average number of clauses not satisfied vs. Alpha.
    plt.figure(16)
    plt.title('Avg # of clauses not satisfied vs. Alpha')
    plt.xlabel('Alpha')
    plt.ylabel('Avg. Best Sol. (# of clauses not satisfied) for ' + str(numInstances) + ' instances of Hp')
    plt.legend()

    # Show all plots.
    plt.show()

# Get from user the number of particles, clauses, and random assignments to try.
numParticles = input('Enter number of particles/spins in the system: \n')
numInstances =  input('Enter number of instances to try: \n')
plotCharData(charHpRange(numParticles, numInstances))

