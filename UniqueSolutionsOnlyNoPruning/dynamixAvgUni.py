# Author: Kelsey C. Justis
# Date: 08/17/2015
# Summary: Program takes as input n and m (integer number of particles and clauses respectively) and uses results from statics
# performed on a 2SAT problem instance with a unique solution to perform dynamics on said instance; the dynamics should inform the user how
# the solver operates to find a solution to the problem as a simulated Adiabatic Quantum Optimization solver.
# Input:
## n - Integer number of particles.
## m - Integer number of clauses.
# Output: 
##- Plot of

# Import required libraries.
import math
import numpy as np
from makeHp import makeHp
from makeHd import makeHd
import matplotlib.pyplot as plt
import scipy.sparse.linalg as spla
from scipy.sparse import csc_matrix
import cProfile
import pstats

# Gets minimum and maximum number of particles to plot gMin and tComp for.
def getUserInput():
    # minNumParticles = input('Enter min number of particles/spins in the system to plot: \n')
    # maxNumParticles = input('Enter max number of particles/spins in the system to plot: \n')

    # Get from user the number of particles, clauses, and random assignments to try.
    numParticles = input('Enter number of particles/spins in the system: \n')
    numClauses = input('Enter number of clauses to test: \n')

    # Check to makes sure number of clauses is possible to make.
    while (numClauses > (2*math.factorial(numParticles)/ math.factorial(numParticles - 2))):
        numClauses = input('***Number of clauses entered exceeds number of unique clauses***. \n'
                           'Enter a number less than 2*factorial(number of particles)/(factorial(number of particles - 2): \n')

    # # Get final time T to evolve state to.
    # T = input('Enter final time, T, for system to evolve to: \n')

    # Get number of steps to evolve system in.
    N = input('Enter number of steps, N, for system to evolve: \n')

    return numParticles,numClauses,N

# Get gap for given Hamiltonian.
def getGS(Hamiltonian):
    numEvals = 0# Initialize the number of eigenvalues found to 0.
    maxIter = 1000000 # Initialize the maximum number of iterations the solver should go through to find the desired eigenvalues.
    tolerance = 1E-4 # Initialize the tolerance desired before eigenvalue solver stops iterating to find more accurate eigenvalue.
                     # "Relative accuracy for eigenvalues (stopping criterion). The default value of 0 implies machine precision."

    # Try the ARPACK sparse eigenvalue solver function until we have two "good" eigenvalues.
    while (numEvals < 2):

        # Try to use solver with current stopping criterion (iterations and tolerance).
        try:
            # Get eigenvalues for ground and first excited state for given Hamiltonian.
            eVal1, eVal2 = np.sort(spla.eigsh(Hamiltonian, 2, which = 'SA', return_eigenvectors = False, tol = tolerance, maxiter = maxIter))

            # If we have no eigenvalues yet and the eigenvalue function worked.
            if (numEvals == 0):
                eValGrnd = eVal1 # Give the smaller of the given two from the eigenvalue function.
                eValExc = eVal2  # Give the greater of the given two from the eigenvalue function.

            # If we already have one eigenvalue and the eigenvalue function worked.
            elif(numEvals == 1):

                # Give eValGrnd the smallest of the previously found eigenvalue and the given two from the current eigenvalue function.
                # and give eValExc the second smallest.
                if (eVal1 == eValGrnd):
                    eValExc = eVal2
                elif (eVal1 > eValGrnd):
                    eValExc = eVal1
                else:
                    if(eVal2 < eValGrnd):
                        eValExc = eVal2
                    else:
                        eValExc = eValGrnd
                    eValGrnd = eVal1

            # Calculate the gap.
            gS = abs(eValExc - eValGrnd)

            # If the gap and the tolerance are within a magnitude of each other.
            if (gS/tolerance < 10.):
                tolerance = tolerance/10. # Make the tolerance smaller.
                numEvals = 0              # Recalculate eigenvalues with smaller tolerance.
            else:
                numEvals = 2  # We found eigenvalues with good enough precision.

        # For some reason the ARPACK eigenvalue solver function did not work.
        except spla.ArpackNoConvergence as err1:

            # Increase the number of iterations (time) it may take to try and find a "good" eigenvalue.
            maxIter = maxIter*10

            # If we the solver failed to provide two eigenvalues but found one eigenvalue.
            if (len(err1.eigenvalues) == 1):

                # If we do not have any eigenvalues.
                if (numEvals == 0):
                    eValGrnd = err1.eigenvalues[0] # Use this eigenvalue as the lowest eigenvalue found.

                # If we already have an eigenvalue.
                elif(numEvals == 1):
                    # Give eValGrnd the smallest of the previously found eigenvalue and the given one from the current eigenvalue function.
                    # and give eValExc the second smallest.
                    if (eValGrnd < err1.eigenvalues[0]):
                        eValExc = err1.eigenvalues[0]
                    else:
                        eValExc = eValGrnd
                        eValGrnd = err1.eigenvalues[0]

                # We found an eigenvalue.
                numEvals += 1

    # Return the gap found.
    return gS

# Finds tComp and optimal evolution time for given driver (Hd) and problem (Hp) Hamiltonians.
def runStatix(Hd,Hp):

    # Set initial values for gMin search process.
    gMin  = getGS(Hd)            # Get initial gap and use as gMin.
    optTEvol = 0.                # Optimal evolution time.
    acc = 0.0001               # Define accuracy desired as percent difference between sequential gMin values found.
    prevgMin = gMin/acc # Initialize old gMin value to a gMin value far outside our desired range of agreement

    # Initialize search grid over s.
    sDelta = 0.1  # Defines step between s values to search.
    sGMin = 0.5   # Set an initial s value (in center) for where gMin is located.
    i = 0
    # While the difference between subsequent gMin found is greater than our accuracy desired.
    while (abs((prevgMin - gMin)/prevgMin) > acc) or (i<5):
        prevgMin = gMin # Record the old gMin that has been succeeded.
        # For each of the s values in the range of s around our suspected location of gMin.
        for s in np.arange(max((sGMin - sDelta*10),0),(min((sGMin + sDelta*10),1)) + sDelta, sDelta):
           gS = getGS(((1 - s)*Hd + s*Hp)) # Get gap for current s value.
           optTEvol += (sDelta)/(gS**2) # Update optTevol.

           # Check if this gap is the smallest found in this range.
           if (gS < gMin):

               # Update this iteration's discovered gMin and record its location in the range of s.
               gMin = gS
               sGMin = s
        sDelta = sDelta/10
        i +=1
    tComp = gMin**-2

    # Return the gMin and optimal evolution time found for this alpha.
    return tComp, optTEvol

# Make a 2SAT problem instance with a unique solution for a given n and m.
def makeProblemInstance(numParticles, numClauses):

    # Counter used to make sure enough instances are tried to ensure current alpha does not have an Hp with a unique solution.
    numInstancesTried = 0

    # Initialize count of Hp with unique solution for current alpha.
    uniHpFound = False

    # Make sure we enough Hp for given alpha.
    while ((numInstancesTried < 100) and (uniHpFound == False)):

        # Create a new Hp with given number of particles and clauses.
        hpResults = makeHp(numParticles,numClauses)

        # If a unique solution Hp get gMin.
        if (len(hpResults[2]) == 1):
            Hp = hpResults[0] # Get current Hp instance.
            uniHpFound = True
        # Increase number of instances tried.
        numInstancesTried += 1

    if (numInstancesTried == 100):
        print 'No problem instance found with a unique solution.'
        return
    # Define initial and problem Hamiltonians.
    Hd = makeHd(numParticles)

    # Define initial state.
    solAssg = hpResults[2][0]
    solState = csc_matrix(([1.],([solAssg-1],[0])), shape=(2**numParticles,1)) # Get sparse vector form of assignment corresponding to the best solution for this Hp instance.

    return Hd,Hp,solState

# Evolves Hamiltonian from initial Hd to H(T).
def runDynamix(numParticles,numClauses,N):
    # Clear current figures and hold next drawings to come.
    plt.cla()
    plt.close("all")
    plt.hold(1)

    # Make a 2SAT problem instance with a unique solution.
    Hd,Hp,solState = makeProblemInstance(numParticles, numClauses)
    prevState = csc_matrix(2**(-numParticles/2.)*(np.ones(((2**numParticles),1))))
    prevOptState = prevState

    # Perform the statics on this problem instance to find the two evolution times desired.
    tComp, optTEvol = runStatix(Hd,Hp)

    # How many time periods to wait.
    numPeriods =10
    tComp = numPeriods*tComp
    optTEvol = numPeriods*optTEvol

    # Calculate time between steps in simulated evolution.
    delT = float(tComp)/N
    delOptT = float(optTEvol)/N

    # Initialize array for probabilities for values of s.
    probs = []
    optProbs = []
    sVals = []

    # Print problem description.
    grndProbs = []
    grndOptProbs = []
    # print 'Hd: \n' + str(Hd.toarray())
    # print '\nHp: \n' + str(Hp.toarray())
    # print '\nSolution State: \n' + str(solState.toarray())
    # print '\nInitial State: \n' + str(prevState.toarray())
    print '\nTcomp:' + str(tComp)+'\nOptimal Evolution time: '+str(optTEvol)
    print '\ndelT: ' + str(delT)
    print '\ndelOptT: ' + str(delOptT)

    # Evolve state.
    for k in range(1, N+1):
        print 'in loop'
        # Find HTotal(t) for next iteration using
        # A typical Hamiltonian of the form H(t) = (1-s)Hd + sHp.
        # For tComp.
        s = float(((k-1)*delT)/tComp)
        sVals = np.hstack([sVals,s])
        prevHam = (1 - s)*Hd + s*Hp
        print 'line236'
        # For optimal evolution time.
        sOpt = float(((k-1)*delOptT)/optTEvol)
        prevOptHam = (1 - sOpt)*Hd + sOpt*Hp
        print 'line240'

        # Find current state of system..
        # For tComp.
        psiK =  (spla.expm((-1j*delT)*prevHam)).dot(prevState)
        prevState = psiK
        print 'line246'
        # For optimal evolution time.
        psiKOpt =  (spla.expm((-1j*delOptT)*prevOptHam)).dot(prevOptState)
        prevOptState = psiKOpt
        print 'line250'

        prob = ((abs(psiK.transpose().dot(solState)))**2).toarray()[0]
        optProb = ((abs(psiKOpt.transpose().dot(solState)))**2).toarray()[0]

        probs = np.hstack([probs,prob])  # Add probability fr current s value to list of probabilities to plot.
        optProbs = np.hstack([optProbs,optProb])  # Add probability for current s value to list of probabilities to plot.
        print 'line260'
        # Print info for current iteration.
        grndState = (spla.eigsh(prevHam, 1, which = 'SA', return_eigenvectors = True, tol = 1E-4, maxiter = 1000000))[1]
        optGrndState = (spla.eigsh(prevOptHam, 1, which = 'SA', return_eigenvectors = True, tol = 1E-4, maxiter = 1000000))[1]
        grndProb = ((abs(psiK.transpose().dot(grndState)))**2)[0]
        grndOptProb = ((abs(psiKOpt.transpose().dot(optGrndState)))**2)[0]
        grndProbs = np.hstack([grndProbs,grndProb])
        grndOptProbs = np.hstack([grndOptProbs,grndOptProb])

        print '\ns: '+str(s)
        # print 'H(t): \n' + str(prevHam.toarray())
        # print 'psiK(t): \n' + str(psiK.toarray())
        # print 'prob(s): \n' + str(prob)
        # print 'Ground State:\n'+ str(grndState)

        print '\nsOpt: '+str(sOpt)
        # print 'optH(t): \n' + str(prevOptHam.toarray())
        # print 'OptPsiK(t): \n' + str(psiKOpt.toarray())
        # print 'optProb(optS): \n' + str(optProb)
        # print 'Optimal Ground State:\n'+ str(optGrndState)

    print '\nSvals:\n'+str(sVals)+'Probs:\n'+str(probs)+'\nOptProbs:\n'+str(optProbs)+'GroundProbs:\n'+str(grndProbs)+'\nOptGroundProbs:\n'+str(grndOptProbs)
    # Plot against alpha.
    plt.figure(1)
    # Plot tComp probabilities vs. s.
    plt.scatter(sVals,probs, color='k') # Plot dots.
    plt.plot(sVals,probs, color='k',ls = '-.', label = 'tComp-# Particles = ' + str(numParticles)) # Connect the dots.

    # Plot optimal evolution time probabilities vs. s.
    plt.scatter(sVals,optProbs, color='k') # Plot dots.
    plt.plot(sVals,optProbs, color='k', label = 'optT-# Particles = ' + str(numParticles)) # Connect the dots.
    # Plot tComp probabilities vs. s.
    plt.scatter(sVals,grndProbs, color='r') # Plot dots.
    plt.plot(sVals,grndProbs, color='r',ls = '-.', label = 'grndProb = ' + str(numParticles)) # Connect the dots.

    # Plot optimal evolution time probabilities vs. s.
    plt.scatter(sVals,grndOptProbs, color='r') # Plot dots.
    plt.plot(sVals,grndOptProbs, color='r', label = 'optGrndProb = ' + str(numParticles)) # Connect the dots.
    # Label plot for probability vs. s.
    plt.title('Probability vs. s')
    plt.xlabel('s')
    plt.ylabel('Probability')
    plt.legend()
    plt.show()
    return psiK, psiKOpt

numParticles,numClauses,N = getUserInput()

# Get timing stats for this code run.
cProfile.run('runDynamix(numParticles,numClauses,N)','dynamixStats')
p = pstats.Stats('dynamixStats')
p.strip_dirs()
p.sort_stats('cumulative').print_stats()