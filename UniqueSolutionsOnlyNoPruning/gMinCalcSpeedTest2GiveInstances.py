# Author: Kelsey C. Justis
# Date: 08/19/2015
# Summary: Program tests how fast minimal gap computer is.
# Input:
## n - Integer number of particles.
## m - Integer number of clauses.
# Output: 
##- times taken to compute minimal gap for various values of alpha.

# Import required libraries.
import numpy as np
from makeHd import makeHd
import time
import scipy.sparse.linalg as spla
import random
import numpy as np
import scipy.sparse as sp

# Create clauses to test.
def clauseListGen(clauseList, numParts):

     # Generate required number of clauses.
     for i in range(len(clauseList)):
          goodClause = 0

          # Generate a clause without redundancy/contradicting variables.
          while (goodClause == 0):
              clauseList[i] = ([random.randint(1,numParts),random.randint(1,numParts),random.randint(1,4) ])

              # Check if clause has duplicate state assignments to same particle .
              if (clauseList[i][0] != clauseList[i][1]):

                 # Make sure particles are ordered in clause by number.
                 if (clauseList[i][0] > clauseList[i][1]):
                     temp = clauseList[i][0]
                     clauseList[i][0] = clauseList[i][1]
                     clauseList[i][1] = temp

                 # Make sure clause is not already in list of clauses.
                 if ((clauseList[i] not in clauseList[0:i])):
                    goodClause = 1 # Clause makes sense.
     return clauseList

# Check to see how many clauses each assignment satisfies.
def makeHp(numParticles,numClauses):

    # Create list of assignments and clauses.
    clauseList = clauseListGen([0]*numClauses, numParticles)

    # Initialize vector for diagonal of Hp with each having no clauses satisfied.
    hp = [numClauses]*(2**numParticles)
    bestAssgs = [] #List to hold best assignments for given 2SAT problem instance.
    minNumClauseNotSat = numClauses

    # For each random assignment to test.
    for assgNum in range(2**numParticles):
        assg = bin(assgNum)[2:].zfill(numParticles) # Generate a new random assignment.

        # Test assignment against each clause.
        for clauseNum in range(numClauses):
            clauseSat = 0
            clausePartState = clauseList[clauseNum][2]         # Get state of particle in clause.

            if (clausePartState == 1):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 2):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1
            elif (clausePartState == 3):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 4):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1

            # Put number of clauses unsatisfied by assignment, i, on Hp diagonal entry, i.
            # If assignment satisfied this clause decrease the count for how many clauses it does not satisfy.
            hp[assgNum] -= clauseSat

        # Keep record of assignment(s) satisfying the most clauses.
        # Number of clauses satisfied has been seen before.
        if (hp[assgNum]  == minNumClauseNotSat):
            bestAssgs = np.hstack([bestAssgs, assgNum + 1])
        # New "low score".
        elif (hp[assgNum]  < minNumClauseNotSat):
            bestAssgs = [(assgNum + 1)]
            minNumClauseNotSat = hp[assgNum]
    return hp, minNumClauseNotSat, bestAssgs

# Get gap for given Hamiltonian.
def getGS(Hamiltonian):
    numEvals = 0# Initialize the number of eigenvalues found to 0.
    maxIter = 1000000 # Initialize the maximum number of iterations the solver should go through to find the desired eigenvalues.
    tolerance = 1E-4 # Initialize the tolerance desired before eigenvalue solver stops iterating to find more accurate eigenvalue.
                     # "Relative accuracy for eigenvalues (stopping criterion). The default value of 0 implies machine precision."

    # Try the ARPACK sparse eigenvalue solver function until we have two "good" eigenvalues.
    while (numEvals < 2):

        # Try to use solver with current stopping criterion (iterations and tolerance).
        try:
            # Get eigenvalues for ground and first excited state for given Hamiltonian.
            eVal1, eVal2 = np.sort(spla.eigsh(Hamiltonian, 2, which = 'SA', return_eigenvectors = False, tol = tolerance, maxiter = maxIter))

            # If we have no eigenvalues yet and the eigenvalue function worked.
            if (numEvals == 0):
                eValGrnd = eVal1 # Give the smaller of the given two from the eigenvalue function.
                eValExc = eVal2  # Give the greater of the given two from the eigenvalue function.

            # If we already have one eigenvalue and the eigenvalue function worked.
            elif(numEvals == 1):

                # Give eValGrnd the smallest of the previously found eigenvalue and the given two from the current eigenvalue function.
                # and give eValExc the second smallest.
                if (eVal1 == eValGrnd):
                    eValExc = eVal2
                elif (eVal1 > eValGrnd):
                    eValExc = eVal1
                else:
                    if(eVal2 < eValGrnd):
                        eValExc = eVal2
                    else:
                        eValExc = eValGrnd
                    eValGrnd = eVal1

            # Calculate the gap.
            gS = abs(eValExc - eValGrnd)

            # If the gap and the tolerance are within a magnitude of each other.
            if (gS/tolerance < 10.):
                tolerance = tolerance/10. # Make the tolerance smaller.
                numEvals = 0              # Recalculate eigenvalues with smaller tolerance.
            else:
                numEvals = 2  # We found eigenvalues with good enough precision.

        # For some reason the ARPACK eigenvalue solver function did not work.
        except spla.ArpackNoConvergence as err1:

            # Increase the number of iterations (time) it may take to try and find a "good" eigenvalue.
            maxIter = maxIter*10

            # If we the solver failed to provide two eigenvalues but found one eigenvalue.
            if (len(err1.eigenvalues) == 1):

                # If we do not have any eigenvalues.
                if (numEvals == 0):
                    eValGrnd = err1.eigenvalues[0] # Use this eigenvalue as the lowest eigenvalue found.

                # If we already have an eigenvalue.
                elif(numEvals == 1):
                    # Give eValGrnd the smallest of the previously found eigenvalue and the given one from the current eigenvalue function.
                    # and give eValExc the second smallest.
                    if (eValGrnd < err1.eigenvalues[0]):
                        eValExc = err1.eigenvalues[0]
                    else:
                        eValExc = eValGrnd
                        eValGrnd = err1.eigenvalues[0]

                # We found an eigenvalue.
                numEvals += 1

    # Return the gap found.
    return gS

# Finds tComp and optimal evolution time for given driver (Hd) and problem (Hp) Hamiltonians.
def runStatix(Hd,Hp):

    # Set initial values for gMin search process.
    gMin  = getGS(Hd)            # Get initial gap and use as gMin.
    acc = 0.0001               # Define accuracy desired as percent difference between sequential gMin values found.
    prevgMin = gMin/acc # Initialize old gMin value to a gMin value far outside our desired range of agreement

    # Initialize search grid over s.
    sDelta = 0.1  # Defines step between s values to search.
    sGMin = 0.5   # Set an initial s value (in center) for where gMin is located.
    i = 0
    # While the difference between subsequent gMin found is greater than our accuracy desired.
    while (abs((prevgMin - gMin)/prevgMin) > acc) or (i<5):
        prevgMin = gMin # Record the old gMin that has been succeeded.
        # For each of the s values in the range of s around our suspected location of gMin.
        for s in np.arange(max((sGMin - sDelta*10),0),(min((sGMin + sDelta*10),1)) + sDelta, sDelta):
           gS = getGS(((1 - s)*Hd + s*Hp)) # Get gap for current s value.

           # Check if this gap is the smallest found in this range.
           if (gS < gMin):

               # Update this iteration's discovered gMin and record its location in the range of s.
               gMin = gS
               sGMin = s
        sDelta = sDelta/10
        i +=1

    # Return the gMin and optimal evolution time found for this alpha.
    return gMin

# Make a 2SAT problem instance with a unique solution for a given n and m.
def makeProblemInstance(numParticles, numClauses):

    # Initialize count of Hp with unique solution for current alpha.
    uniHpFound = False

    # Make sure we enough Hp for given alpha.
    while ((uniHpFound == False)):

        # Create a new Hp with given number of particles and clauses.
        hpResults = makeHp(numParticles,numClauses)

        # If a unique solution Hp get gMin.
        if (len(hpResults[2]) == 1):
            Hp = hpResults[0] # Get current Hp instance.
            uniHpFound = True
    return Hp

# Evolves Hamiltonian from initial Hd to H(T).
def runSpeedTest(numParticles,numClauses):
    # Make Hd
    Hd = makeHd(numParticles)

    print '\n#Particles = ' + str(numParticles)+'\n#Clauses = '+str(numClauses)
    # Make a 2SAT problem instance with a unique solution.
    Hp = makeProblemInstance(numParticles, numClauses)
    print 'Hp is:\n' + str(Hp)
    Hp = sp.diags(Hp,0)
    # Start clock.
    startTime = time.time()
    gMin = runStatix(Hd,Hp) # Compute minimal gap.
    endTime = time.time()-startTime # Stop clock.

    # Record results.
    print("\n%s seconds to compute minimum gap = " % (endTime)) + str(gMin)


# 14 Particles.
runSpeedTest(14,21)# Alpha = 1.5
runSpeedTest(14,28)# Alpha = 2
runSpeedTest(14,36)# Alpha = ~10%AlphaMax
runSpeedTest(14,42)# Alpha = 3
runSpeedTest(14,72)# Alpha = ~20%AlphaMax
runSpeedTest(14,91)# Alpha = 25%AlphaMax
runSpeedTest(14,144)# Alpha = ~40%AlphaMax
runSpeedTest(14,182)# Alpha = 50%AlphaMax
runSpeedTest(14,273)# Alpha = 75%AlphaMax
runSpeedTest(14,288)# Alpha = ~80%AlphaMax
runSpeedTest(14,327)# Alpha = ~90%AlphaMax