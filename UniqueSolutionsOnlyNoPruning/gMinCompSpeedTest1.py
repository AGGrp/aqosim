# Author: Kelsey C. Justis
# Date: 08/19/2015
# Summary: Program tests how fast minimal gap computer is.
# Input:
## n - Integer number of particles.
## m - Integer number of clauses.
# Output: 
##- times taken to compute minimal gap for various values of alpha.

# Import required libraries.
import numpy as np
from makeHp import makeHp
from makeHd import makeHd
import time
import scipy.sparse.linalg as spla

# Get gap for given Hamiltonian.
def getGS(Hamiltonian):
    numEvals = 0# Initialize the number of eigenvalues found to 0.
    maxIter = 1000000 # Initialize the maximum number of iterations the solver should go through to find the desired eigenvalues.
    tolerance = 1E-4 # Initialize the tolerance desired before eigenvalue solver stops iterating to find more accurate eigenvalue.
                     # "Relative accuracy for eigenvalues (stopping criterion). The default value of 0 implies machine precision."

    # Try the ARPACK sparse eigenvalue solver function until we have two "good" eigenvalues.
    while (numEvals < 2):

        # Try to use solver with current stopping criterion (iterations and tolerance).
        try:
            # Get eigenvalues for ground and first excited state for given Hamiltonian.
            eVal1, eVal2 = np.sort(spla.eigsh(Hamiltonian, 2, which = 'SA', return_eigenvectors = False, tol = tolerance, maxiter = maxIter))

            # If we have no eigenvalues yet and the eigenvalue function worked.
            if (numEvals == 0):
                eValGrnd = eVal1 # Give the smaller of the given two from the eigenvalue function.
                eValExc = eVal2  # Give the greater of the given two from the eigenvalue function.

            # If we already have one eigenvalue and the eigenvalue function worked.
            elif(numEvals == 1):

                # Give eValGrnd the smallest of the previously found eigenvalue and the given two from the current eigenvalue function.
                # and give eValExc the second smallest.
                if (eVal1 == eValGrnd):
                    eValExc = eVal2
                elif (eVal1 > eValGrnd):
                    eValExc = eVal1
                else:
                    if(eVal2 < eValGrnd):
                        eValExc = eVal2
                    else:
                        eValExc = eValGrnd
                    eValGrnd = eVal1

            # Calculate the gap.
            gS = abs(eValExc - eValGrnd)

            # If the gap and the tolerance are within a magnitude of each other.
            if (gS/tolerance < 10.):
                tolerance = tolerance/10. # Make the tolerance smaller.
                numEvals = 0              # Recalculate eigenvalues with smaller tolerance.
            else:
                numEvals = 2  # We found eigenvalues with good enough precision.

        # For some reason the ARPACK eigenvalue solver function did not work.
        except spla.ArpackNoConvergence as err1:

            # Increase the number of iterations (time) it may take to try and find a "good" eigenvalue.
            maxIter = maxIter*10

            # If we the solver failed to provide two eigenvalues but found one eigenvalue.
            if (len(err1.eigenvalues) == 1):

                # If we do not have any eigenvalues.
                if (numEvals == 0):
                    eValGrnd = err1.eigenvalues[0] # Use this eigenvalue as the lowest eigenvalue found.

                # If we already have an eigenvalue.
                elif(numEvals == 1):
                    # Give eValGrnd the smallest of the previously found eigenvalue and the given one from the current eigenvalue function.
                    # and give eValExc the second smallest.
                    if (eValGrnd < err1.eigenvalues[0]):
                        eValExc = err1.eigenvalues[0]
                    else:
                        eValExc = eValGrnd
                        eValGrnd = err1.eigenvalues[0]

                # We found an eigenvalue.
                numEvals += 1

    # Return the gap found.
    return gS

# Finds tComp and optimal evolution time for given driver (Hd) and problem (Hp) Hamiltonians.
def runStatix(Hd,Hp):

    # Set initial values for gMin search process.
    gMin  = getGS(Hd)            # Get initial gap and use as gMin.
    acc = 0.0001               # Define accuracy desired as percent difference between sequential gMin values found.
    prevgMin = gMin/acc # Initialize old gMin value to a gMin value far outside our desired range of agreement

    # Initialize search grid over s.
    sDelta = 0.1  # Defines step between s values to search.
    sGMin = 0.5   # Set an initial s value (in center) for where gMin is located.
    i = 0
    # While the difference between subsequent gMin found is greater than our accuracy desired.
    while (abs((prevgMin - gMin)/prevgMin) > acc) or (i<5):
        prevgMin = gMin # Record the old gMin that has been succeeded.
        # For each of the s values in the range of s around our suspected location of gMin.
        for s in np.arange(max((sGMin - sDelta*10),0),(min((sGMin + sDelta*10),1)) + sDelta, sDelta):
           gS = getGS(((1 - s)*Hd + s*Hp)) # Get gap for current s value.

           # Check if this gap is the smallest found in this range.
           if (gS < gMin):

               # Update this iteration's discovered gMin and record its location in the range of s.
               gMin = gS
               sGMin = s
        sDelta = sDelta/10
        i +=1

    # Return the gMin and optimal evolution time found for this alpha.
    return gMin

# Make a 2SAT problem instance with a unique solution for a given n and m.
def makeProblemInstance(numParticles, numClauses):

    # Initialize count of Hp with unique solution for current alpha.
    uniHpFound = False

    # Make sure we enough Hp for given alpha.
    while ((uniHpFound == False)):

        # Create a new Hp with given number of particles and clauses.
        hpResults = makeHp(numParticles,numClauses)

        # If a unique solution Hp get gMin.
        if (len(hpResults[2]) == 1):
            Hp = hpResults[0] # Get current Hp instance.
            uniHpFound = True
    return Hp

# Evolves Hamiltonian from initial Hd to H(T).
def runSpeedTest(numParticles,numClauses,numInstances):
    # Make Hd
    Hd = makeHd(numParticles)

    # Initial variables to record speed.
    avgTime = 0.
    avgGMin = 0.
    print '\n#Particles = ' + str(numParticles)+'\n#Clauses = '+str(numClauses)

    # For number of instances desired.
    for instance in range(numInstances):
        # Make a 2SAT problem instance with a unique solution.
        Hp = makeProblemInstance(numParticles, numClauses)

        # Start clock.
        startTime = time.time()
        gMin = runStatix(Hd,Hp) # Compute minimal gap.
        endTime = time.time()-startTime # Stop clock.

        # Record results.
        print("%s seconds to compute minimum gap = " % (endTime)) + str(gMin) + ' for instance '+ str(instance + 1)
        avgTime += endTime
        avgGMin += gMin
        
    # Record average results.
    print'Average time per instance = ' + str(avgTime/numInstances)
    print'Average gMin = ' + str(avgGMin/numInstances)

numInstances = 10
#
# # 12 Particles.
# runSpeedTest(12,18,numInstances)# Alpha = 1.5
# runSpeedTest(12,24,numInstances)# Alpha = 2
# runSpeedTest(12,66,numInstances)# Alpha = 25%AlphaMax
# runSpeedTest(12,132,numInstances)# Alpha = 50%AlphaMax
# runSpeedTest(12,198,numInstances)# Alpha = 75%AlphaMax
#
# # 14 Particles.
# runSpeedTest(14,21,numInstances)# Alpha = 1.5
# runSpeedTest(14,28,numInstances)# Alpha = 2
# runSpeedTest(14,91,numInstances)# Alpha = 25%AlphaMax
# runSpeedTest(14,182,numInstances)# Alpha = 50%AlphaMax
# runSpeedTest(14,273,numInstances)# Alpha = 75%AlphaMax
#
# # 16 Particles.
# runSpeedTest(16,24,numInstances) # Alpha = 1.5
runSpeedTest(16,32,numInstances) # Alpha = 2
runSpeedTest(16,120,numInstances)# Alpha = 25%AlphaMax
runSpeedTest(16,240,numInstances)# Alpha = 50%AlphaMax
runSpeedTest(16,360,numInstances)# Alpha = 75%AlphaMax
