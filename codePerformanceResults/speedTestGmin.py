# Author: Kelsey C. Justis
# Date: 08/14/2015
# Summary: Program computes gMin for a given range of n, number of particles for all possible m clauses for each n.
# Then plots averaged tComp against alpha for all the Hp instances of the current alpha with a unique solution only.
# Input:
##- Minimum number of particles.
##- Maximum number of particles.
# Output: 
##- Plot of average tComp for alpha with unique solution Hps for every possible value of alpha in the given n range.

# %matplotlib inline
import numpy as np
from makeHp import makeHp
from makeHd import makeHd
import scipy.sparse.linalg as spla
import cProfile
import pstats
import matplotlib.pyplot as plt


# Get gap for given Hamiltonian.
def getGS(Hamiltonian):
    numEvals = 0# Initialize the number of eigenvalues found to 0.
    maxIter = 1000000 # Initialize the maximum number of iterations the solver should go through to find the desired eigenvalues.
    tolerance = 1E-4 # Initialize the tolerance desired before eigenvalue solver stops iterating to find more accurate eigenvalue.
                     # "Relative accuracy for eigenvalues (stopping criterion). The default value of 0 implies machine precision."

    # Try the ARPACK sparse eigenvalue solver function until we have two "good" eigenvalues.
    while (numEvals < 2):

        # Try to use solver with current stopping criterion (iterations and tolerance).
        try:
            # Get eigenvalues for ground and first excited state for given Hamiltonian.
            eVal1, eVal2 = np.sort(spla.eigsh(Hamiltonian, 2, which = 'SA', return_eigenvectors = False, tol = tolerance, maxiter = maxIter))

            # If we have no eigenvalues yet and the eigenvalue function worked.
            if (numEvals == 0):
                eValGrnd = min(eVal1,eVal2) # Give the smaller of the given two from the eigenvalue function.
                eValExc = max(eVal1,eVal2)  # Give the greater of the given two from the eigenvalue function.

            # If we already have one eigenvalue and the eigenvalue function worked.
            elif(numEvals == 1):
                eValGrnd = min(eValGrnd,eVal1,eVal2) # Give the smaller of the previously found eigenvalue and the given two from the current eigenvalue function.
                eValExc = max(eValGrnd,eVal1,eVal2)  # Give the greater of the previously found eigenvalue and the given two from the current eigenvalue function.

            # Calculate the gap.
            gS = abs(eValExc - eValGrnd)

            # If the gap and the tolerance are within a magnitude of each other.
            if (gS/tolerance < 10.):
                tolerance = tolerance/10. # Make the tolerance smaller.
                numEvals = 0              # Recalculate eigenvalues with smaller tolerance.
            else:
                numEvals = 2  # We found eigenvalues with good enough precision.

        # For some reason the ARPACK eigenvalue solver function did not work.
        except spla.ArpackNoConvergence as err1:

            # Increase the number of iterations (time) it may take to try and find a "good" eigenvalue.
            maxIter = maxIter*10

            # If we the solver failed toprovide two eigenvalues but found one eigenvalue.
            if (len(err1.eigenvalues) == 1):

                # If we do not have any eigenvalues.
                if (numEvals == 0):
                    eValGrnd = err1.eigenvalues[0] # Use this eigenvalue as the lowest eigenvalue found.

                # If we already have an eigenvalue.
                elif(numEvals == 1):
                    eValGrnd = min(eValGrnd,err1.eigenvalues[0]) # Give the smaller of the previously found eigenvalue and the given one from the current eigenvalue function.
                    eValExc = max(eValGrnd,err1.eigenvalues[0]) # Give the greater of the previously found eigenvalue and the given one from the current eigenvalue function.

                # We found an eigenvalue.
                numEvals += 1

    # Return the gap found.
    return gS

# Finds gMin for a given driver (Hd) and problem (Hp) Hamiltonians.
def getGMin(Hp, Hd):

    # Set initial values for gMin search process.
    gMin  = getGS(Hd)            # Get initial gap and use as gMin.
    optTEvol = 0.                # Optimal evolution time.
    acc = 0.0001               # Define accuracy desired as percent difference between sequential gMin values found.
    prevgMin = gMin/acc # Initialize old gMin value to a gMin value far outside our desired range of agreement

    # Initialize search grid over s.
    sDelta = 0.1  # Defines step between s values to search.
    sGMin = 0.5   # Set an initial s value (in center) for where gMin is located.

    i = 0

    # # Clear current figures and hold next drawings to come.
    # plt.cla()
    # plt.close("all")
    # plt.hold(1)
    #
    # gMins = [gMin]
    # sgMins = [sGMin]

    print gMin
    # While the difference between subsequent gMin found is greater than our accuracy desired.
    while (abs((prevgMin - gMin)/prevgMin) > acc) or (i<5):
        prevgMin = gMin # Record the old gMin that has been succeeded.
        # For each of the s values in the range of s around our suspected location of gMin.
        for s in np.arange(max((sGMin - sDelta*10),0),(min((sGMin + sDelta*10),1)) + sDelta, sDelta):
           gS = getGS(((1 - s)*Hd + s*Hp)) # Get gap for current s value.
           optTEvol += (sDelta)/(gS**2) # Update optTevol.
           print 'g(' + str(s) + '): ' + str(gS)
           # Check if this gap is the smallest found in this range.
           if (gS < gMin):

               # Update this iteration's discovered gMin and record its location in the range of s.
               gMin = gS
               sGMin = s

           # # Plot against alpha.
           # plt.figure(1)
           # # Plot tComp vs. alpha for current value of n.
           # plt.scatter(s,gS, color='k') # Plot dots.
           # plt.plot(s,gS, color='k',ls = '-.') # Connect the dots.

        sDelta = sDelta/10
        # gMins = np.hstack([gMins,gMin])
        # sgMins = np.hstack([sgMins,sGMin])
        i +=1

        print '\nprevGmin: ' + str(prevgMin) + '\ncurrGMin: ' + str(gMin)

    # plt.scatter(sgMins,gMins, color='r') # Plot dots.
    # plt.show()
    print 'gMin is ' + str(gMin)
    # Return the gMin found for this alpha.
    return gMin#, optTEvol

# Plots gMin vs alpha for given range of particles.
def plotGminVsAlpha(minNumParticles,maxNumParticles):
    gMin = 0
    for numParticles in range(minNumParticles,maxNumParticles +1):

        # Define initial and problem Hamiltonians.
        Hd = makeHd(numParticles)

        # Initialize number of clauses to check past alpha =1 where there are no unique instances.
        numClauses = numParticles + 25

         # Maximum number of unique clauses for given n, num of particles
        maxNumClauses = numClauses
        print numClauses
        # While we have not checked each possible m up to mMax.
        while (numClauses <= maxNumClauses):
            # print '\n#Particles = ' + str(numParticles) + '\n#Clauses = ' + str(numClauses)
            # Counter used to make sure enough instances are tried to ensure current alpha does not have an Hp with a unique solution.
            numInstancesTried = 0

            # Initialize count of Hp with unique solution for current alpha.
            numUniHp = 0

            # Make sure we enough Hp for given alpha.
            while ((numInstancesTried < 100) and (numUniHp < 1)):

                # Create a new Hp with given number of particles and clauses.
                hpResults = makeHp(numParticles,numClauses)

                # If a unique solution Hp get gMin.
                if ((len(hpResults[2])) == 1):
                    Hp = hpResults[0] # Get current Hp instance.
                    numUniHp = 1 # Increase count for number of Hp with unique solutions.
                    gMin = getGMin(Hp,Hd) # Get gMin for current Hp instance.

                # Increase number of instances tried.
                numInstancesTried += 1

            # Go to next appropriate alpha in tests.
            numClauses +=1
    return gMin

def main(numParticles,numClauses):

    # Define initial and problem Hamiltonians.
    Hd = makeHd(numParticles)

    # Maximum number of unique clauses for given n, num of particles
    maxNumClauses = numClauses + 1

    # While we have not checked each possible m up to mMax.
    while (numClauses < maxNumClauses):
        print '\n#Particles = ' + str(numParticles) + '\n#Clauses = ' + str(numClauses)

        # Counter used to make sure enough instances are tried to ensure current alpha does not have an Hp with a unique solution.
        numInstancesTried = 0

        # Initialize count of Hp with unique solution for current alpha.
        numUniHp = 0

        # Make sure we enough Hp for given alpha.
        while ((numInstancesTried < 100) and (numUniHp < 1)):

            # Create a new Hp with given number of particles and clauses.
            hpResults = makeHp(numParticles,numClauses)

            # If a unique solution Hp get gMin.
            if ((len(hpResults[2])) == 1):
                Hp = hpResults[0] # Get current Hp instance.
                numUniHp = 1 # Increase count for number of Hp with unique solutions.
                getGMin(Hp,Hd) # Get gMin for current Hp instance.

            # Increase number of instances tried.
            numInstancesTried += 1

        if (numInstancesTried == 100):
            print 'Bad Alpha'
        numClauses +=1
cProfile.run('main(15,90)','gMinStats')
p = pstats.Stats('gMinStats')
p.strip_dirs()
p.sort_stats('cumulative').print_stats(20)