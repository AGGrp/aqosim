# Author: Kelsey C. Justis
# Date: 08/07/2015
# Summary: Program computes gMin for a given range of n, number of particles for all possible m clauses for each n.
# Then plots averaged tComp against alpha for all the Hp instances of the current alpha with a unique solution only.
# Input:
##- Minimum number of particles.
##- Maximum number of particles.
# Output: 
##- Plot of average tComp for alpha with unique solution Hps for every possible value of alpha in the given n range.


import numpy as np
from makeHp import makeHp
from makeHd import makeHd
import scipy.sparse.linalg as spla
import cProfile
import pstats

def getGS(Htotal):
    numEvals = 0
    maxIter = 1000000
    tolerance = 1E-4
    # i = 1
    while (numEvals < 2):
        try:
            # Get initial eigenvalues for ground and excited state for the initial total Hamiltonian.
            eVal1, eVal2 = np.sort(spla.eigsh(Htotal, 2, which = 'SA', return_eigenvectors = False, tol = tolerance, maxiter = maxIter))
            if (numEvals == 0):
                eValGrnd = min(eVal1,eVal2)
                eValExc = max(eVal1,eVal2)
            elif(numEvals == 1):
                eValGrnd = min(eValGrnd,eVal1,eVal2)
                eValExc = max(eValGrnd,eVal1,eVal2)

            gS = abs(eValExc - eValGrnd)
            if (gS/tolerance < 10.):
                tolerance = tolerance/10.
                numEvals = 0
            else:
                numEvals = 2
        except spla.ArpackNoConvergence as err1:
            # print str(i) + ' convergence exception occurred'
            # i +=1
            maxIter = maxIter*10
            if (len(err1.eigenvalues) == 1):
                if (numEvals == 0):
                    eValGrnd = err1.eigenvalues[0]

                elif(numEvals == 1):
                    eValGrnd = min(eValGrnd,err1.eigenvalues[0])
                    eValExc = max(eValGrnd,err1.eigenvalues[0])
                numEvals += 1
    return gS

# Finds gMin for a given driver (Hd) and problem (Hp) Hamiltonians.
def getGMin(Hp, Hd):

    # Hp = identity(2**15)-identity(2**15)

    # Set initial values for gMin search process.
    gMin  = getGS(Hd) # Get initial gap and use as gMin.
    optTEvol = 0.                        # Optimal tEvol.
    perAccGmin = 0.0001        # Define accuracy desired as percent difference between sequential gMin values found.
    gMinNew = gMin                   # Initialize the new gMin value to the initial gMin.
    gMinOld = gMinNew/perAccGmin     # Initialize old gMin value to a gMin value far outside our desired range of agreement

    # Initialize search grid over s.
    sDelta = 0.1            # Defines step between s values to search.
    sGMin = 0.5              # Set an initial s value for where gMin is located.

    i = 0
    # While the difference between subsequent gMin found is greater than our accuracy desired.
    while (abs((gMinOld - gMinNew)/gMinNew) > perAccGmin) and (i<5):

        # Set our new old gMin value found to the previous iteration.
        gMinOld = gMinNew
        gMin = gMinNew

        # For each of the s values in the range of s around our suspected location of gMin.
        for s in np.arange(max((sGMin - sDelta*10),0),(min((sGMin + sDelta*10),1)) + sDelta, sDelta):
           gS = getGS(((1 - s)*Hd + s*Hp))

           # Update optTevol.
           optTEvol += (sDelta)/(gS**2)

           # Check if this gap is the smallest found in this range.
           if (gS < gMin):

               # Assign a new gMin and its location in the range of s.
               gMin = gS
               sGMin = s

        # Update this iteration's discovered gMin.
        gMinNew = gMin
        sDelta = sDelta/10
        i +=1
    # print 'gMin is ' + str(gMin)
    # Return the gMin found for this alpha.
    return gMin#, optTEvol

# Plots gMin vs alpha for given range of particles.
def plotGminVsAlpha(minNumParticles,maxNumParticles):
    gMin = 0
    for numParticles in range(minNumParticles,maxNumParticles +1):

        # Define initial and problem Hamiltonians.
        Hd = makeHd(numParticles)

        # Initialize number of clauses to check past alpha =1 where there are no unique instances.
        numClauses = numParticles + 25

         # Maximum number of unique clauses for given n, num of particles
        maxNumClauses = numClauses
        print numClauses
        # While we have not checked each possible m up to mMax.
        while (numClauses <= maxNumClauses):
            # print '\n#Particles = ' + str(numParticles) + '\n#Clauses = ' + str(numClauses)
            # Counter used to make sure enough instances are tried to ensure current alpha does not have an Hp with a unique solution.
            numInstancesTried = 0

            # Initialize count of Hp with unique solution for current alpha.
            numUniHp = 0

            # Make sure we enough Hp for given alpha.
            while ((numInstancesTried < 100) and (numUniHp < 1)):

                # Create a new Hp with given number of particles and clauses.
                hpResults = makeHp(numParticles,numClauses)

                # If a unique solution Hp get gMin.
                if ((len(hpResults[2])) == 1):
                    Hp = hpResults[0] # Get current Hp instance.
                    numUniHp = 1 # Increase count for number of Hp with unique solutions.
                    gMin = getGMin(Hp,Hd) # Get gMin for current Hp instance.

                # Increase number of instances tried.
                numInstancesTried += 1

            # Go to next appropriate alpha in tests.
            numClauses +=1
    return gMin
def main():
    # Get range of particle numbers to test over.
    minNumParticles = 15
    maxNumParticles = minNumParticles
    print minNumParticles
    avg =0.
    i = 0
    while (i<1):

        # Plot gMin vs alpha for request range on n.
        gMin = plotGminVsAlpha(minNumParticles,maxNumParticles)
        if (gMin > 0 ):
            avg += gMin
            i+=1

cProfile.run('main()','gMinStats')
p = pstats.Stats('gMinStats')
p.strip_dirs()
p.sort_stats('cumulative').print_stats(20)