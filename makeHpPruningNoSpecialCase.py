# Author: Kelsey C. Justis
# Date: 08/21/2015
# Summary: Program creates a sparse Hp matrix which has the number of clauses each assignment did not satisfy along the diagonal
# such that Hp(i,i) = number of clauses not satisfied by assignment,i.
# Performs pruning of particles appearing in only one clause or appears with the same state in all of its involved clauses.
# Input: 
#-n: integer number of particles
#-m: integer number of clauses
# Output:
#-Sparse Hp

import random
import numpy as np
import scipy.sparse as sp


# Check to see how many clauses each assignment satisfies.
def makeHp(numParticles,numClauses):

    # Create list of assignments and clauses.
    clauseList = clauseListGen([0]*numClauses, numParticles)

    # Initialize vector for diagonal of Hp with each having no clauses satisfied.
    hp = [numClauses]*(2**numParticles)
    bestAssgs = [] #List to hold best assignments for given 2SAT problem instance.
    minNumClauseNotSat = numClauses

    # For each random assignment to test.
    for assgNum in range(2**numParticles):
        assg = bin(assgNum)[2:].zfill(numParticles) # Generate a new random assignment.

        # Test assignment against each clause.
        for clauseNum in range(numClauses):
            clauseSat = 0
            clausePartState = clauseList[clauseNum][2]         # Get state of particle in clause.

            if (clausePartState == 1):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 2):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1
            elif (clausePartState == 3):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 4):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1

            # Put number of clauses unsatisfied by assignment, i, on Hp diagonal entry, i.
            # If assignment satisfied this clause decrease the count for how many clauses it does not satisfy.
            hp[assgNum] -= clauseSat

        # Keep record of assignment(s) satisfying the most clauses.
        # Number of clauses satisfied has been seen before.
        if (hp[assgNum]  == minNumClauseNotSat):
            bestAssgs = np.hstack([bestAssgs, assgNum + 1])
        # New "low score".
        elif (hp[assgNum]  < minNumClauseNotSat):
            bestAssgs = [(assgNum + 1)]
            minNumClauseNotSat = hp[assgNum]
    hp = sp.diags(hp,0)
    return hp, minNumClauseNotSat, bestAssgs

numClauses = 150
numParticles = 200
clauseList = clauseListGen([0]*numClauses, numParticles)
prunedClauseMatrix, newNumClauses, newNumParticles = pruneClauseList(clauseList,numClauses,numParticles)
print '\nFinal clause matrix is:\n'+str(prunedClauseMatrix)
print '\nOld m = '+str(numClauses)+'\nNew m = '+str(newNumClauses)+'\nNew n = '+str(newNumParticles)
# hp, minNumClauseNotSat, bestAssgs = makeHp(numParticles,newNumClauses)
#
# print'Hp:\n'+str(hp.toarray())+'\n\nSmallest # of clauses not satisfied = '+str(minNumClauseNotSat)+\
#      '\n'+str(len(bestAssgs))+' Assignments had this #; they are:\n'+str(bestAssgs)