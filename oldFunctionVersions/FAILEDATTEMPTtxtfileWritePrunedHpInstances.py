# Author: Kelsey C. Justis
# Date: 08/21/2015
# Summary: Program creates a sparse Hp matrix which has the number of clauses each assignment did not satisfy along the diagonal
# such that Hp(i,i) = number of clauses not satisfied by assignment,i.
# Performs pruning of particles appearing in only one clause or appears with the same state in all of its involved clauses.
# Input: 
#-n: integer number of particles
#-m: integer number of clauses
# Output:
#-Sparse Hp

import random
import numpy as np
import scipy.sparse as sp
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import datetime
import os
import errno

def openRecords(path):
    try:
        newMs = open(path+'/newMs','w')
        newNs = open(path+'/newNs','w')
        newAlphas = open(path+'/newAlphas','w')
        clauseLists = open(path+'/clauseLists','w')
        prunedClauseLists = open(path+'/prunedClauseLists','w')
        hpLists = open(path+'/hpLists','w')
        oldNumSols = open(path+'/oldNumSols','w')
        newNumSols = open(path+'/newNumSols','w')
        bestAssgs = open(path+'/bestAssgs','w')
        avgNewM = open(path+'/avgNewM','w')
        avgNewN = open(path+'/avgNewN','w')
        avgNewAlpha = open(path+'/avgNewAlpha','w')

    except IOError:
        os.makedirs(str(path))
        newMs = open(path+'/newMs','w')
        newNs = open(path+'/newNs','w')
        newAlphas = open(path+'/newAlphas','w')
        clauseLists = open(path+'/clauseLists','w')
        prunedClauseLists = open(path+'/prunedClauseLists','w')
        hpLists = open(path+'/hpLists','w')
        oldNumSols = open(path+'/oldNumSols','w')
        newNumSols = open(path+'/newNumSols','w')
        bestAssgs = open(path+'/bestAssgs','w')
        avgNewM = open(path+'/avgNewM','w')
        avgNewN = open(path+'/avgNewN','w')
        avgNewAlpha = open(path+'/avgNewAlpha','w')
    except OSError:
        os.makedirs(str(path))
        newMs = open(path+'/newMs','w')
        newNs = open(path+'/newNs','w')
        newAlphas = open(path+'/newAlphas','w')
        clauseLists = open(path+'/clauseLists','w')
        prunedClauseLists = open(path+'/prunedClauseLists','w')
        hpLists = open(path+'/hpLists','w')
        oldNumSols = open(path+'/oldNumSols','w')
        newNumSols = open(path+'/newNumSols','w')
        bestAssgs = open(path+'/bestAssgs','w')
        avgNewM = open(path+'/avgNewM','w')
        avgNewN = open(path+'/avgNewN','w')
        avgNewAlpha = open(path+'/avgNewAlpha','w')

    # Return opened files.
    return newMs,newNs,newAlphas,clauseLists,prunedClauseLists,hpLists,oldNumSols,newNumSols,bestAssgs,avgNewM,avgNewN,avgNewAlpha
def closeRecords(fileNameArray):
    for fileName in fileNameArray:
        fileName.close()
# Create clauses to test.
def clauseListGen(clauseList, numParts):

     # Generate required number of clauses.
     for i in range(len(clauseList)):
          goodClause = 0

          # Generate a clause without redundancy/contradicting variables.
          while (goodClause == 0):
              clauseList[i] = ([((-1)**random.randint(0,1))*(random.randint(1,numParts)),\
                            ((-1)**random.randint(0,1))*(random.randint(1,numParts))])

              # Check if clause has duplicate state assignments to same particle .
              if (abs(clauseList[i][0])!=abs(clauseList[i][1])):

                 # Make sure particles are ordered in clause by number.
                 if (abs(clauseList[i][0]) > abs(clauseList[i][1])):
                     temp = clauseList[i][0]
                     clauseList[i][0] = clauseList[i][1]
                     clauseList[i][1] = temp

                 # Make sure clause is not already in list of clauses.
                 if ((clauseList[i] not in clauseList[0:i])):
                    goodClause = 1 # Clause makes sense.
     return clauseList
# Prune Clause List.
def pruneClauseList(clauseList,numClauses,numParticles):

    # Build clause matrix from clause list.
    clauseMatrix = np.zeros((numClauses,numParticles),dtype = int)
    for clauseNum in range(numClauses):
        var1 = clauseList[clauseNum][0]
        var2 = clauseList[clauseNum][1]
        clauseMatrix[clauseNum,abs(var1)-1] = var1
        clauseMatrix[clauseNum,abs(var2)-1] = var2

    # Prune matrix if necessary
    donePruning = False
    initialClauseMatrix = clauseMatrix

    # Keep pruning until we can't prune anymore.
    while (donePruning == False):

        currParticle= 0
        prunedClauseMatrix = initialClauseMatrix

        # While there are still multiple particle columns in matrix and we are not in the last particle column, and there are multiple clauses.
        while ((prunedClauseMatrix.shape[1]!=0) and (currParticle < prunedClauseMatrix.shape[1])and (prunedClauseMatrix.shape[0]!=1)):

            # The frequency of a particle number regardless of state.
            partFreq = sum(abs(np.sign(prunedClauseMatrix[:,currParticle])))

            # The frequency of a particle with state accounted for.
            partFreqWithState = sum(np.sign(prunedClauseMatrix[:,currParticle]))

            # If the particle appears in only one clause.
            if (partFreq == 1):

                # Find the clause it appears in.
                clauseRow = np.where(abs(prunedClauseMatrix[:,currParticle])!=0)[0][0]

                # Delete that clause.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,clauseRow, axis=0)

                # Delete the column for that particle number.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)

            # The current particle is not present in any clause.
            elif (partFreq == 0):

                # Delete the column for that particle number.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)

            # If the particle appear more than once and always the same state.
            elif (partFreq == abs(partFreqWithState) != 0):

                    # Find the clauses it appears in.
                    clauseRows = np.where(abs(prunedClauseMatrix[:,currParticle])!=0)[0]


                    # Delete those clauses.
                    prunedClauseMatrix = np.delete(prunedClauseMatrix,clauseRows, axis=0)

                    # Delete the column for that particle number.
                    prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)


            # Go on to the next particle in the matrix.
            currParticle +=1

        # Check to see if anything has been pruned off, if not we must be finished, if so check to see if can prune anymore.
        if (initialClauseMatrix.size == prunedClauseMatrix.size):
            donePruning = True
        else:
            initialClauseMatrix = prunedClauseMatrix

    numClauses = prunedClauseMatrix.shape[0]
    if (numClauses==1):
        prunedClauseMatrix = [[]]#np.delete(prunedClauseMatrix,0, axis=0)
        numParts = 0
        numClauses  =0
    else:
        prunedClauseMatrix = prunedClauseMatrix[np.nonzero(prunedClauseMatrix)]
        prunedClauseMatrix = np.reshape(prunedClauseMatrix,(numClauses,2))
        numParts = len(np.unique(abs(prunedClauseMatrix)))
        # Reformat for use with rest of code.
        prunedClauseMatrix = np.hstack([prunedClauseMatrix,np.zeros((numClauses,1),dtype = int)])
        for clauseNum in range(numClauses):
            var1 = prunedClauseMatrix[clauseNum][0]
            var2 = prunedClauseMatrix[clauseNum][1]
            var1State = np.sign(var1)
            var2State = np.sign(var2)

            if (var1State == 1):
                if (var2State == 1):
                    prunedClauseMatrix[clauseNum][2] = 1
                else:
                    prunedClauseMatrix[clauseNum][1] = var2State*var2
                    prunedClauseMatrix[clauseNum][2] = 2
            else:
                prunedClauseMatrix[clauseNum][0] = var1State*var1
                if (var2State == 1):
                    prunedClauseMatrix[clauseNum][2] = 3
                else:
                    prunedClauseMatrix[clauseNum][1] = var2State*var2
                    prunedClauseMatrix[clauseNum][2] = 4
    return  prunedClauseMatrix, numClauses,numParts

def charKernel(numInstances):
    particleNumList = [4,10]
    numVals = 50 # Number of alpha values in alpha range of given n to plot.

    # For each particle number up to n.
    for particleNum in particleNumList:

        # Maximum number of unique clauses depending on n.
        mMax = 2*particleNum

        # Make sure we check clauses spread out over range of possible alpha between 0 and 2 for the current particle number value, n.
        if (mMax <= numVals):
            clauseNumList = np.linspace(1,mMax,mMax,dtype = int)
        else:
            clauseNumList = np.linspace(1,mMax,numVals,dtype = int)

        # For each clause up to mMax.
        for numClauses in clauseNumList:

            # Initialize variables o hold values found.
            alpha  = float(float(numClauses)/float(particleNum)) # Alpha
            newMList = []
            newNList = []
            newAlphaList = []
            avgNewM1 = 0.
            avgNewN1 = 0.
            avgNewAlpha1 = 0.

            # Where to save results.
            path  = 'PrunedHpInstances/i'+str(numInstances) + '/n'+str(particleNum) + '/m'+str(numClauses)

            # Open files for writing.
            newMs,newNs,newAlphas,clauseLists,prunedClauseLists,hpLists,oldNumSols,newNumSols,bestAssgs,avgNewM,avgNewN,avgNewAlpha = openRecords(path)

            # Generate the number of instances desired for stats requested.
            for sample in range(numInstances):
                clauseList= clauseListGen([0]*numClauses, particleNum)
                prunedClauseList,newM,newN = pruneClauseList(clauseList,numClauses,particleNum)
                # If no empty kernel.
                if (newN == 0):
                    newM = 0
                    newAlpha = 0.
                    hpList = []
                    minNumClauseNotSat= 0
                    bestAssgList = []
                else:
                    newAlpha = float(newM)/newN
                    hpList, minNumClauseNotSat, bestAssgList = makeHp(particleNum,newM,prunedClauseList)
                # print '\n\nSAMPLEclauseList:'+str(clauseList)+'\nprunClauseList:'+str(prunedClauseList)+'\nnewM: '+str(newM)+'\nnewN: '+str(newN)+'\nnewAlpha: '\
                #       +str(newAlpha)+'\nhpList: '+str(hpList)+'\nminNumCl: '+str(minNumClauseNotSat)+'\nbestAssgs: '+str(bestAssgList)

                # Record values found.
                newMList = np.hstack([newMList,newM])
                newNList = np.hstack([newNList,newN])
                newAlphaList = np.hstack([newAlphaList,newAlpha])

                newMs.write(str(newM)+'\n')
                newNs.write(str(newN)+'\n')
                newAlphas.write(str(newAlpha)+'\n')
                clauseLists.write(str(clauseList)+'\n')
                prunedClauseLists.write(str(prunedClauseList)+'\n')
                hpLists.write(str(hpList)+'\n')
                oldNumSols.write(str(newM)+'\n')
                newNumSols.write(str(newM)+'\n')
                bestAssgs.write(str(bestAssgList)+'\n')

                # Sum up values found.
                avgNewM1 += newM
                avgNewN1 += newN
                avgNewAlpha1 += newAlpha


            # Get averages of values found.
            avgNewM1 = avgNewM1/numInstances
            avgNewN1 = avgNewN1/numInstances
            avgNewAlpha1 = avgNewAlpha1/numInstances

            # Get standard deviation scaled by number of samples/instances for values found.
            newMsSTD = np.std(newMList)/(numInstances**(1/2))
            newNsSTD = np.std(newNList)/(numInstances**(1/2))
            newAlphaSTD = np.std(newAlphaList)/(numInstances**(1/2))

            # Record averages.
            avgNewM.write(str(avgNewM1)+'\n')
            avgNewN.write(str(avgNewN1)+'\n')
            avgNewAlpha.write(str(avgNewAlpha1)+'\n')
            closeRecords([newMs,newNs,newAlphas,clauseLists,prunedClauseLists,hpLists,oldNumSols,newNumSols,bestAssgs,avgNewM,avgNewN,avgNewAlpha])

            # # Print results for this value of alpha to console.
            # print '\n#Particles = ' + str(particleNum) + '\n#Clauses = ' + str(numClauses)+\
            #       '\nAvg New m = '+str(avgNewM) +' with STD ' + str(newMsSTD)+\
            #       '\nAvg New n = '+str(avgNewM) +' with STD ' + str(newNsSTD)+\
            #       '\nAvg New alpha = '+str(avgNewAlpha) +' with STD ' + str(newAlphaSTD)

# Check to see how many clauses each assignment satisfies.
def makeHp(numParticles,numClauses,clauseList):


    # Initialize vector for diagonal of Hp with each having no clauses satisfied.
    hp = [numClauses]*(2**numParticles)
    bestAssgs = [] #List to hold best assignments for given 2SAT problem instance.
    minNumClauseNotSat = numClauses

    # For each random assignment to test.
    for assgNum in range(2**numParticles):
        assg = bin(assgNum)[2:].zfill(numParticles) # Generate a new random assignment.

        # Test assignment against each clause.
        for clauseNum in range(numClauses):
            clauseSat = 0
            clausePartState = clauseList[clauseNum][2]         # Get state of particle in clause.

            if (clausePartState == 1):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 2):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1
            elif (clausePartState == 3):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 4):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1

            # Put number of clauses unsatisfied by assignment, i, on Hp diagonal entry, i.
            # If assignment satisfied this clause decrease the count for how many clauses it does not satisfy.
            hp[assgNum] -= clauseSat

        # Keep record of assignment(s) satisfying the most clauses.
        # Number of clauses satisfied has been seen before.
        if (hp[assgNum]  == minNumClauseNotSat):
            bestAssgs = np.hstack([bestAssgs, assgNum + 1])
        # New "low score".
        elif (hp[assgNum]  < minNumClauseNotSat):
            bestAssgs = [(assgNum + 1)]
            minNumClauseNotSat = hp[assgNum]
    bestAssgsList = [0]*(len(bestAssgs))
    for i in range(len(bestAssgs)):
        bestAssgsList[i] = [bestAssgs[i]]
    bestAssgs = bestAssgsList
    hp = sp.diags(hp,0)
    return hp, minNumClauseNotSat, bestAssgs

numInstances =  10
charKernel(numInstances)
