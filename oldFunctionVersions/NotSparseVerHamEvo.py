# Author: Kelsey C. Justis
# Date: 07/24/2015
# Summary: Not sparse program computes evolved state under adiabatic evolution.
# Input:
##- Hd (Matrix): Driver hamiltonian
##- Hp (Matrix): Problem hamiltonian
##- N (Integer): Number of steps 
## -T (Double time [seconds]): Final time
## -psiI (Vector): Initial state vector
##- H(t): Time function
#
# Output: 
##- psiT (Vector): Evolved state ket at time, T.

import math
import time
import numpy as np
import scipy.sparse.linalg as la
from makeHp import makeHp
from makeHd import makeHd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import scipy.sparse as sp

# Finds gMin for a given alpha.
def getGMin(numParticles, numClauses):

    # Define initial and problem Hamiltonians.
    Hd = makeHd(numParticles)

    # Create a new Hp with given number of particles and clauses.
    hpResults = makeHp(numParticles,numClauses)

    # Get number of assignments with this number of unsatisfied assignments.
    numBestAssgs = len(hpResults[2])

    # Counter used to make sure enough instances are tried to ensure current alpha does not have an Hp with a unique solution.
    numInstancesTried = 0

    # Make sure we choose a Hp with a unique solution.
    while (numBestAssgs > 1):

        # Create a new Hp with given number of particles and clauses.
        hpResults = makeHp(numParticles,numClauses)

        # Get number of assignments with this number of unsatisfied assignments.
        numBestAssgs = len(hpResults[2])

        # Increase number of instances tried.
        numInstancesTried += 1

        # Check to see if we really tried.
        if (numInstancesTried == 1000):
            return 'No Hp instance found with a unique solution; please try a different alpha.'

    # Get Hp.
    Hp = hpResults[0]
    Htotal = Hd
    print Htotal
    # Get initial eigenvalues for ground and excited state for the initial total Hamiltonian.
    eValGrnd, eValExc  = la.eigsh(Htotal,2, which = 'SA',sigma = 0, return_eigenvectors = False)
    print la.eigsh(Htotal,2, which = 'SA',sigma = 0, return_eigenvectors = False)
    # Set initial values for gMin search process.
    gMin  = float(abs(eValExc - eValGrnd))  # Get initial gap and use as gMin.
    perAccGmin = float(0.0001)        # Define accuracy desired as percent difference between sequential gMin values found.
    gMinNew = float(gMin)                   # Initialize the new gMin value to the initial gMin.
    gMinOld = float(gMinNew/perAccGmin)     # Initialize old gMin value to a gMin value far outside our desired range of agreement
                                        #  with the new gMin value. (Strictly to go in the while loop at least once.)
    sMin = float(0)                  # Initial lower s to search from.
    sMax = float(1)                  # Initial higher s bound to search to.
    sDelta = float(0.1)             # Defines step between s values to search.
    sGMin = float(0.5)               # Set an initial s value for where gMin is located.

    # Creat figure to plot gMin convergence.
    plt.hold(1)
    plt.figure(1)

    iterNum = 1
    print gMinOld,gMinNew, gMin

    # While the difference between subsequent gMin found is greater than our accuracy desired.
    while (abs((gMinOld - gMinNew)/gMinNew) >= perAccGmin):

        # Set our new old gMin value found to the previous iteration.
        gMinOld = gMinNew
        # print gMinOld
        # Set gMin to our best gMin value found so far.
        gMin = gMinNew

        # For each of the s values in the range of s around our suspected location of gMin.
        for s in np.arange(sMin,sMax + sDelta, sDelta):

           # Get Htotal for the current s value.
           Htotal = (1 - s)*Hd + s*Hp # A typical Hamiltonian of the form H(t) = (1-s)Hd + sHp.
           print Htotal

           # Get eigen values of H(s) and find the gap between.
           eValGrnd, eValExc  = la.eigsh(Htotal,2, which = 'SA',sigma = 0, return_eigenvectors = False)
           print la.eigsh(Htotal,2, which = 'SA')[0]
           gS = abs(eValExc - eValGrnd)
           print 'Curr g(s) = g(' + str(s) + ') ' + str(gS)

           # Check if this gap is the smallest found in this range.
           if (gS < gMin):

               # Assign a new gMin and its location in the range of s.
               gMin = gS
               sGMin = s

           if (gS > gMin):
               plt.scatter(s,gS, color = 'k')
               plt.plot(s,gS, color = 'k')


        plt.annotate(str(iterNum), xy=(sGMin, gMin), xytext=(sGMin , gMin + 0.0005))
        plt.scatter(sGMin,gMin, color = 'r')
        plt.plot(sGMin,gMin,  color = 'r')
        print 'gMin for iter. ' + str(iterNum) + ' is ' + str(gMin) +'\n'
        # Update this iteration's discovered gMin.
        gMinNew = gMin
        # print gMinNew
        # Update the range and spacing of s values to search over.
        sMin = sGMin - sDelta
        sMax = sGMin + sDelta

        if (sMax > 1.):
            sMax = 1.
        if (sMin < 0.):
            sMin = 0.
        sDelta = sDelta/10
        iterNum += 1

    # Return the gMin found for this alpha.
    return gMin

# Get from user the number of particles, clauses, and random assignments to try.
numParticles = input('Enter number of particles/spins in the system: \n')
numClauses = input('Enter number of clauses to test: \n')

# Check to makes sure number of clauses is possible to make.
while (numClauses > (2*math.factorial(numParticles)/ math.factorial(numParticles - 2))):
    numClauses = input('***Number of clauses entered exceeds number of unique clauses***. \n'
                       'Enter a number less than 2*factorial(number of particles)/(factorial(number of particles - 2): \n')

# Pauli matrices.
sigX = np.array([[0,1], [1,0]])
sigY = np.array([[0,-1j], [1j,0]])
sigZ = np.array([[1,0], [0,-1]])

start_time = time.time()

print '\nFinal gMin:\n' + str(getGMin(numParticles, numClauses))
#
# def getGMinRange(numParticles, numClauses):
#     alphaMin =
#     alphaMax =
#     alphaDelta =
#     for (alpha in xrange(alphaMin,alphaMax + alphaDelta, alphaDelta)):

print("--- %s seconds ---" % (time.time() - start_time))

# Label plot for gS vs. s.
plt.figure(1)
plt.title('g(s) vs.s')
plt.xlabel('s value')
plt.ylabel('g(s)')
plt.show()