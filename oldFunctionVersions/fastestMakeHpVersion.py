# Author: Kelsey C. Justis
# Date: 07/26/2015
# Summary: Program creates a sparse Hp matrix which has the number of clauses each assignment did not satisfy along the diagonal
# such that Hp(i,i) = number of clauses not satisfied by assignment,i.
# Changes: makes assignment on fly rather than a list of them before checking.
# Input: 
#-n: integer number of particles
#-m: integer number of clauses
# Output:
#-Sparse Hp

import random
import numpy as np
import scipy.sparse as sp
import time

# Create clauses to test.
def clauseListGen(clauseList, numParts):

     # Generate required number of clauses.
     for i in range(len(clauseList)):
          goodClause = 0

          # Generate a clause without redundancy/contradicting variables.
          while (goodClause == 0):
              clauseList[i] = ([random.randint(1,numParts),random.randint(1,numParts),random.randint(1,4) ])

              # Check if clause has duplicate state assignments to same particle .
              if (clauseList[i][0] != clauseList[i][1]):

                 # Make sure particles are ordered in clause by number.
                 if (clauseList[i][0] > clauseList[i][1]):
                     temp = clauseList[i][0]
                     clauseList[i][0] = clauseList[i][1]
                     clauseList[i][1] = temp

                 # Make sure clause is not already in list of clauses.
                 if ((clauseList[i] not in clauseList[0:i])):
                    goodClause = 1 # Clause makes sense.
     return clauseList

# Check to see how many clauses each assignment satisfies.
def makeHp(numParticles,numClauses):
    totalTime = time.time()
    numPossAssgs = 2**numParticles

    # Create list of assignments and clauses.
    # clausetime = time.time()
    clauseList = clauseListGen([0]*numClauses, numParticles)
    # print("--- %s seconds for making clause list---" % (time.time() - clausetime))

    # checktime = time.time()
    # Initialize vector for diagonal of Hp with each having no clauses satisfied.
    hp = [numClauses]*numPossAssgs
    bestAssgs = [] #List to hold best assignments for given 2SAT problem instance.
    minNumClauseNotSat = numClauses
    # totalAssgTime = 0.
    # totClauseCheckTime = 0
    # totTrackResultsTime = 0
    # For each random assignment to test.
    for assgNum in range(numPossAssgs):
        # assgtime = time.time()
        assg = bin(assgNum)[2:].zfill(numParticles) # Generate a new random assignment.
        # totalAssgTime += ((time.time() - assgtime))

        #
        # clauseCheckTime = time.time()
        # Test assignment against each clause.
        for clauseNum in range(numClauses):
            clauseSat = 0
            clausePartState = clauseList[clauseNum][2]         # Get state of particle in clause.

            if (clausePartState == 1):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 2):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1
            elif (clausePartState == 3):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 4):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1

            # Put number of clauses unsatisfied by assignment, i, on Hp diagonal entry, i.
            # If assignment satisfied this clause decrease the count for how many clauses it does not satisfy.
            hp[assgNum] -= clauseSat

        # totClauseCheckTime += (time.time()-clauseCheckTime)

        # Keep record of assignment(s) satisfying the most clauses.
        # Number of clauses satisfied has been seen before.
        if (hp[assgNum]  == minNumClauseNotSat):
            bestAssgs = np.hstack([bestAssgs, assgNum + 1])
        # New "low score".
        elif (hp[assgNum]  < minNumClauseNotSat):
            bestAssgs = [(assgNum + 1)]
            minNumClauseNotSat = hp[assgNum]

    hp = sp.diags(hp,0)
    # print("--- %s seconds for checkingClauses---" % (totClauseCheckTime))
    # print("--- %s seconds for making assgList---" % (totalAssgTime))
    # print("--- %s seconds for assgClausecheck---" % (time.time() - checktime))
    # print("--- %s seconds for makeHp---" % (time.time() - totalTime))
    return hp, minNumClauseNotSat, bestAssgs
#
# for i in range(1,2):
#     for n in range(13,14):
#         print '# Particles = ' + str(n)
#
#         for m in range(100,111):
#             print '\n# Clauses = ' + str(m)
#
#             if (int(m) > (2*math.factorial(int(n))/ math.factorial(int(n) - 2))):
#                 break
#             makeHp(n,m)