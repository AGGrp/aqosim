def largeNLast(numParticles,numInstances):

     # Clear current figures.
    plt.cla()
    plt.close("all")
    plt.hold(1)
    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(numParticles)
    ys = [i+x+(i*x)**2 for i in range(numParticles)]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))

    # For each particle number up to n.
    for particleNum in range(2,numParticles + 1):

        # Get new pretty color for this number of particles.
        newColor = next(colors)

        # Maximum number of unique clauses depending on n.
        mMax = 2*particleNum

        # For each clause up to mMax.
        for numClauses in range(1, mMax+1):
            alpha  = np.ones((numInstances,1))*float(float(numClauses)/float(particleNum)) # Alpha
            ms = numClauses*np.ones((numInstances,1))
            newMs = []
            newNs = []
            newAlphas = []
            avgNewM = 0.
            avgNewAlpha = 0.
            for sample in range(numInstances):
                clauseList = clauseListGen([0]*numClauses, particleNum)
                newM,newN = pruneClauseList(clauseList,numClauses,particleNum)
                if (newN == 0):
                    newM = 0.
                    newAlpha = 0.
                else:
                    newAlpha = float(newM)/newN
                newMs = np.hstack([newMs,newM])
                newNs = np.hstack([newNs,newN])
                newAlphas = np.hstack([newAlphas,newAlpha])
                avgNewM += newM
                avgNewAlpha += newAlpha
            avgNewM = avgNewM/numInstances
            newMsSTD = np.std(newMs)
            avgNewAlpha = avgNewAlpha/numInstances
            newAlphaSTD = np.std(newAlphas)
            print '\n#Particles = ' + str(particleNum) + '\n#Clauses = ' + str(numClauses)+'\nAvg New m = '+str(round(avgNewM)) +' with STD ' + str(newMsSTD)+'\nAvg New alpha = '+str(round(avgNewAlpha)) +' with STD ' + str(newAlphaSTD)
            msDiff = ms.transpose()-newMs
            newMsQuotient = newMs/ms.transpose()

            if (numClauses==1):
                # All
                figNum = 1
                plt.figure(figNum)
                plt.scatter(alpha, newMs, color=newColor,label = str(particleNum)+' Particles')
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, msDiff, color=newColor,label = str(particleNum)+' Particles')
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newMsQuotient, color=newColor,label = str(particleNum)+' Particles')
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newAlphas, color=newColor,label = str(particleNum)+' Particles')

            else:
                figNum = 1
                plt.figure(figNum)
                plt.scatter(alpha, newMs, color=newColor)
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, msDiff, color=newColor)
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newMsQuotient, color=newColor)
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newAlphas, color=newColor)

    # All
    figNum = 1
    plt.figure(figNum)
    plt.title('BACK:Kernel Size vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size(New#Clauses')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    plt.figure(figNum)
    plt.title('BACK:Kernel Size Difference vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size Difference(Old size - New Size)')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    plt.figure(figNum)
    plt.title('BACK:Kernel Size Quotient vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size Quotient(New size/Old Size)')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    plt.figure(figNum)
    plt.title('BACK:New Alpha vs. Old Alpha')
    plt.xlabel('Old Alpha(Entered n and m)')
    plt.ylabel('New Alpha(Entered n and pruned m')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    # Show all plots.
    plt.show()
def largeNLastWithListOfPartNums(numInstances):

    particleNumList = [2,5,10,15,20,30,50,75,100,150,175,200,250,300,400,600,800,1000]
    # Clear current figures.
    plt.cla()
    plt.close("all")
    plt.hold(1)
    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(len(particleNumList))
    ys = [i+x+(i*x)**2 for i in range(len(particleNumList))]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))

    # For each particle number up to n.
    for particleNum in particleNumList:

        # Get new pretty color for this number of particles.
        newColor = next(colors)

        # Maximum number of unique clauses depending on n.
        mMax = 2*particleNum

        if (mMax < 10):
            clauseNumList = np.linspace(1,mMax,10,dtype = int)
        else:
            clauseNumList = np.linspace(1,mMax,50,dtype = int)

        # For each clause up to mMax.
        for numClauses in clauseNumList:
            alpha  = np.ones((numInstances,1))*float(float(numClauses)/float(particleNum)) # Alpha
            ms = numClauses*np.ones((numInstances,1))
            newMs = []
            newNs = []
            newAlphas = []
            avgNewM = 0.
            avgNewAlpha = 0.
            for sample in range(numInstances):
                clauseList = clauseListGen([0]*numClauses, particleNum)
                newM,newN = pruneClauseList(clauseList,numClauses,particleNum)
                if (newN == 0):
                    newM = 0.
                    newAlpha = 0.
                else:
                    newAlpha = float(newM)/newN
                newMs = np.hstack([newMs,newM])
                newNs = np.hstack([newNs,newN])
                newAlphas = np.hstack([newAlphas,newAlpha])
                avgNewM += newM
                avgNewAlpha += newAlpha
            avgNewM = avgNewM/numInstances
            newMsSTD = np.std(newMs)
            avgNewAlpha = avgNewAlpha/numInstances
            newAlphaSTD = np.std(newAlphas)
            print '\n#Particles = ' + str(particleNum) + '\n#Clauses = ' + str(numClauses)+'\nAvg New m = '+str(round(avgNewM)) +' with STD ' + str(newMsSTD)+'\nAvg New alpha = '+str(round(avgNewAlpha)) +' with STD ' + str(newAlphaSTD)
            msDiff = ms.transpose()-newMs
            newMsQuotient = newMs/ms.transpose()

            if (numClauses==1):
                # All
                figNum = 1
                plt.figure(figNum)
                plt.scatter(alpha, newMs, color=newColor,label = str(particleNum)+' Particles')
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, msDiff, color=newColor,label = str(particleNum)+' Particles')
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newMsQuotient, color=newColor,label = str(particleNum)+' Particles')
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newAlphas, color=newColor,label = str(particleNum)+' Particles')

            else:
                figNum = 1
                plt.figure(figNum)
                plt.scatter(alpha, newMs, color=newColor)
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, msDiff, color=newColor)
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newMsQuotient, color=newColor)
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newAlphas, color=newColor)

    # All
    figNum = 1
    plt.figure(figNum)
    plt.title('BACK:Kernel Size vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size(New#Clauses')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    plt.tight_layout()
    figNum +=1

    plt.figure(figNum)
    plt.title('BACK:Kernel Size Difference vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size Difference(Old size - New Size)')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    plt.tight_layout()
    figNum +=1

    plt.figure(figNum)
    plt.title('BACK:Kernel Size Quotient vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size Quotient(New size/Old Size)')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    plt.tight_layout()
    figNum +=1

    plt.figure(figNum)
    plt.title('BACK:New Alpha vs. Old Alpha')
    plt.xlabel('Old Alpha(Entered n and m)')
    plt.ylabel('New Alpha(Entered n and pruned m')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    plt.tight_layout()
    figNum +=1

    # Show all plots.
    plt.show()
def smallNLast(numParticles,numInstances):

     # Clear current figures.
    plt.cla()
    plt.close("all")
    plt.hold(1)
    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(numParticles)
    ys = [i+x+(i*x)**2 for i in range(numParticles)]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))

    # For each particle number up to n.
    for particleNum in range(2,numParticles + 1):
        newPartNum = 2 + numParticles - particleNum # Used to go backwards towards smallest n last.
        # Get new pretty color for this number of particles.
        newColor = next(colors)

        # Maximum number of unique clauses depending on n.
        mMax = 2*newPartNum

        # For each clause up to mMax.
        for numClauses in range(1, mMax+1):
            alpha  = np.ones((numInstances,1))*float(float(numClauses)/float(newPartNum)) # Alpha
            ms = numClauses*np.ones((numInstances,1))
            newMs = []
            newAlphas = []
            newNs = []
            avgNewM = 0.
            avgNewAlpha = 0.
            for sample in range(numInstances):
                clauseList = clauseListGen([0]*numClauses, newPartNum)
                newM,newN = pruneClauseList(clauseList,numClauses,newPartNum)
                if (newN == 0):
                    newM = 0.
                    newAlpha = 0.
                else:
                    newAlpha = float(newM)/newN
                newMs = np.hstack([newMs,newM])
                newNs = np.hstack([newNs,newN])
                newAlphas = np.hstack([newAlphas,newAlpha])
                avgNewM += newM
                avgNewAlpha += newAlpha
            avgNewM = avgNewM/numInstances
            newMsSTD = np.std(newMs)
            avgNewAlpha = avgNewAlpha/numInstances
            newAlphaSTD = np.std(newAlphas)
            print '\n#Particles = ' + str(newPartNum) + '\n#Clauses = ' + str(numClauses)+'\nAvg New m = '+str(round(avgNewM)) +' with STD ' + str(newMsSTD)+'\nAvg New alpha = '+str(round(avgNewAlpha)) +' with STD ' + str(newAlphaSTD)
            msDiff = ms.transpose()-newMs
            newMsQuotient = newMs/ms.transpose()

            if (numClauses==1):
                # All
                figNum = 1
                plt.figure(figNum)
                plt.scatter(alpha, newMs, color=newColor,label = str(newPartNum)+' Particles')
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, msDiff, color=newColor,label = str(newPartNum)+' Particles')
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newMsQuotient, color=newColor,label = str(newPartNum)+' Particles')
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newAlphas, color=newColor,label = str(newPartNum)+' Particles')

            else:
                figNum = 1
                plt.figure(figNum)
                plt.scatter(alpha, newMs, color=newColor)
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, msDiff, color=newColor)
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newMsQuotient, color=newColor)
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newAlphas, color=newColor)

    # All
    figNum = 1
    plt.figure(figNum)
    plt.title('FRONT:Kernel Size vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size(New#Clauses')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    plt.figure(figNum)
    plt.title('FRONT:Kernel Size Difference vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size Difference(Old size - New Size)')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    plt.figure(figNum)
    plt.title('FRONT:Kernel Size Quotient vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size Quotient(New size/Old Size)')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    plt.figure(figNum)
    plt.title('FRONT:New Alpha vs. Old Alpha')
    plt.xlabel('Old Alpha(Entered n and m)')
    plt.ylabel('New Alpha(Entered n and pruned m')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    # Show all plots.
    plt.show()
def smallNLastWithListOfPartNums(numInstances):

    particleNumList = [1000, 800, 600, 400, 300, 250, 200, 175, 150, 100, 75, 50, 30, 20, 15, 10, 5, 2]
    # Clear current figures.
    plt.cla()
    plt.close("all")
    plt.hold(1)
    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(len(particleNumList))
    ys = [i+x+(i*x)**2 for i in range(len(particleNumList))]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))

    # For each particle number up to n.
    for particleNum in particleNumList:

        # Get new pretty color for this number of particles.
        newColor = next(colors)

        # Maximum number of unique clauses depending on n.
        mMax = 2*particleNum
        if (mMax < 10):
            clauseNumList = np.linspace(1,mMax,10,dtype = int)
        else:
            clauseNumList = np.linspace(1,mMax,50,dtype = int)
        # For each clause up to mMax.
        for numClauses in clauseNumList:
            alpha  = np.ones((numInstances,1))*float(float(numClauses)/float(particleNum)) # Alpha
            ms = numClauses*np.ones((numInstances,1))
            newMs = []
            newAlphas = []
            newNs = []
            avgNewM = 0.
            avgNewAlpha = 0.
            for sample in range(numInstances):
                clauseList = clauseListGen([0]*numClauses, particleNum)
                newM,newN = pruneClauseList(clauseList,numClauses,particleNum)
                if (newN == 0):
                    newM = 0.
                    newAlpha = 0.
                else:
                    newAlpha = float(newM)/newN
                newMs = np.hstack([newMs,newM])
                newNs = np.hstack([newNs,newN])
                newAlphas = np.hstack([newAlphas,newAlpha])
                avgNewM += newM
                avgNewAlpha += newAlpha
            avgNewM = avgNewM/numInstances
            newMsSTD = np.std(newMs)
            avgNewAlpha = avgNewAlpha/numInstances
            newAlphaSTD = np.std(newAlphas)
            print '\n#Particles = ' + str(particleNum) + '\n#Clauses = ' + str(numClauses)+'\nAvg New m = '+str(round(avgNewM)) +' with STD ' + str(newMsSTD)+'\nAvg New alpha = '+str(round(avgNewAlpha)) +' with STD ' + str(newAlphaSTD)
            msDiff = ms.transpose()-newMs
            newMsQuotient = newMs/ms.transpose()

            if (numClauses==1):
                # All
                figNum = 1
                plt.figure(figNum)
                plt.scatter(alpha, newMs, color=newColor,label = str(particleNum)+' Particles')
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, msDiff, color=newColor,label = str(particleNum)+' Particles')
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newMsQuotient, color=newColor,label = str(particleNum)+' Particles')
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newAlphas, color=newColor,label = str(particleNum)+' Particles')

            else:
                figNum = 1
                plt.figure(figNum)
                plt.scatter(alpha, newMs, color=newColor)
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, msDiff, color=newColor)
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newMsQuotient, color=newColor)
                figNum +=1

                plt.figure(figNum)
                plt.scatter(alpha, newAlphas, color=newColor)

    # All
    figNum = 1
    plt.figure(figNum)
    plt.title('FRONT:Kernel Size vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size(New#Clauses')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    plt.figure(figNum)
    plt.title('FRONT:Kernel Size Difference vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size Difference(Old size - New Size)')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    plt.figure(figNum)
    plt.title('FRONT:Kernel Size Quotient vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size Quotient(New size/Old Size)')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    figNum +=1

    plt.figure(figNum)
    plt.title('FRONT:New Alpha vs. Old Alpha')
    plt.xlabel('Old Alpha(Entered n and m)')
    plt.ylabel('New Alpha(Entered n and pruned m')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    # Show all plots.
    plt.show()
# 3D
from mpl_toolkits.mplot3d import Axes3D
def plot3D(numParticles,numInstances):

     # Clear current figures.
    plt.cla()
    plt.close("all")
    plt.hold(1)
    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(numParticles)
    ys = [i+x+(i*x)**2 for i in range(numParticles)]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))

    # For each particle number up to n.
    for particleNum in range(2,numParticles + 1):

        # Get new pretty color for this number of particles.
        newColor = next(colors)

        # Create array for plotting against the number of particles.
        ns = particleNum*np.ones((numInstances,1))

        # Maximum number of unique clauses depending on n.
        mMax = 2*particleNum

        # For each clause up to mMax.
        for numClauses in range(1, mMax+1):
            alpha  = np.ones((numInstances,1))*float(float(numClauses)/float(particleNum)) # Alpha
            ms = numClauses*np.ones((numInstances,1))
            newMs = []
            avgNewM = 0.
            avgNewAlpha = 0.
            for sample in range(numInstances):
                clauseList = clauseListGen([0]*numClauses, particleNum)
                newM,newN = pruneClauseList(clauseList,numClauses,particleNum)
                if (newN == 0):
                    newM = 0.
                    newAlpha = 0.
                else:
                    newAlpha = float(newM)/newN
                newMs = np.hstack([newMs,newM])
                newNs = np.hstack([newNs,newN])
                newAlphas = np.hstack([newAlphas,newAlpha])
                avgNewM += newM
                avgNewAlpha += newAlpha
            avgNewM = avgNewM/numInstances
            newMsSTD = np.std(newMs)
            avgNewAlpha = avgNewAlpha/numInstances
            newAlphaSTD = np.std(newAlphas)
            print '\n#Particles = ' + str(particleNum) + '\n#Clauses = ' + str(numClauses)+'\nAvg New m = '+str(round(avgNewM)) +' with STD ' + str(newMsSTD)+'\nAvg New alpha = '+str(round(avgNewAlpha)) +' with STD ' + str(newAlphaSTD)
            msDiff = ms.transpose()-newMs
            newMsQuotient = newMs/ms.transpose()

            if (numClauses==1):
                fig = plt.figure(1)
                ax = fig.gca(projection='3d')
                ax.scatter(ns,alpha, newMs, color=newColor,label = str(particleNum)+' Particles')

                fig = plt.figure(2)
                ax = fig.gca(projection='3d')
                ax.scatter(ns,alpha, msDiff, color=newColor,label = str(particleNum)+' Particles')

                fig = plt.figure(3)
                ax = fig.gca(projection='3d')
                ax.scatter(ns,alpha, newMsQuotient, color=newColor,label = str(particleNum)+' Particles')

                fig = plt.figure(4)
                ax = fig.gca(projection='3d')
                ax.scatter(ns,alpha, newAlphas, color=newColor,label = str(particleNum)+' Particles')

            else:
                fig = plt.figure(1)
                ax = fig.gca(projection='3d')
                ax.scatter(ns,alpha, newMs, color=newColor)

                fig = plt.figure(2)
                ax = fig.gca(projection='3d')
                ax.scatter(ns,alpha, msDiff, color=newColor)

                fig = plt.figure(3)
                ax = fig.gca(projection='3d')
                ax.scatter(ns,alpha, newMsQuotient, color=newColor)

                fig = plt.figure(4)
                ax = fig.gca(projection='3d')
                ax.scatter(ns,alpha, newAlphas, color=newColor)

    plt.figure(1)
    plt.gca().set_title('Kernel Size vs. Alpha')
    plt.gca().set_ylabel('Alpha (From Entered m)')
    plt.gca().set_zlabel('Kernel Size(New#Clauses)')
    plt.gca().set_xlabel('#Of Particles,n')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    plt.figure(2)
    plt.gca().set_title('Kernel Size Difference vs. Alpha')
    plt.gca().set_ylabel('Alpha (From Entered m)')
    plt.gca().set_zlabel('Kernel Size Difference(Old size - New Size)')
    plt.gca().set_xlabel('#Of Particles,n')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    plt.figure(3)
    plt.gca().set_title('Kernel Size Quotient vs. Alpha')
    plt.gca().set_ylabel('Alpha (From Entered m,n)')
    plt.gca().set_zlabel('Kernel Size Quotient(New size/Old Size)')
    plt.gca().set_xlabel('#Of Particles,n')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    plt.figure(4)
    plt.gca().set_title('New Alpha vs. Old Alpha')
    plt.gca().set_ylabel('Old Alpha(Entered n and m)')
    plt.gca().set_zlabel('New Alpha(Pruned n and m')
    plt.gca().set_xlabel('#Of Particles,n')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    # Show all plots.
    plt.show()
#Does NOT WORK!
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import colors as cl
def plot3DHistoAttempt(numParticles,numInstances):

     # Clear current figures.
    plt.cla()
    plt.close("all")
    plt.hold(1)
    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(numParticles)
    ys = [i+x+(i*x)**2 for i in range(numParticles)]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))
    labels = ''
    # For each particle number up to n.
    for particleNum in range(2,numParticles + 1):

        # Get new pretty color for this number of particles.
        newColor = next(colors)
        labels = labels+','+str(particleNum)+' Particles'
        # Create array for plotting against the number of particles.
        ns = particleNum*np.ones((numInstances,1))

        # Maximum number of unique clauses depending on n.
        mMax = 2*particleNum

        # For each clause up to mMax.
        for numClauses in range(1, mMax+1):
            alpha  = np.ones((numInstances,1))*float(float(numClauses)/float(particleNum)) # Alpha
            ms = numClauses*np.ones((numInstances,1))
            newMs = []
            avgNewM = 0.
            for sample in range(numInstances):
                clauseList = clauseListGen([0]*numClauses, particleNum)
                newM = pruneClauseList(clauseList,numClauses,particleNum)
                newMs = np.hstack([newMs,newM])
                avgNewM += newM
            newMsSTD = np.std(newMs)
            print '\n#Particles = ' + str(particleNum) + '\n#Clauses = ' + str(numClauses)+'\nAvg New m = '+str(round(avgNewM/numInstances)) +' with STD ' + str(newMsSTD)

            fig = plt.figure(1)
            ax = fig.gca(projection='3d')
            y = np.ones(len(newMs))*particleNum
            hist, xedges, yedges = np.histogram2d(y.transpose(),newMs,bins =len(np.unique(newMs)))
            elements = (len(xedges) - 1) * (len(yedges) - 1)
            xpos, ypos = np.meshgrid(xedges[:-1]+0.25, yedges[:-1]+0.25)

            xpos = xpos.flatten()
            ypos = ypos.flatten()
            zpos = np.zeros(elements)
            dx = np.ones_like(zpos)
            dy = dx.copy()
            dz = hist.flatten()
            ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color= cl.colorConverter.to_rgb(newColor), zsort='average')

    # All
    plt.figure(1)
    plt.gca().set_title('Kernel Size Distribution')
    plt.gca().set_ylabel('Kernel Size(New#Clauses')
    plt.gca().set_zlabel('# Of Instances out of '+str(numInstances))
    plt.gca().set_xlabel('#Of Particles,n')
    plt.legend([labels],bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    print labels

    # Show all plots.
    plt.show()
def histoGram2D(numParticles,numInstances):

    # For each particle number up to n.
    for particleNum in range(2,numParticles + 1):

        # Maximum number of unique clauses depending on n.
        mMax = 2*particleNum

        # For each clause up to mMax.
        for numClauses in range(1, mMax+1):
            P.cla()
            newMs = []
            avgNewM = 0.
            for sample in range(numInstances):
                clauseList = clauseListGen([0]*numClauses, particleNum)
                newM = pruneClauseList(clauseList,numClauses,particleNum)
                newMs = np.hstack([newMs,newM])
                avgNewM += newM
            print '\n#Particles = ' + str(particleNum) + '\n#Clauses = ' + str(numClauses)+'\nAvg New m = '+str(round(avgNewM/numInstances))
            n, bins, patches = P.hist(newMs, len(np.unique(newMs)))
            P.setp(patches, 'facecolor', 'k','edgecolor','r', 'alpha', 0.75)
            P.title('Kernel Size Histogram [n='+str(particleNum)+', m='+str(numClauses)+']')
            P.xlabel('Kernel Size(New#Clauses)')
            P.ylabel('# Of Instances out of '+str(numInstances))
            ims.append(( P.hist(newMs, len(np.unique(newMs)))))
def alphaScaled(numParticles,numInstances):

     # Clear current figures.
    plt.cla()
    plt.close("all")
    plt.hold(1)
    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(numParticles)
    ys = [i+x+(i*x)**2 for i in range(numParticles)]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))

    # For each particle number up to n.
    for particleNum in range(2,numParticles + 1):

        # Get new pretty color for this number of particles.
        newColor = next(colors)

        # Maximum number of unique clauses depending on n.
        mMax = 2*particleNum*(particleNum - 1)

        # Maximum alpha depending on mMax and n.
        alphaMax = (mMax)/(particleNum)

        # For each clause up to mMax.
        for numClauses in range(1, mMax+1):
            alpha  = np.ones((numInstances,1))*float(float(numClauses)/float(particleNum)) # Alpha
            alphaScaled = (alphaMax**-1)*alpha
            ms = numClauses*np.ones((numInstances,1))
            numClauseScaled = (mMax**-1)*ms
            newMs = []
            for sample in range(numInstances):
                clauseList = clauseListGen([0]*numClauses, particleNum)
                newMs = np.hstack([newMs,pruneClauseList(clauseList,numClauses,particleNum)])
            if (numClauses==1):
                # All
                plt.figure(1)
                plt.scatter(alpha, newMs, color=newColor)
                plt.plot(alpha, newMs, color=newColor,label = str(particleNum)+' Particles')

                plt.figure(2)
                plt.scatter(alphaScaled,newMs, color=newColor)
                plt.plot(alphaScaled,newMs, color=newColor,label = str(particleNum)+' Particles')

                plt.figure(3)
                plt.scatter(ms, newMs, color=newColor)
                plt.plot(ms, newMs, color=newColor,label = str(particleNum)+' Particles')

                plt.figure(4)
                plt.scatter(numClauseScaled,newMs, color=newColor)
                plt.plot(numClauseScaled,newMs, color=newColor,label = str(particleNum)+' Particles')

            else:
                # All
                plt.figure(1)
                plt.scatter(alpha, newMs, color=newColor)
                plt.plot(alpha, newMs, color=newColor)

                plt.figure(2)
                plt.scatter(alphaScaled,newMs, color=newColor)
                plt.plot(alphaScaled,newMs, color=newColor)

                plt.figure(3)
                plt.scatter(ms, newMs, color=newColor)
                plt.plot(ms, newMs, color=newColor)

                plt.figure(4)
                plt.scatter(numClauseScaled,newMs, color=newColor)
                plt.plot(numClauseScaled,newMs, color=newColor)

    # All
    plt.figure(1)
    plt.title('Kernel Size vs. Alpha')
    plt.xlabel('Alpha (From Entered m)')
    plt.ylabel('Kernel Size (New#Clauses')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    plt.figure(2)
    plt.title('Kernel Size vs. AlphaScaled')
    plt.xlabel('Alpha (scaled by alpha/alphaMax)(From Entered m)')
    plt.ylabel('Kernel Size (New#Clauses')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    plt.figure(3)
    plt.title('Kernel Size vs. m:# of Clauses')
    plt.xlabel('Entered # Clauses ')
    plt.ylabel('Kernel Size (New#Clauses')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)

    plt.figure(4)
    plt.title('Kernel Size vs. m:# of Clauses')
    plt.xlabel('Entered # Clauses (scaled by m/mMax)')
    plt.ylabel('Kernel Size (New#Clauses')
    plt.legend(bbox_to_anchor=(1., 1), loc=2, borderaxespad=0.)
    # Show all plots.
    plt.show()