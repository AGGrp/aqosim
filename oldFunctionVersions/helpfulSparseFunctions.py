###########################################################
import time
start_time = time.time()
main()
print("--- %s seconds ---" % (time.time() - start_time))
###########################################################
>>> from scipy.sparse import coo_matrix
>>> coo_matrix((3, 4), dtype=np.int8).toarray()
array([[0, 0, 0, 0],
       [0, 0, 0, 0],
       [0, 0, 0, 0]], dtype=int8)
>>>
>>> row  = np.array([0, 3, 1, 0])
>>> col  = np.array([0, 3, 1, 2])
>>> data = np.array([4, 5, 7, 9])
>>> coo_matrix((data, (row, col)), shape=(4, 4)).toarray()
array([[4, 0, 9, 0],
       [0, 7, 0, 0],
       [0, 0, 0, 0],
       [0, 0, 0, 5]])
>>>
>>> # example with duplicates
>>> row  = np.array([0, 0, 1, 3, 1, 0, 0])
>>> col  = np.array([0, 2, 1, 3, 1, 0, 0])
>>> data = np.array([1, 1, 1, 1, 1, 1, 1])
>>> coo_matrix((data, (row, col)), shape=(4, 4)).toarray()
array([[3, 0, 1, 0],
       [0, 2, 0, 0],
       [0, 0, 0, 0],
       [0, 0, 0, 1]])
###########################################################

arcsin()	Element-wise arcsin.
arcsinh()	Element-wise arcsinh.
arctan()	Element-wise arctan.
arctanh()	Element-wise arctanh.
asformat(format)	Return this matrix in a given sparse format
asfptype()	Upcast matrix to a floating point format (if necessary)
astype(t)
ceil()	Element-wise ceil.
conj()
conjugate()
copy()
deg2rad()	Element-wise deg2rad.
diagonal()	Returns the main diagonal of the matrix
dot(other)	Ordinary dot product
expm1()	Element-wise expm1.
floor()	Element-wise floor.
getH()
get_shape()
getcol(j)	Returns a copy of column j of the matrix, as an (m x 1) sparse matrix (column vector).
getformat()
getmaxprint()
getnnz([axis])	Get the count of explicitly-stored values (nonzeros)
getrow(i)	Returns a copy of row i of the matrix, as a (1 x n) sparse matrix (row vector).
log1p()	Element-wise log1p.
max([axis])	Maximum of the elements of this matrix.
maximum(other)
mean([axis])	Average the matrix over the given axis.
min([axis])	Minimum of the elements of this matrix.
minimum(other)
multiply(other)	Point-wise multiplication by another matrix
nonzero()	nonzero indices
power(n[, dtype])	This function performs element-wise power.
rad2deg()	Element-wise rad2deg.
reshape(shape)
rint()	Element-wise rint.
set_shape(shape)
setdiag(values[, k])	Set diagonal or off-diagonal elements of the array.
sign()	Element-wise sign.
sin()	Element-wise sin.
sinh()	Element-wise sinh.
sqrt()	Element-wise sqrt.
sum([axis])	Sum the matrix over the given axis.
sum_duplicates()	Eliminate duplicate matrix entries by adding them together
tan()	Element-wise tan.
tanh()	Element-wise tanh.
toarray([order, out])	See the docstring for spmatrix.toarray.
tobsr([blocksize])
tocoo([copy])
tocsc()	Return a copy of this matrix in Compressed Sparse Column format
tocsr()	Return a copy of this matrix in Compressed Sparse Row format
todense([order, out])	Return a dense matrix representation of this matrix.
todia()
todok()
tolil()
transpose([copy])
trunc()	Element-wise trunc.
###########################################################
Building sparse matrices:

eye(m[, n, k, dtype, format])	Sparse matrix with ones on diagonal
identity(n[, dtype, format])	Identity matrix in sparse format
kron(A, B[, format])	kronecker product of sparse matrices A and B
kronsum(A, B[, format])	kronecker sum of sparse matrices A and B
diags(diagonals, offsets[, shape, format, dtype])	Construct a sparse matrix from diagonals.
spdiags(data, diags, m, n[, format])	Return a sparse matrix from diagonals.
block_diag(mats[, format, dtype])	Build a block diagonal sparse matrix from provided matrices.
tril(A[, k, format])	Return the lower triangular portion of a matrix in sparse format
triu(A[, k, format])	Return the upper triangular portion of a matrix in sparse format
bmat(blocks[, format, dtype])	Build a sparse matrix from sparse sub-blocks
hstack(blocks[, format, dtype])	Stack sparse matrices horizontally (column wise)
vstack(blocks[, format, dtype])	Stack sparse matrices vertically (row wise)
rand(m, n[, density, format, dtype, ...])	Generate a sparse matrix of the given shape and density with uniformly distributed values.
norm
Sparse matrix tools:

find(A)	Return the indices and values of the nonzero elements of a matrix
###########################################################
###########################################################
###########################################################
###########################################################
###########################################################
###########################################################
###########################################################
###########################################################
###########################################################
###########################################################
###########################################################
###########################################################

