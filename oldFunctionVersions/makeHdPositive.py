# Author: Kelsey C. Justis
# Date: 07/29/2015
# Summary: Program constructs Hd given the number of particles in the system in consideration.
# Updated to work faster with sparse matrices; works up to 15 particles quickly after this memory problems encountered.
# Input:
#-n: number of particles/spins in system
#
# Output: 
#-Hd: driver Hamiltonian = (2^n)X(2^n) matrix (sum of sigma_x)^i

from numpy import array
from scipy.sparse import kron,coo_matrix,identity

# Sigma x
sigX = coo_matrix(array([[0,1], [1,0]]))

# Makes driver Hamiltonian which is sum of tensor expressions involving n-1 identity matrix terms and one sigma x term corresponding to ith particle.
def makeHd(n):

    # Initialize matrix for Hd
    hd = identity(2**n)-identity(2**n)

    # Calculate Hd by summing up tensor acting on each particle.
    for i in range(1,n+1):
               # Calculate current tensor expression on ith particle and add to Hd.
        hd += kron(kron(identity(2**(i-1)), sigX), identity(2**(n-i)))

    # Return array form of Hd matrix.
    return hd