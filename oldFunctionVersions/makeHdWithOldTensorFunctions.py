# Author: Kelsey C. Justis
# Date: 07/22/2015
# Summary: Program constructs Hd given the number of particles in the system in consideration.
# Updated to work faster with sparse matrices; works up to 15 particles quickly after this memory problems encountered.
# Input:
#-n: number of particles/spins in system
#
# Output: 
#-Hd: driver Hamiltonian = (2^n)X(2^n) matrix (sum of sigma_x)^i

from numpy import*
from scipy.sparse import *

# Calculates tensor product of input list of sparse matrices in intended order to be tensored together.
def tensor(orderedListOfMatrices):

    # Calculate tensor expression result.
    for matrixNum in range(len(orderedListOfMatrices)-1):

        # For first two terms tensored together.
        if (matrixNum == 0):
            tensorProd = kron(orderedListOfMatrices[0], orderedListOfMatrices[1])

        # Tensor product for all subsequent terms.
        else:
            tensorProd = kron(tensorProd, orderedListOfMatrices[matrixNum+1])

    # Return sparse tensor product matrix.
    return tensorProd

# Sigma x
sigX = coo_matrix(array([[0,1], [1,0]]))

# Makes driver Hamiltonian which is sum of tensor expressions involving n-1 identity matrix terms and one sigma x term corresponding to ith particle.
def makeHd(n):

    # Initialize matrix for Hd
    hd = identity(2**n)-identity(2**n)

    # Calculate Hd by summing up tensor acting on each particle.
    for i in range(1,n+1):

        # Calculate current tensor expression on ith particle.
        temp = tensor([identity(2**(i-1)), sigX, identity(2**(n-i))])

        # Add current tensor expression to Hd.
        hd += temp

    # Return array form of Hd matrix.
    return hd.toarray()

print makeHd(15)


# Author: Kelsey C. Justis
# Date: 07/22/2015
# Summary: Program constructs Hd given the number of particles in the system in consideration.
# Updated to work faster with sparse matrices; works up to 15 particles quickly after this memory problems encountered.
# Input:
#-n: number of particles/spins in system
#
# Output:
#-Hd: driver Hamiltonian = (2^n)X(2^n) matrix (sum of sigma_x)^i

from numpy import*
from scipy.sparse import *
import time
# Calculates tensor product of input list of sparse matrices in intended order to be tensored together.
def tensor(orderedListOfMatrices):

    # Calculate tensor expression result.
    for matrixNum in range(len(orderedListOfMatrices)-1):

        # For first two terms tensored together.
        if (matrixNum == 0):
            tensorProd = kron(orderedListOfMatrices[0], orderedListOfMatrices[1])

        # Tensor product for all subsequent terms.
        else:
            tensorProd = kron(tensorProd, orderedListOfMatrices[matrixNum+1])

    # Return sparse tensor product matrix.
    return tensorProd

# Sigma x
sigX = coo_matrix(array([[0,1], [1,0]]))

# Makes driver Hamiltonian which is sum of tensor expressions involving n-1 identity matrix terms and one sigma x term corresponding to ith particle.
def makeHd(n):

    # Initialize matrix for Hd
    hd = identity(2**n)-identity(2**n)

    # Calculate Hd by summing up tensor acting on each particle.
    for i in range(1,n+1):

        temp= kron(kron(identity(2**(i-1)), sigX), identity(2**(n-i)))
        # Calculate current tensor expression on ith particle.
        # temp = tensor([identity(2**(i-1)), sigX, identity(2**(n-i))])

        # Add current tensor expression to Hd.
        hd += temp

    # Return array form of Hd matrix.
    return hd.toarray()