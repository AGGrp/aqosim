# Author: Kelsey C. Justis
# Date: 08/21/2015
# Summary: Program creates a sparse Hp matrix which has the number of clauses each assignment did not satisfy along the diagonal
# such that Hp(i,i) = number of clauses not satisfied by assignment,i.
# Performs pruning of particles appearing in only one clause or appears with the same state in all of its involved clauses. INCLUDES special case of all
# 4 permutations of 2 particles involved.
# Input: 
#-n: integer number of particles
#-m: integer number of clauses
# Output:
#-Sparse Hp

import random
import numpy as np
import scipy.sparse as sp
from collections import Counter
# Create clauses to test.
def clauseListGen(clauseList, numParts):

     # Generate required number of clauses.
     for i in range(len(clauseList)):
          goodClause = 0

          # Generate a clause without redundancy/contradicting variables.
          while (goodClause == 0):
              clauseList[i] = ([((-1)**random.randint(0,1))*(random.randint(1,numParts)),\
                            ((-1)**random.randint(0,1))*(random.randint(1,numParts))])

              # Check if clause has duplicate state assignments to same particle .
              if (abs(clauseList[i][0])!=abs(clauseList[i][1])):

                 # Make sure particles are ordered in clause by number.
                 if (abs(clauseList[i][0]) > abs(clauseList[i][1])):
                     temp = clauseList[i][0]
                     clauseList[i][0] = clauseList[i][1]
                     clauseList[i][1] = temp

                 # Make sure clause is not already in list of clauses.
                 if ((clauseList[i] not in clauseList[0:i])):
                    goodClause = 1 # Clause makes sense.
     return clauseList
# Prune Clause List.
def pruneClauseList(clauseList,numClauses,numParticles):

    # Build clause matrix from clause list.
    clauseMatrix = np.zeros((numClauses,numParticles),dtype = int)
    for clauseNum in range(numClauses):
        var1 = clauseList[clauseNum][0]
        var2 = clauseList[clauseNum][1]
        clauseMatrix[clauseNum,abs(var1)-1] = var1
        clauseMatrix[clauseNum,abs(var2)-1] = var2

    # Prune matrix if necessary
    donePruning = False
    initialClauseMatrix = clauseMatrix

    # Keep pruning until we can't prune anymore.
    while (donePruning == False):

        currParticle= 0
        prunedClauseMatrix = initialClauseMatrix
        print 'Initial Clause matrix is:\n'+str(prunedClauseMatrix)+'\n'

        # While there are still multiple particle columns in matrix and we are not in the last particle column, and there are multiple clauses.
        while ((prunedClauseMatrix.shape[1]!=0) and (currParticle < prunedClauseMatrix.shape[1])and (prunedClauseMatrix.shape[0]!=1)):

            # The frequency of a particle number regardless of state.
            partFreq = sum(abs(np.sign(prunedClauseMatrix[:,currParticle])))

            # The frequency of a particle with state accounted for.
            partFreqWithState = sum(np.sign(prunedClauseMatrix[:,currParticle]))

            # If the particle appears in only one clause.
            if (partFreq == 1):

                # Find the clause it appears in.
                clauseRow = np.where(abs(prunedClauseMatrix[:,currParticle])!=0)[0][0]
                print 'Pruning row: '+str(clauseRow+1)

                # Delete that clause.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,clauseRow, axis=0)
                print 'Pruning column: '+str(currParticle+1)

                # Delete the column for that particle number.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)
                print '\nPruned Clause matrix is:\n'+str(prunedClauseMatrix)

            # The current particle is not present in any clause.
            elif (partFreq == 0):
                print 'Pruning column: '+str(currParticle+1)

                # Delete the column for that particle number.
                prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)
                print '\nPruned Clause matrix is:\n'+str(prunedClauseMatrix)

            # If the particle appear more than once and always the same state.
            elif (partFreq == abs(partFreqWithState) != 0):

                    # Find the clauses it appears in.
                    clauseRows = np.where(abs(prunedClauseMatrix[:,currParticle])!=0)[0]
                    print 'Pruning rows: ' + str(clauseRows+1)

                    # Delete those clauses.
                    prunedClauseMatrix = np.delete(prunedClauseMatrix,clauseRows, axis=0)
                    print '\nPruned Clause matrix is:\n'+str(prunedClauseMatrix)
                    print 'Pruning column: '+str(currParticle+1)

                    # Delete the column for that particle number.
                    prunedClauseMatrix = np.delete(prunedClauseMatrix,currParticle, axis=1)
                    print '\nPruned Clause matrix is:\n'+str(prunedClauseMatrix)

            # If the frequency of the current particle number is a multiple of 4.
            elif (partFreq%4 == 0):

                # Find the clauses it appears in.
                clauseRows = np.where(abs(prunedClauseMatrix[:,currParticle])!=0)[0]
                print 'curr col = '+ str(currParticle)
                print 'Possible degenerate rows: ' + str(clauseRows+1)

                # Intialize a matrix to hold frequency of 2nd variable/particle in clause with current particle number.
                degMatrix = [0]*(prunedClauseMatrix.shape[1])

                # Find the frequency of 2nd variable/particle in clause with current particle number.
                for row in clauseRows:

                    # Find the particle that is not the current particle in this clause and increase its count/frequency
                    if(np.where(abs(prunedClauseMatrix[row,:])!=0)[0][0] == currParticle):
                        part2 = np.where(abs(prunedClauseMatrix[row,:])!=0)[0][1]
                        degMatrix[part2]+=1
                    else:
                        part2 = np.where(abs(prunedClauseMatrix[row,:])!=0)[0][0]
                        degMatrix[part2]+=1

                # Delete the columns where the second particle has been found to occur 4 times.
                delCols = [col for col in range(len(degMatrix)) if degMatrix[col] == 4]
                print 'Pruning degenrate rcolumn(s): '+str(delCols)
                prunedClauseMatrix = np.delete(prunedClauseMatrix,delCols, axis=1)
                print 'Pruned Clause matrix is:\n'+str(prunedClauseMatrix)

                # Delete any clauses still in the matrix that now only have one particle involved.
                delRows = [row for row in range(prunedClauseMatrix.shape[0]) if len(prunedClauseMatrix[row,:]) <= 1]
                print 'Pruning row(s): '+str(delRows)
                prunedClauseMatrix = np.delete(prunedClauseMatrix,delRows, axis=0)
                print 'Pruned Clause matrix is:\n'+str(prunedClauseMatrix)

            # Go on to the next particle in the matrix.
            currParticle +=1

        print '\nCHECK:\n   #elts in initial matrix = '+ str(initialClauseMatrix.size)
        print '   #elts in pruned matrix = '+ str(prunedClauseMatrix.size)+'\n'

        # Check to see if anything has been pruned off, if not we must be finished, if so check to see if can prune anymore.
        if (initialClauseMatrix.size == prunedClauseMatrix.size):
            donePruning = True
        else:
            initialClauseMatrix = prunedClauseMatrix

    numClauses = prunedClauseMatrix.shape[0]
    prunedClauseMatrix = prunedClauseMatrix[np.nonzero(prunedClauseMatrix)]
    prunedClauseMatrix = np.reshape(prunedClauseMatrix,(numClauses,2))

    # Reformat for use with rest of code.
    prunedClauseMatrix = np.hstack([prunedClauseMatrix,np.zeros((numClauses,1),dtype = int)])
    for clauseNum in range(numClauses):
        var1 = prunedClauseMatrix[clauseNum][0]
        var2 = prunedClauseMatrix[clauseNum][1]
        var1State = np.sign(var1)
        var2State = np.sign(var2)

        if (var1State == 1):
            if (var2State == 1):
                prunedClauseMatrix[clauseNum][2] = 1
            else:
                prunedClauseMatrix[clauseNum][1] = var2State*var2
                prunedClauseMatrix[clauseNum][2] = 2
        else:
            prunedClauseMatrix[clauseNum][0] = var1State*var1
            if (var2State == 1):
                prunedClauseMatrix[clauseNum][2] = 3
            else:
                prunedClauseMatrix[clauseNum][1] = var2State*var2
                prunedClauseMatrix[clauseNum][2] = 4

    return prunedClauseMatrix, numClauses

# Check to see how many clauses each assignment satisfies.
def makeHp(numParticles,numClauses):

    # Create list of assignments and clauses.
    clauseList = clauseListGen([0]*numClauses, numParticles)

    # Initialize vector for diagonal of Hp with each having no clauses satisfied.
    hp = [numClauses]*(2**numParticles)
    bestAssgs = [] #List to hold best assignments for given 2SAT problem instance.
    minNumClauseNotSat = numClauses

    # For each random assignment to test.
    for assgNum in range(2**numParticles):
        assg = bin(assgNum)[2:].zfill(numParticles) # Generate a new random assignment.

        # Test assignment against each clause.
        for clauseNum in range(numClauses):
            clauseSat = 0
            clausePartState = clauseList[clauseNum][2]         # Get state of particle in clause.

            if (clausePartState == 1):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 2):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '1'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1
            elif (clausePartState == 3):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '1'):
                    clauseSat = 1
            elif (clausePartState == 4):
                if ((assg[(clauseList[clauseNum][0]) - 1]) == '0'):
                    clauseSat = 1
                elif ((assg[(clauseList[clauseNum][1]) - 1]) == '0'):
                    clauseSat = 1

            # Put number of clauses unsatisfied by assignment, i, on Hp diagonal entry, i.
            # If assignment satisfied this clause decrease the count for how many clauses it does not satisfy.
            hp[assgNum] -= clauseSat

        # Keep record of assignment(s) satisfying the most clauses.
        # Number of clauses satisfied has been seen before.
        if (hp[assgNum]  == minNumClauseNotSat):
            bestAssgs = np.hstack([bestAssgs, assgNum + 1])
        # New "low score".
        elif (hp[assgNum]  < minNumClauseNotSat):
            bestAssgs = [(assgNum + 1)]
            minNumClauseNotSat = hp[assgNum]
    hp = sp.diags(hp,0)
    return hp, minNumClauseNotSat, bestAssgs

numClauses = 6
numParticles = 6
clauseList = clauseListGen([0]*numClauses, numParticles)
prunedClauseMatrix, newNumClauses = pruneClauseList(clauseList,numClauses,numParticles)
print '\nFinal clause matrix is:\n'+str(prunedClauseMatrix)
print '\nOld m = '+str(numClauses)+'\nNew m = '+str(newNumClauses)
hp, minNumClauseNotSat, bestAssgs = makeHp(numParticles,newNumClauses)

print'Hp:\n'+str(hp.toarray())+'\n\nSmallest # of clauses not satisfied = '+str(minNumClauseNotSat)+\
     '\n'+str(len(bestAssgs))+' Assignments had this #; they are:\n'+str(bestAssgs)