# Author: Kelsey C. Justis
# Date: 07/22/2015
# Summary: Program computes evolved state under adiabatic evolution.
# Input:
##- Hd (Matrix): Driver hamiltonian
##- Hp (Matrix): Problem hamiltonian
##- N (Integer): Number of steps 
## -T (Double time [seconds]): Final time
## -psiI (Vector): Initial state vector
##- H(t): Time function
#
# Output: 
##- psiT (Vector): Evolved state ket at time, T.

from numpy import *
import scipy.linalg as la
from makeHp import makeHp
from makeHd import makeHd
import math

# Get from user the number of particles, clauses, and random assignments to try.
numParticles = input('Enter number of particles/spins in the system: \n')
numClauses = input('Enter number of clauses to test: \n')

# Check to makes sure number of clauses is possible to make.
while (numClauses > (2*math.factorial(numParticles)/ math.factorial(numParticles - 2))):
    numClauses = input('***Number of clauses entered exceeds number of unique clauses***. \n'
                       'Enter a number less than 2*factorial(number of particles)/(factorial(number of particles - 2): \n')

# Get final time T to evolve state to.
T = input('Enter final time, T, for system to evolve to: \n')

# Get number of steps to evolve system in.
N = input('Enter number of steps, N, for system to evolve: \n')

# Calculate time between steps in simulated evolution.
delT = double(T)/double(N)

# Pauli matrices.
sigX = array([[0,1], [1,0]])
sigY = array([[0,-1j], [1j,0]])
sigZ = array([[1,0], [0,-1]])

# Define initial and problem Hamiltonians.        
Hd = makeHd(numParticles)
Hp = makeHp(numParticles,numClauses)[0]

# Define initial state.
initState = array([1,0,0,0])

# A typical Hamiltonian of the form H(t) = (1-s)Hd + sHp.
def Ht(currTime):
    s = double(currTime/T)
    print 'currS: ' + str(s)
    # print 'currtime: ' + str(currTime)
    # print 'delT: ' + str(delT)
    return (1 - s)*Hd + s*Hp

# Evolves Hamiltonian from initial Hd to H(T).
def evolveHam(initState):
    oldPsik = initState 
    
    # Evolve state.
    for k in range(1, N+1):
        # print 'k: '+ str(k)
        
        # Find H((k-1)*deltT) for next iteration.
        oldHk = Ht((k-1)*delT)
        # print 'H(t): \n' + str(oldHk)

        # print str(la.expm((-1j*delT)*oldHk))
        # Find psi(k*delT).
        psiK =  dot(la.expm((-1j*delT)*oldHk), oldPsik)    
        # print 'psi(t): \n' + str(psiK) + '\n'
        oldPsik = psiK
        
    return psiK

print 'Evolved state ket: \n' + str(evolveHam(initState))

print abs(dot(evolveHam(initState),initState))**2