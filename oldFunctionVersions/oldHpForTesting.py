# Author: Kelsey C. Justis
# Date: 07/22/2015
# Summary: Program creates Hp which has the number of clauses each assignment satisfied along the diagonal
# such that Hp(i,i) = numOfClausesSatisfied by assignment,i.
# Input: 
#-List of clauses
#-List of assignments
# Output:
#-Hp

import random
import numpy as np
import math

# Get from user the number of particles, clauses, and random assignments to try.
numParticles = input('Enter number of particles/spins in the system: \n')
numClauses = input('Enter number of clauses to test: \n')

# Check to makes sure number of clauses is possible to make.
while (numClauses > (2*math.factorial(numParticles)/ math.factorial(numParticles - 2))):
    numClauses = input('***Number of clauses entered exceeds number of unique clauses***. \n'
                       'Enter a number less than 2*factorial(number of particles)/(factorial(number of particles - 2): \n')
numAssgsToTest = 2**numParticles #input('Enter number of random assignments to test: \n')
alpha = numClauses/numParticles #input('Enter alpha: \n')
numVars = 2 # 2-Sat problem

# Create assignments to test.
def assgListGen(assgList, numParts):

    # If we have enough random assignments return them.
    for i in range(len(assgList)):
        # Generate a new random assignment.
        newAssg = str(int(bin(i)[2:])).zfill(numParts)
        assgList[i] = newAssg
    return assgList

# Create clauses to test.
def clauseListGen(clauseList, numParts):

     # Generate required number of clauses..
     for i in range(len(clauseList)):
          goodClause = 0

          # Generate a clause without redundancy/contradicting variables.
          while (goodClause == 0):
              clauseList[i] = ([((-1)**random.randint(0,1))*(random.randint(1,numParts)),\
                            ((-1)**random.randint(0,1))*(random.randint(1,numParts))])

              # Check if clause has duplicate state assignments to same particle .
              if (abs(clauseList[i][0])!=abs(clauseList[i][1])):

                 # Order particles in clause by number.
                 if (abs(clauseList[i][0]) > abs(clauseList[i][1])):
                     temp = clauseList[i][0]
                     clauseList[i][0] = clauseList[i][1]
                     clauseList[i][1] = temp

                     # Make sure clause is not already in list of clauses.
                     if ((clauseList[i] not in clauseList[0:i])):
                        goodClause = 1 # Clause makes sense.
     return clauseList

# Check to see how many clauses each assignment satisfies.
def makeHp(numParticles,numClauses):
    # Create list of assignments and clauses.
    assgList = assgListGen([0]*numAssgsToTest, numParticles)
    clauseList = clauseListGen([0]*numClauses, numParticles) #input('Enter clauses to check (ex: [(Clause 1 Variable i, ..., Clause 1 Variable N), ..., (Clause j Variable N)]): \n')
    hp = np.zeros((np.size(assgList,0),np.size(assgList,0)))
    bestAssgs = []
    maxNumClauseSat = 0

    # For each random assignment to test.
    for assgNum in range(numAssgsToTest):
        numClauseSat = int(0) # No clauses satisfied by this assignment yet.

        # Test assignment against each clause.
        for clauseNum in range(numClauses):
            j = 0 # Start with the first particle in both the clause and assignment
            clauseSat = 0 # Clause is not yet satisfied.

            # Check is the assignment satisfies the clause.
            while ((clauseSat == 0) and (j < numVars)):
                clausePartNum = clauseList[clauseNum][j]                      # Get particle number.
                assgPartState = int(assgList[assgNum][abs(clausePartNum) -1]) # Get assigned state of particle number.
                clausePartState = int((np.sign(clausePartNum)+1)/2)           # Get state of particle in clause.
                clauseSat = int(clausePartState == assgPartState)             # Check if particle in clause and assignment agree.
                j += 1                                                        # Move on to next particle.

            # If assignment satisfied this clause increase the count for how many clauses it satisfies.
            numClauseSat += clauseSat

        # Put number of clauses satisfied by assignment, i, on Hp diagonal entry, i.
        hp[assgNum, assgNum] = numClauseSat

        # Keep record of assignment(s) satisfying the most clauses.
        # Number of clauses satisfied has been seen before.
        if (numClauseSat == maxNumClauseSat):
            bestAssgs = np.hstack([bestAssgs, assgNum + 1])
        # New "high score".
        elif (numClauseSat > maxNumClauseSat):
            bestAssgs = [(assgNum + 1)]
            maxNumClauseSat = numClauseSat

    return hp, bestAssgs, maxNumClauseSat



# Give Hp which has the number of clauses each assignment satisfied along the diagonal such that Hp(i,i) = numOfClausesSatisfied by assignment,i.
results = makeHp(assgList, clauseList)
Hp = results[0]
bestAssgs = results[1]
maxNumClauseSat = results[2]

print '\nClauses tested are: \n' + str(clauseList) + '\n'
print 'Hp: \n' + str(Hp) + '\n'

# Print best assignments.
if (maxNumClauseSat== numClauses):
    print 'All clauses satisfied by assignment(s): \n' + str(bestAssgs)
else:
    print '2SAT has no solution here. \n' + str(maxNumClauseSat) + ' clauses satisfied by assignment(s): \n' + str(bestAssgs)


