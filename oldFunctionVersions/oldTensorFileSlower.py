# Author: Kelsey C. Justis
# Date: 07/07/2015
# Summary: Program computes tensor products.
# Input: list of multidimensional arrays in order of tensor operation.
# Output: Multidimensional array resulting from tensor product.

from numpy import *

# # Pauli matrices.
# sigX = array([[0,1], [1,0]])
# sigY = array([[0,-1j], [1j,0]])
# sigZ = array([[1,0], [0,-1]])
#
# # Play matrices.
# C = array([[3,0,0], [0,4,0], [0,0,5]])
# D = array([[1,0,0], [0,1,0], [0,0,1]])

# # Array of matrices to tensor together.
# tensorInput = [eye(2), sigX,eye(2)]

# Calculates tensor product between two input matrices.
def tensorProduct2(matrix1, matrix2):
    
    # Calculate each row of tensor product matrix.
    for row in range(size(matrix1,0)):
        
        # Calculate each entry in current row constructing.
        for col in range(size(matrix1,0)):
            
            # Calculate entry.
            currEntry = matrix1[row,col] * matrix2 
            
            # For first entry in row.
            if (col == 0):
                currRow = currEntry
                
            # Concatenate entries together to form current row.
            else:
                currRow = hstack([currRow, currEntry])
                
        # For first row of tensor product matrix.
        if (row == 0): 
            tensorProd = currRow
            
        # Concatenate rows to form required tensor product matrix.
        else:
            tensorProd = vstack([tensorProd, currRow])
    return tensorProd

# Calculates tensor product of input list of matrices in intended order to be tensored together.
def tensor(orderedListOfMatrices):

    tensorInput = orderedListOfMatrices

    # Calculate tensor expression result.
    for matrixNum in range(len(tensorInput)-1):

        # For first two terms tensored together.
        if (matrixNum == 0):
            tensorProd = tensorProduct2(tensorInput[0], tensorInput[1])

        # Tensor product for all subsequent terms.
        else:
            tensorProd = tensorProduct2(tensorProd, tensorInput[matrixNum+1])
        # print "Tensor Operation " + str(matrixNum+1)
        # print str(size(tensorProd,0)) + ' by ' + str(size(tensorProd,1)) + ' matrix'
        # print tensorProd
    return tensorProd
