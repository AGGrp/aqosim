# Author: Kelsey C. Justis
# Date: 07/21/2015
# Summary: Program constructs Hd given the number of particles in the system in consideration.
# Input:
#-n: number of particles/spins in system
#
# Output: 
#-Hd: driver Hamiltonian = (2^n)X(2^n) matrix (sum of sigma_x)^i

from numpy import*
from tensor import *

# Sigma x
sigX = array([[0,1], [1,0]])

def makeHd(n):

    # Initialize matrix for Hd
    hd = zeros((2**n,2**n), dtype = float)

    # Calculate Hd by summing up tensor acting on each particle.
    for i in range(1,n+1):

        # Calculate current tensor expression on ith particle.
        temp = tensor([eye(2**(i-1)), sigX, eye(2**(n-i))])

        # Add current tensor expression to Hd.
        hd = hd + temp
        #print 'i: \n' + str(i)
        #print 'temp: \n' + str(temp)
        #print 'Hd: \n' + str(hd)
    return hd

print 'Hd: \n' + str(makeHd(5))