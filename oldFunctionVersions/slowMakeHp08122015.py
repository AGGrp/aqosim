# Author: Kelsey C. Justis
# Date: 08/12/2015
# Summary: Program creates a sparse Hp matrix which has the number of clauses each assignment did not satisfy along the diagonal
# such that Hp(i,i) = number of clauses not satisfied by assignment,i.
# Input: 
#-n: integer number of particles
#-m: integer number of clauses
# Output:
#-Sparse Hp

import random
import numpy as np
import scipy.sparse as sp
import time
import math

# Create assignments to test.
def assgListGen(assgList, numParts):

    # When we have enough random assignments return them.
    for i in range(len(assgList)):
        assgList[i] = str(int(bin(i)[2:])).zfill(numParts) # Generate a new random assignment.
    return assgList

# Create clauses to test.
def clauseListGen(clauseList, numParts):

     # Generate required number of clauses.
     for i in range(len(clauseList)):
          goodClause = 0

          # Generate a clause without redundancy/contradicting variables.
          while (goodClause == 0):
              clauseList[i] = ([((-1)**random.randint(0,1))*(random.randint(1,numParts)),\
                            ((-1)**random.randint(0,1))*(random.randint(1,numParts))])

              # Check if clause has duplicate state assignments to same particle .
              if (abs(clauseList[i][0])!=abs(clauseList[i][1])):

                 # Make sure particles are ordered in clause by number.
                 if (abs(clauseList[i][0]) > abs(clauseList[i][1])):
                     temp = clauseList[i][0]
                     clauseList[i][0] = clauseList[i][1]
                     clauseList[i][1] = temp

                 # Make sure clause is not already in list of clauses.
                 if ((clauseList[i] not in clauseList[0:i])):
                    goodClause = 1 # Clause makes sense.
     return clauseList

# Check to see how many clauses each assignment satisfies.
def makeHp(numParticles,numClauses):
    assgtime = time.time()
    numPossAssgs = 2**numParticles

    # Create list of assignments and clauses.
    assgList = assgListGen([0]*numPossAssgs, numParticles)
    print("--- %s seconds for making assgList---" % (time.time() - assgtime))
    clausetime = time.time()
    clauseList = clauseListGen([0]*numClauses, numParticles)

    print("--- %s seconds for making clause list---" % (time.time() - clausetime))

    checktime = time.time()
    # Initialize vector for diagonal of Hp.
    hp = [0]*numPossAssgs
    bestAssgs = [] #List to hold best assignments for given 2SAT problem instance.
    minNumClauseNotSat = numClauses

    # For each random assignment to test.
    for assgNum in range(numPossAssgs):
        numClauseNotSat = numClauses # No clauses satisfied by this assignment yet.

        # Test assignment against each clause.
        for clauseNum in range(numClauses):
            particleNumber = 0 # Start with the first particle in both the clause and assignment
            clauseSat = 0 # Clause is not yet satisfied.

            # Check is the assignment satisfies the clause.
            while ((clauseSat == 0) and (particleNumber < 2)):
                clausePartNum = int(clauseList[clauseNum][particleNumber])                      # Get particle number.
                assgPartState = int(assgList[assgNum][abs(clausePartNum) - 1]) # Get assigned state of particle number.
                clausePartState = int((np.sign(clausePartNum) + 1)/2)           # Get state of particle in clause.
                clauseSat = int(clausePartState == assgPartState)             # Check if particle in clause and assignment agree.
                particleNumber += 1                                                        # Move on to next particle.

            # If assignment satisfied this clause decrease the count for how many clauses it does not satisfy.
            numClauseNotSat -= clauseSat

        # Put number of clauses unsatisfied by assignment, i, on Hp diagonal entry, i.
        hp[assgNum] = numClauseNotSat

        # Keep record of assignment(s) satisfying the most clauses.
        # Number of clauses satisfied has been seen before.
        if (numClauseNotSat == minNumClauseNotSat):
            bestAssgs = np.hstack([bestAssgs, assgNum + 1])
        # New "low score".
        elif (numClauseNotSat < minNumClauseNotSat):
            bestAssgs = [(assgNum + 1)]
            minNumClauseNotSat = numClauseNotSat
    hp = sp.diags(hp,0)

    print("--- %s seconds for assgClausecheck---" % (time.time() - checktime))
    return hp, minNumClauseNotSat, bestAssgs

for n in range(28,29):
    print '# Particles = ' + str(n)

    for m in range(1,2):
        print '\n# Clauses = ' + str(m)

        if (int(m) > (2*math.factorial(int(n))/ math.factorial(int(n) - 2))):
            break
        makeHp(n,m)