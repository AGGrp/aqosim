# Author: Kelsey C. Justis
# Date: 08/05/2015
# Summary: Program computes gMin for a given range of n, number of particles for all possible m clauses for each n.
# Then plots averaged tComp against alpha for all the Hp instances of the current alpha with any number of solutions.
# Input:
##- Minimum number of particles.
##- Maximum number of particles.
# Output:
##- Plot of average tComp for every possible value of alpha in the given n range.

import math
import time
import numpy as np
import scipy.linalg as la
from makeHp import makeHp
from makeHd import makeHd
import matplotlib.pyplot as plt
import scipy.sparse.linalg as spla
import matplotlib.cm as cm
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

# Gets minimum and maximum number of particles to plot gMin and tComp for.
def getUserInput():
    minNumParticles = input('Enter min number of particles/spins in the system to plot: \n')
    maxNumParticles = input('Enter min number of particles/spins in the system to plot: \n')
    return minNumParticles, maxNumParticles

# Calculates probability.
def calcProb(ket1,ket2):
    return (abs(la.dot(ket1,ket2)))**2

# Finds gMin for a given driver (Hd) and problem (Hp) Hamiltonians.
def getGMin(Hp, Hd):

    # Initialize Htotal to the driver Hamiltonian
    Htotal = Hd
    maxIter = (Hd.getnnz())*1000
    # Get initial eigenvalues for ground and excited state for the initial total Hamiltonian.
    eValGrnd, eValExc = np.sort(spla.eigsh(Htotal, 2, which = 'SA', return_eigenvectors = False, tol = 1E-6, maxiter = maxIter))

    # Set initial values for gMin search process.
    gMin  = float(abs(eValExc - eValGrnd)) # Get initial gap and use as gMin.
    perAccGmin = float(0.0001)        # Define accuracy desired as percent difference between sequential gMin values found.
    gMinNew = float(gMin)                   # Initialize the new gMin value to the initial gMin.
    gMinOld = float(gMinNew/perAccGmin)     # Initialize old gMin value to a gMin value far outside our desired range of agreement
                                        #  with the new gMin value. (Strictly to go in the while loop at least once.)

    # Initialize search grid over s.
    sMin = float(0)                  # Initial lower s to search from.
    sMax = float(1)                  # Initial higher s bound to search to.
    sDelta = float(0.1)             # Defines step between s values to search.
    sGMin = float(0.5)               # Set an initial s value for where gMin is located.

    # While the difference between subsequent gMin found is greater than our accuracy desired.
    while (abs((gMinOld - gMinNew)/gMinNew) >= perAccGmin):

        # Set our new old gMin value found to the previous iteration.
        gMinOld = gMinNew
        # print gMinOld
        # Set gMin to our best gMin value found so far.
        gMin = gMinNew

        # For each of the s values in the range of s around our suspected location of gMin.
        for s in np.arange(sMin,sMax + sDelta, sDelta):

           # Get Htotal for the current s value.
           Htotal = (1 - s)*Hd + s*Hp # A typical Hamiltonian of the form H(t) = (1-s)Hd + sHp.

           # Get eigen values of H(s) and find the gap between.
           eValGrnd, eValExc = np.sort(spla.eigsh(Htotal, 2, which = 'SA', return_eigenvectors = False, tol = 1E-6, maxiter = maxIter))
           gS = abs(eValExc - eValGrnd)

           # Check if this gap is the smallest found in this range.
           if (gS < gMin):

               # Assign a new gMin and its location in the range of s.
               gMin = gS
               sGMin = s

        # Update this iteration's discovered gMin.
        gMinNew = gMin

        # Update the range and spacing of s values to search over.
        sMin = sGMin - sDelta
        sMax = sGMin + sDelta

        if (sMax > 1.):
            sMax = 1.
        if (sMin < 0.):
            sMin = 0.
        sDelta = sDelta/10

    # Return the gMin found for this alpha.
    return gMin

# Plots gMin vs alpha for given range of particles.
def plotGminVsAlpha(minNumParticles,maxNumParticles):
    # Clear current figures.
    plt.cla()
    plt.close("all")

    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(abs(maxNumParticles - minNumParticles)+1)
    ys = [i+x+(i*x)**2 for i in range(abs(maxNumParticles - minNumParticles)+1)]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))

    # Create figure to plot gMin convergence.
    plt.hold(1)
    plt.figure(1)

    for numParticles in range(minNumParticles,maxNumParticles +1):

        # Get new pretty color for this number of particles for plotting purposes.
        newColor = next(colors)

        # Define initial and problem Hamiltonians.
        Hd = makeHd(numParticles)

        # Make arrays to hold values of alpha and tComp found.
        alphas = []
        tComps = []

        # Initialize number of clauses to check to 1.
        numClauses = 1

         # Maximum number of unique clauses for given n, num of particles
        maxNumClauses = (2*math.factorial(numParticles)/ math.factorial(numParticles - 2))

        # While we have not checked each possible m up to mMax.
        while (numClauses <= maxNumClauses):

            # Counter used to make sure enough instances are tried to ensure current alpha does not have an Hp with a unique solution.
            numInstancesTried = 1

            # Initialize average gMin for current alpha.
            avgGmin = 0.

            # Make sure we enough Hp for given alpha.
            while (numInstancesTried <= 500):

                # Create a new Hp with given number of particles and clauses.
                hpResults = makeHp(numParticles,numClauses)
                Hp = hpResults[0] # Get current Hp instance.
                gMin = getGMin(Hp,Hd) # Get gMin for current Hp instance.
                avgGmin += gMin # Add gMin to average.
                # Increase number of instances tried.
                numInstancesTried += 1

            # Get alpha and to array of alphas for current n to plot against.
            alpha = float(float(numClauses)/float(numParticles))
            alphas = np.hstack([alphas,alpha])

            # Find gMin and add to list of gMins for current n to plot against.
            avgGmin = float(avgGmin/numInstancesTried)
            avgTComp = avgGmin**(-2)
            tComps = np.hstack([tComps,avgTComp])

            # Print results of gMin and tComp for user.
            print '\n#Particles = ' + str(numParticles) + '\n#Clauses = ' + str(numClauses) + '\nAverage gMin is ' + str(avgGmin)
            print 'Average tComp is ' + str(avgTComp)

            # Go to next appropriate alpha in tests.
            numClauses +=1

        # Plot gMin vs. alpha for current value of n.
        plt.scatter(alphas,tComps, color=newColor) # Plot dots.
        plt.plot(alphas,tComps, color=newColor, label = '# Particles = ' + str(numParticles)) # Connect the dots.

    # Label plot for tComp vs. Alpha.
    fig = plt.figure(1)
    plt.title('tComp vs. Alpha')
    plt.xlabel('alpha')
    plt.ylabel('tComp (1/gmin^2)')
    plt.legend()

    # Make grid lines
    ax = fig.gca()

    # X-axis Grid.
    xMin, xMax = ax.get_xlim()
    minorXSpacing = abs(xMin-xMax)/10
    ax.xaxis.set_major_locator(MultipleLocator(1)) # larger x axis gris lines interval
    ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.xaxis.set_minor_locator(MultipleLocator(minorXSpacing)) # smaller x axis gris lines interval
    ax.xaxis.grid(True,'minor')
    ax.xaxis.grid(True,'major',linewidth=1)

    # Y-axis Grid.
    yMin, yMax = ax.get_ylim()
    minorYSpacing = abs(yMin-yMax)/10
    ax.yaxis.set_major_locator(MultipleLocator(0.1)) # smaller y axis gris lines interval
    ax.yaxis.set_minor_locator(MultipleLocator(minorYSpacing)) # larger y axis gris lines interval
    ax.yaxis.grid(True,'minor')
    ax.yaxis.grid(True,'major',linewidth=1)

    # Show plot.
    plt.show()

# Get range of particle numbers to test over.
minNumParticles,maxNumParticles = getUserInput()

# Start the clock to measure time of function.
start_time = time.time()

# Plot gMin vs alpha for request range on n.
plotGminVsAlpha(minNumParticles,maxNumParticles)

# Stop the clock and print elapsed time for program.
print("--- %s seconds ---" % (time.time() - start_time))
