# Author: Kelsey C. Justis
# Date: 07/27/2015
# Summary: Program computes tensor products.
# Input: list of sparse multidimensional arrays in order of tensor operation.
# Output: Multidimensional array resulting from tensor product.

from numpy import *
from scipy.sparse import kron

# Calculates tensor product of input list of sparse matrices in intended order to be tensored together.
def tensor(orderedListOfMatrices):

    # Calculate tensor expression result.
    for matrixNum in range(len(orderedListOfMatrices)-1):

        # For first two terms tensored together.
        if (matrixNum == 0):
            tensorProd = kron(orderedListOfMatrices[0], orderedListOfMatrices[1])

        # Tensor product for all subsequent terms.
        else:
            tensorProd = kron(tensorProd, orderedListOfMatrices[matrixNum+1])
    return tensorProd
