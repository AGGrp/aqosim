# Author: Kelsey C. Justis
# Date: 08/14/2015
# Summary: Program computes gMin for a given range of n, number of particles for all possible m clauses for each n.
# Then plots averaged tComp against alpha for all the Hp instances of the current alpha with a unique solution only.
# Input:
##- Minimum number of particles.
##- Maximum number of particles.
# Output: 
##- Plot of average tComp for alpha with unique solution Hps for every possible value of alpha in the given n range.

import math
import numpy as np
import scipy.linalg as la
from makeHp import makeHp
from makeHd import makeHd
import matplotlib.pyplot as plt
import scipy.sparse.linalg as spla
import matplotlib.cm as cm
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import cProfile
import pstats

# Gets minimum and maximum number of particles to plot gMin and tComp for.
def getUserInput():
    minNumParticles = input('Enter min number of particles/spins in the system to plot: \n')
    maxNumParticles = input('Enter max number of particles/spins in the system to plot: \n')
    return minNumParticles, maxNumParticles

# Calculates probability.
def calcProb(ket1,ket2):
    return (abs(la.dot(ket1,ket2)))**2

# Get gap for given Hamiltonian.
def getGS(Hamiltonian):
    numEvals = 0# Initialize the number of eigenvalues found to 0.
    maxIter = 1000000 # Initialize the maximum number of iterations the solver should go through to find the desired eigenvalues.
    tolerance = 1E-4 # Initialize the tolerance desired before eigenvalue solver stops iterating to find more accurate eigenvalue.
                     # "Relative accuracy for eigenvalues (stopping criterion). The default value of 0 implies machine precision."

    # Try the ARPACK sparse eigenvalue solver function until we have two "good" eigenvalues.
    while (numEvals < 2):

        # Try to use solver with current stopping criterion (iterations and tolerance).
        try:
            # Get eigenvalues for ground and first excited state for given Hamiltonian.
            eVal1, eVal2 = np.sort(spla.eigsh(Hamiltonian, 2, which = 'SA', return_eigenvectors = False, tol = tolerance, maxiter = maxIter))

            # If we have no eigenvalues yet and the eigenvalue function worked.
            if (numEvals == 0):
                eValGrnd = min(eVal1,eVal2) # Give the smaller of the given two from the eigenvalue function.
                eValExc = max(eVal1,eVal2)  # Give the greater of the given two from the eigenvalue function.

            # If we already have one eigenvalue and the eigenvalue function worked.
            elif(numEvals == 1):
                eValGrnd = min(eValGrnd,eVal1,eVal2) # Give the smaller of the previously found eigenvalue and the given two from the current eigenvalue function.
                eValExc = max(eValGrnd,eVal1,eVal2)  # Give the greater of the previously found eigenvalue and the given two from the current eigenvalue function.

            # Calculate the gap.
            gS = abs(eValExc - eValGrnd)

            # If the gap and the tolerance are within a magnitude of each other.
            if (gS/tolerance < 10.):
                tolerance = tolerance/10. # Make the tolerance smaller.
                numEvals = 0              # Recalculate eigenvalues with smaller tolerance.
            else:
                numEvals = 2  # We found eigenvalues with good enough precision.

        # For some reason the ARPACK eigenvalue solver function did not work.
        except spla.ArpackNoConvergence as err1:

            # Increase the number of iterations (time) it may take to try and find a "good" eigenvalue.
            maxIter = maxIter*10

            # If we the solver failed toprovide two eigenvalues but found one eigenvalue.
            if (len(err1.eigenvalues) == 1):

                # If we do not have any eigenvalues.
                if (numEvals == 0):
                    eValGrnd = err1.eigenvalues[0] # Use this eigenvalue as the lowest eigenvalue found.

                # If we already have an eigenvalue.
                elif(numEvals == 1):
                    eValGrnd = min(eValGrnd,err1.eigenvalues[0]) # Give the smaller of the previously found eigenvalue and the given one from the current eigenvalue function.
                    eValExc = max(eValGrnd,err1.eigenvalues[0]) # Give the greater of the previously found eigenvalue and the given one from the current eigenvalue function.

                # We found an eigenvalue.
                numEvals += 1

    # Return the gap found.
    return gS

# Finds gMin for a given driver (Hd) and problem (Hp) Hamiltonians.
def getGMin(Hp, Hd):

    # Set initial values for gMin search process.
    gMin  = getGS(Hd)            # Get initial gap and use as gMin.
    optTEvol = 0.                # Optimal evolution time.
    acc = 0.0001               # Define accuracy desired as percent difference between sequential gMin values found.
    prevgMin = gMin/acc # Initialize old gMin value to a gMin value far outside our desired range of agreement

    # Initialize search grid over s.
    sDelta = 0.1  # Defines step between s values to search.
    sGMin = 0.5   # Set an initial s value (in center) for where gMin is located.
    i = 0
    # While the difference between subsequent gMin found is greater than our accuracy desired.
    while (abs((prevgMin - gMin)/prevgMin) > acc) or (i<5):
        prevgMin = gMin # Record the old gMin that has been succeeded.
        # For each of the s values in the range of s around our suspected location of gMin.
        for s in np.arange(max((sGMin - sDelta*10),0),(min((sGMin + sDelta*10),1)) + sDelta, sDelta):
           gS = getGS(((1 - s)*Hd + s*Hp)) # Get gap for current s value.
           optTEvol += (sDelta)/(gS**2) # Update optTevol.

           # Check if this gap is the smallest found in this range.
           if (gS < gMin):

               # Update this iteration's discovered gMin and record its location in the range of s.
               gMin = gS
               sGMin = s
        sDelta = sDelta/10
        i +=1

    # Return the gMin and optimal evolution time found for this alpha.
    return gMin, optTEvol

# Plots gMin vs alpha for given range of particles.
def plotGminVsAlpha(minNumParticles,maxNumParticles):

    # Clear current figures and hold next drawings to come.
    plt.cla()
    plt.close("all")
    plt.hold(1)

    # Some stuff found online for creating random distinct and complementing colors for graphs.
    x = np.arange(abs(maxNumParticles - minNumParticles)+1)
    ys = [i+x+(i*x)**2 for i in range(abs(maxNumParticles - minNumParticles)+1)]
    colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))

    for numParticles in range(minNumParticles,maxNumParticles +1):

        # Get new pretty color for this number of particles for plotting purposes.
        newColor = next(colors)

        # Define initial and problem Hamiltonians.
        Hd = makeHd(numParticles)

        # Make arrays to hold values of alpha and tComp found.
        alphas = []
        tComps = []
        tCompVars = []
        optTComps = []
        optTCompVars = []

        # Initialize number of clauses to check past alpha =1 where there are no unique instances.
        numClauses = numParticles + 1

         # Maximum number of unique clauses for given n, num of particles
        maxNumClauses = (2*math.factorial(numParticles)/ math.factorial(numParticles - 2))
        alphaMax = (maxNumClauses)/(numParticles)

        # While we have not checked each possible m up to mMax.
        while (numClauses <= maxNumClauses):
            print '\n#Particles = ' + str(numParticles) + '\n#Clauses = ' + str(numClauses)

            # Counter used to make sure enough instances are tried to ensure current alpha does not have an Hp with a unique solution.
            numInstancesTried = 0

            # Initialize count of Hp with unique solution for current alpha.
            numUniHp = 0

            # Initialize array for holding gMins for current alpha.
            currGMins = []

            # Initialize array for holding optTEvols for current alpha.
            currOptTs = []

            # Make sure we enough Hp for given alpha.
            while ((numInstancesTried < 250) and (numUniHp < 30)):

                # Create a new Hp with given number of particles and clauses.
                hpResults = makeHp(numParticles,numClauses)

                # Get number of assignments with this number of unsatisfied assignments.
                numBestAssgs = len(hpResults[2])

                # If a unique solution Hp get gMin.
                if (numBestAssgs == 1):
                    Hp = hpResults[0] # Get current Hp instance.
                    gMin,optTEvol = getGMin(Hp,Hd) # Get gMin for current Hp instance.

                    # print 'gMin is '+ str(gMin)+' optT is '+str(optTEvol)
                    currGMins = np.hstack([currGMins,gMin])  # Add gMin to list of gMins for this alpha.
                    currOptTs = np.hstack([currOptTs,optTEvol]) # Add optimal evolution time to list of times for this alpha.
                    numUniHp += 1 # Increase count for number of Hp with unique solutions.
                # Increase number of instances tried.
                numInstancesTried += 1

            # If this is a good alpha record its results and plot them.
            if (numUniHp != 0):

                # Get alpha and to array of alphas for current n to plot against.
                alpha = float(numClauses)/numParticles
                alphas = np.hstack([alphas,alpha])

                # Find tComp and add to list of tComps for current n to plot against.
                avgGmin = (np.sum(currGMins))/(numUniHp) # Get average gMin.
                gMinVar = np.var(currGMins) # Get variance in gMin
                avgTComp = avgGmin**(-2) # Get average tComp.
                tCompVar = np.var(np.square(currGMins)) # Get variance in tComps.
                tComps = np.hstack([tComps,avgTComp])
                tCompVars = np.hstack([tCompVars,tCompVar])

                # Find avg optimal evolution time.
                avgOptTComp = (np.sum(currOptTs))/(numUniHp) # Get average optimal evolution time.
                optTCompVar = np.var(currOptTs) # Get variance in tComps.
                optTComps = np.hstack([optTComps,avgOptTComp])
                optTCompVars = np.hstack([optTCompVars,optTCompVar])

                # Print results of gMin and tComp for user.
                print 'Average gMin is ' + str(avgGmin) + ' with variance ' + str(gMinVar)
                print 'Average tComp is ' + str(avgTComp) + ' with variance ' + str(tCompVar)
                print 'Average optimal tEvol is ' + str(avgOptTComp) + ' with variance ' + str(optTCompVar)
                print 'Averaged ' + str(numUniHp) + ' Hp instances (with unique solutions) out of ' + str(numInstancesTried) + ' total instances tried.'
            else:
                print 'No Hp instances with unique solutions found out of ' + str(numInstancesTried) + ' total instances tried.'

            # Go to next appropriate alpha in tests.
            numClauses +=1

        # Plot against alpha.
        plt.figure(1)
        # Plot tComp vs. alpha for current value of n.
        plt.scatter(alphas,tComps, color=newColor) # Plot dots.
        plt.errorbar(alphas,tComps, yerr = 0.5*tCompVars, color=newColor, fmt ='o') # Plot variances.
        plt.plot(alphas,tComps, color=newColor,ls = '-.', label = 'tComp-# Particles = ' + str(numParticles)) # Connect the dots.

        # Plot optComp vs. alpha for current value of n.
        plt.scatter(alphas,optTComps, color=newColor) # Plot dots.
        plt.errorbar(alphas,optTComps, yerr = 0.5*optTCompVars, color=newColor,fmt ='o') # Plot variances.
        plt.plot(alphas,optTComps, color=newColor, label = 'optTComp-# Particles = ' + str(numParticles)) # Connect the dots.

        # Plot against scaled alpha.
        plt.figure(2)
        scaledAlphas = alphas/alphaMax

        # Plot tComp vs. alpha for current value of n.
        plt.scatter(scaledAlphas,tComps, color=newColor) # Plot dots.
        plt.errorbar(scaledAlphas,tComps, yerr = 0.5*tCompVars, color=newColor,fmt ='o') # Plot variances.
        plt.plot(scaledAlphas,tComps, color=newColor,ls = '-.', label = 'tComp-# Particles = ' + str(numParticles)) # Connect the dots.

        # Plot optComp vs. alpha for current value of n.
        plt.scatter(scaledAlphas,optTComps, color=newColor) # Plot dots.
        plt.errorbar(scaledAlphas,optTComps, yerr = 0.5*optTCompVars, color=newColor,fmt ='o') # Plot variances.
        plt.plot(scaledAlphas,optTComps, color=newColor, label = 'optTComp-# Particles = ' + str(numParticles)) # Connect the dots.

    # Label plot for tComp vs. Alpha.
    fig = plt.figure(1)
    plt.title('tComp vs. Alpha')
    plt.xlabel('alpha')
    plt.ylabel('tComp (1/gmin^2)')
    plt.legend()

    # Make grid lines
    ax = fig.gca()

    # X-axis Grid.
    xMin, xMax = ax.get_xlim()
    minorXSpacing = abs(xMin-xMax)/50
    ax.xaxis.set_major_locator(MultipleLocator(1)) # larger x axis gris lines interval
    ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.xaxis.set_minor_locator(MultipleLocator(minorXSpacing)) # smaller x axis gris lines interval
    ax.xaxis.grid(True,'minor')
    ax.xaxis.grid(True,'major',linewidth=2)

    # Y-axis Grid.
    yMin, yMax = ax.get_ylim()
    minorYSpacing = abs(yMin-yMax)/30
    ax.yaxis.set_major_locator(MultipleLocator(1)) # smaller y axis gris lines interval
    ax.yaxis.set_minor_locator(MultipleLocator(minorYSpacing)) # larger y axis gris lines interval
    ax.yaxis.grid(True,'minor')
    ax.yaxis.grid(True,'major',linewidth=2)

     # Label plot for tComp vs. scaledAlpha.
    fig = plt.figure(2)
    plt.title('tComp vs. scaledAlpha')
    plt.xlabel('alpha (scaled over alphaMax)')
    plt.ylabel('tComp (1/gmin^2)')
    plt.legend()

    # Make grid lines
    ax = fig.gca()

    # X-axis Grid.
    # xMin, xMax = ax.get_xlim()
    ax.xaxis.set_major_locator(MultipleLocator(0.1)) # larger x axis gris lines interval
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    ax.xaxis.set_minor_locator(MultipleLocator(0.01)) # smaller x axis gris lines interval
    ax.xaxis.grid(True,'minor')
    ax.xaxis.grid(True,'major',linewidth=2)

    # Y-axis Grid.
    yMin, yMax = ax.get_ylim()
    minorYSpacing = abs(yMin-yMax)/30
    ax.yaxis.set_major_locator(MultipleLocator(1)) # smaller y axis gris lines interval
    ax.yaxis.set_minor_locator(MultipleLocator(minorYSpacing)) # larger y axis gris lines interval
    ax.yaxis.grid(True,'minor')
    ax.yaxis.grid(True,'major',linewidth=2)

    # Show plot.
    plt.show()

# Get range of particle numbers to test over.
minNumParticles,maxNumParticles = getUserInput()

cProfile.run('plotGminVsAlpha(minNumParticles,maxNumParticles)','statixStats')
p = pstats.Stats('statixStats')
p.strip_dirs()
p.sort_stats('cumulative').print_stats()