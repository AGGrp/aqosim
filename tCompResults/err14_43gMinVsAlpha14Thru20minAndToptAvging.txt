/home/qmechanic/anaconda/bin/python /home/qmechanic/Documents/PyCode/aqosim/NEWESThamEvoSparseAvgGminUniSolHpOnly.py
Enter min number of particles/spins in the system to plot: 
14
Enter max number of particles/spins in the system to plot: 
20

#Particles = 14
#Clauses = 15
--- 55.3941028118 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 16
--- 58.0286900997 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 17
--- 60.8749058247 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 18
--- 63.7015049458 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 19
--- 66.6279320717 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 20
--- 32.7542779446 seconds for finding gMin---
Average gMin is 0.254719481137
Average tComp is 15.4125918303
Average optimal tEvol is 6.1237148861
Used 1 Hp instances with unique solutions to average out of 300 total instances tried.
--- 32.8299441338 seconds used calculating gMin---
--- 69.1378347874 seconds used to build Hp instances for this alpha iteration---
--- 101.968839884 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 21
--- 72.1069359779 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 22
--- 58.0665380955 seconds for finding gMin---
--- 34.9002919197 seconds for finding gMin---
Average gMin is 0.117897052001
Average tComp is 71.9439218231
Average optimal tEvol is 85.2262304256
Used 2 Hp instances with unique solutions to average out of 300 total instances tried.
--- 93.1331279278 seconds used calculating gMin---
--- 75.1839852333 seconds used to build Hp instances for this alpha iteration---
--- 168.318182945 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 23
--- 39.0355041027 seconds for finding gMin---
Average gMin is 0.0615895032478
Average tComp is 263.625002839
Average optimal tEvol is 34.2707198704
Used 1 Hp instances with unique solutions to average out of 300 total instances tried.
--- 39.1144869328 seconds used calculating gMin---
--- 78.2363169193 seconds used to build Hp instances for this alpha iteration---
--- 117.351877928 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 24
--- 29.2118480206 seconds for finding gMin---
--- 27.9000339508 seconds for finding gMin---
Average gMin is 0.207634579559
Average tComp is 23.1953342079
Average optimal tEvol is 8.36961133259
Used 2 Hp instances with unique solutions to average out of 300 total instances tried.
--- 57.2710239887 seconds used calculating gMin---
--- 81.084294796 seconds used to build Hp instances for this alpha iteration---
--- 138.356393099 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 25
--- 54.8603758812 seconds for finding gMin---
Average gMin is 0.0140399185206
Average tComp is 5073.0696527
Average optimal tEvol is 125.719837871
Used 1 Hp instances with unique solutions to average out of 300 total instances tried.
--- 54.9439079762 seconds used calculating gMin---
--- 83.9057934284 seconds used to build Hp instances for this alpha iteration---
--- 138.850769997 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 26
--- 37.7269649506 seconds for finding gMin---
Average gMin is 0.261673533031
Average tComp is 14.6042885029
Average optimal tEvol is 6.02326418045
Used 1 Hp instances with unique solutions to average out of 300 total instances tried.
--- 37.8044111729 seconds used calculating gMin---
--- 86.818924427 seconds used to build Hp instances for this alpha iteration---
--- 124.624432087 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 27
--- 29.0356490612 seconds for finding gMin---
--- 28.9011580944 seconds for finding gMin---
--- 41.8114449978 seconds for finding gMin---
--- 25.7414650917 seconds for finding gMin---
--- 28.9732549191 seconds for finding gMin---
--- 41.8210940361 seconds for finding gMin---
--- 38.83020401 seconds for finding gMin---
--- 41.3809249401 seconds for finding gMin---
--- 19.7800760269 seconds for finding gMin---
--- 18.7781820297 seconds for finding gMin---
--- 38.4603970051 seconds for finding gMin---
Average gMin is 0.186709838515
Average tComp is 28.6857093244
Average optimal tEvol is 13.6776811915
Used 11 Hp instances with unique solutions to average out of 300 total instances tried.
--- 354.418318272 seconds used calculating gMin---
--- 90.2017934322 seconds used to build Hp instances for this alpha iteration---
--- 444.621213913 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 28
--- 16.0637738705 seconds for finding gMin---
--- 31.4616761208 seconds for finding gMin---
--- 30.72706604 seconds for finding gMin---
--- 39.9158959389 seconds for finding gMin---
--- 28.438133955 seconds for finding gMin---
--- 36.2105779648 seconds for finding gMin---
--- 43.9601500034 seconds for finding gMin---
--- 19.2838728428 seconds for finding gMin---
--- 40.6737258434 seconds for finding gMin---
--- 27.0608081818 seconds for finding gMin---
--- 32.0240418911 seconds for finding gMin---
--- 22.3627040386 seconds for finding gMin---
Average gMin is 0.273988378721
Average tComp is 13.3209658513
Average optimal tEvol is 24.6856635193
Used 12 Hp instances with unique solutions to average out of 300 total instances tried.
--- 369.162323952 seconds used calculating gMin---
--- 92.8825945854 seconds used to build Hp instances for this alpha iteration---
--- 462.04607892 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 29
--- 44.0376811028 seconds for finding gMin---
--- 27.9507730007 seconds for finding gMin---
--- 12.9724960327 seconds for finding gMin---
--- 23.8149909973 seconds for finding gMin---
--- 37.4253749847 seconds for finding gMin---
--- 28.3093008995 seconds for finding gMin---
--- 25.5465600491 seconds for finding gMin---
--- 36.6968841553 seconds for finding gMin---
--- 46.799148798 seconds for finding gMin---
--- 30.3008351326 seconds for finding gMin---
--- 32.7445008755 seconds for finding gMin---
--- 40.2507920265 seconds for finding gMin---
--- 33.8203799725 seconds for finding gMin---
Average gMin is 0.224109503853
Average tComp is 19.9103755481
Average optimal tEvol is 101.618587553
Used 13 Hp instances with unique solutions to average out of 300 total instances tried.
--- 421.752203465 seconds used calculating gMin---
--- 96.0893304348 seconds used to build Hp instances for this alpha iteration---
--- 517.847988844 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 30
--- 34.2141721249 seconds for finding gMin---
--- 34.7761831284 seconds for finding gMin---
--- 30.9024310112 seconds for finding gMin---
--- 59.6206879616 seconds for finding gMin---
--- 27.1211299896 seconds for finding gMin---
--- 34.4522099495 seconds for finding gMin---
--- 38.4676060677 seconds for finding gMin---
--- 31.1407389641 seconds for finding gMin---
--- 12.6597349644 seconds for finding gMin---
--- 35.8107419014 seconds for finding gMin---
--- 36.500138998 seconds for finding gMin---
--- 31.5721008778 seconds for finding gMin---
--- 34.802752018 seconds for finding gMin---
--- 23.2720239162 seconds for finding gMin---
--- 27.7104449272 seconds for finding gMin---
--- 26.0192959309 seconds for finding gMin---
--- 32.3195707798 seconds for finding gMin---
Average gMin is 0.340261458024
Average tComp is 8.63722996418
Average optimal tEvol is 5.24912715915
Used 17 Hp instances with unique solutions to average out of 300 total instances tried.
--- 552.826849699 seconds used calculating gMin---
--- 99.7271294594 seconds used to build Hp instances for this alpha iteration---
--- 652.555152893 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 31
--- 36.6494660378 seconds for finding gMin---
--- 29.4854199886 seconds for finding gMin---
--- 28.4384379387 seconds for finding gMin---
--- 32.1412801743 seconds for finding gMin---
--- 24.3013100624 seconds for finding gMin---
--- 16.3674669266 seconds for finding gMin---
--- 28.3904149532 seconds for finding gMin---
--- 29.5595569611 seconds for finding gMin---
--- 23.3589141369 seconds for finding gMin---
--- 26.4538090229 seconds for finding gMin---
--- 30.0515079498 seconds for finding gMin---
--- 28.3197469711 seconds for finding gMin---
--- 31.6317028999 seconds for finding gMin---
--- 17.5025579929 seconds for finding gMin---
Average gMin is 0.271899274451
Average tComp is 13.5264522145
Average optimal tEvol is 7.94839495737
Used 14 Hp instances with unique solutions to average out of 300 total instances tried.
--- 383.827149153 seconds used calculating gMin---
--- 102.424993038 seconds used to build Hp instances for this alpha iteration---
--- 486.253303051 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 32
--- 32.5150139332 seconds for finding gMin---
--- 56.4924969673 seconds for finding gMin---
--- 19.8806159496 seconds for finding gMin---
--- 32.8380901814 seconds for finding gMin---
--- 34.5536789894 seconds for finding gMin---
--- 20.85729599 seconds for finding gMin---
--- 36.3199920654 seconds for finding gMin---
--- 33.338452816 seconds for finding gMin---
--- 34.4068250656 seconds for finding gMin---
--- 13.0320520401 seconds for finding gMin---
--- 29.865956068 seconds for finding gMin---
--- 28.6164388657 seconds for finding gMin---
--- 29.5889480114 seconds for finding gMin---
Average gMin is 0.289008103138
Average tComp is 11.9723653366
Average optimal tEvol is 9.1589568843
Used 13 Hp instances with unique solutions to average out of 300 total instances tried.
--- 403.400228262 seconds used calculating gMin---
--- 105.043757439 seconds used to build Hp instances for this alpha iteration---
--- 508.445162058 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 33
--- 32.1979739666 seconds for finding gMin---
--- 32.7880690098 seconds for finding gMin---
--- 54.5756480694 seconds for finding gMin---
--- 27.2047581673 seconds for finding gMin---
--- 27.1791629791 seconds for finding gMin---
--- 51.2439742088 seconds for finding gMin---
--- 37.3533859253 seconds for finding gMin---
--- 33.3176410198 seconds for finding gMin---
--- 14.4097192287 seconds for finding gMin---
--- 36.0859458447 seconds for finding gMin---
--- 23.2861409187 seconds for finding gMin---
--- 37.2933290005 seconds for finding gMin---
--- 18.1780431271 seconds for finding gMin---
--- 46.6960449219 seconds for finding gMin---
--- 61.2920701504 seconds for finding gMin---
Average gMin is 0.343280367912
Average tComp is 8.48598121462
Average optimal tEvol is 5.81157893597
Used 15 Hp instances with unique solutions to average out of 300 total instances tried.
--- 534.302284956 seconds used calculating gMin---
--- 107.928706884 seconds used to build Hp instances for this alpha iteration---
--- 642.232181072 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 34
--- 23.8714001179 seconds for finding gMin---
--- 34.8493058681 seconds for finding gMin---
--- 28.1374440193 seconds for finding gMin---
--- 32.1492660046 seconds for finding gMin---
--- 31.5945520401 seconds for finding gMin---
--- 39.0976719856 seconds for finding gMin---
--- 38.1622600555 seconds for finding gMin---
--- 38.8948850632 seconds for finding gMin---
--- 34.7965738773 seconds for finding gMin---
--- 25.2603509426 seconds for finding gMin---
--- 41.6475291252 seconds for finding gMin---
--- 31.1636440754 seconds for finding gMin---
--- 39.6248641014 seconds for finding gMin---
--- 32.8610749245 seconds for finding gMin---
--- 29.5770409107 seconds for finding gMin---
--- 25.756608963 seconds for finding gMin---
--- 38.5618741512 seconds for finding gMin---
--- 18.5765628815 seconds for finding gMin---
--- 32.7467839718 seconds for finding gMin---
--- 31.7988209724 seconds for finding gMin---
Average gMin is 0.267310036289
Average tComp is 13.9948893881
Average optimal tEvol is 14.4001704618
Used 20 Hp instances with unique solutions to average out of 300 total instances tried.
--- 650.762741566 seconds used calculating gMin---
--- 110.998138428 seconds used to build Hp instances for this alpha iteration---
--- 761.762031078 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 35
--- 42.7182021141 seconds for finding gMin---
--- 19.5189840794 seconds for finding gMin---
--- 41.8191859722 seconds for finding gMin---
--- 29.5862381458 seconds for finding gMin---
--- 25.6681349277 seconds for finding gMin---
--- 59.8785040379 seconds for finding gMin---
--- 31.0845148563 seconds for finding gMin---
--- 40.1535592079 seconds for finding gMin---
--- 25.2900519371 seconds for finding gMin---
--- 27.61876297 seconds for finding gMin---
--- 49.1871609688 seconds for finding gMin---
--- 26.6435120106 seconds for finding gMin---
--- 34.2208390236 seconds for finding gMin---
--- 24.5745940208 seconds for finding gMin---
--- 29.1882379055 seconds for finding gMin---
--- 19.8888530731 seconds for finding gMin---
--- 23.2754340172 seconds for finding gMin---
--- 17.6732859612 seconds for finding gMin---
Average gMin is 0.336537344081
Average tComp is 8.8294463904
Average optimal tEvol is 9.00513439324
Used 18 Hp instances with unique solutions to average out of 300 total instances tried.
--- 569.456428528 seconds used calculating gMin---
--- 114.138616085 seconds used to build Hp instances for this alpha iteration---
--- 683.596215963 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 36
--- 22.3690240383 seconds for finding gMin---
--- 38.1729590893 seconds for finding gMin---
--- 26.3761050701 seconds for finding gMin---
--- 18.2097029686 seconds for finding gMin---
--- 27.6685481071 seconds for finding gMin---
--- 48.1637868881 seconds for finding gMin---
--- 44.1665291786 seconds for finding gMin---
--- 28.0576269627 seconds for finding gMin---
--- 37.8050570488 seconds for finding gMin---
--- 29.7056891918 seconds for finding gMin---
--- 43.1187720299 seconds for finding gMin---
--- 23.4704759121 seconds for finding gMin---
--- 30.655148983 seconds for finding gMin---
--- 21.4400179386 seconds for finding gMin---
--- 24.3407189846 seconds for finding gMin---
--- 28.0651979446 seconds for finding gMin---
--- 35.8059859276 seconds for finding gMin---
--- 17.2435181141 seconds for finding gMin---
--- 23.7633321285 seconds for finding gMin---
--- 45.176970005 seconds for finding gMin---
--- 32.358620882 seconds for finding gMin---
--- 31.9132609367 seconds for finding gMin---
--- 25.1552619934 seconds for finding gMin---
--- 34.6035881042 seconds for finding gMin---
--- 45.1544771194 seconds for finding gMin---
Average gMin is 0.343869880449
Average tComp is 8.45691031015
Average optimal tEvol is 4.8053818107
Used 25 Hp instances with unique solutions to average out of 300 total instances tried.
--- 785.032094955 seconds used calculating gMin---
--- 116.862539053 seconds used to build Hp instances for this alpha iteration---
--- 901.895825863 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 37
--- 26.8780779839 seconds for finding gMin---
--- 35.5951621532 seconds for finding gMin---
--- 30.5851528645 seconds for finding gMin---
--- 31.1985299587 seconds for finding gMin---
--- 99.6022949219 seconds for finding gMin---
--- 30.6124310493 seconds for finding gMin---
--- 28.5593259335 seconds for finding gMin---
--- 31.7891569138 seconds for finding gMin---
--- 27.487224102 seconds for finding gMin---
--- 33.247961998 seconds for finding gMin---
--- 36.2755000591 seconds for finding gMin---
--- 28.2491378784 seconds for finding gMin---
--- 29.2536609173 seconds for finding gMin---
--- 32.5056731701 seconds for finding gMin---
--- 23.2383811474 seconds for finding gMin---
--- 32.7109849453 seconds for finding gMin---
--- 30.1947569847 seconds for finding gMin---
--- 72.9616639614 seconds for finding gMin---
--- 37.1541998386 seconds for finding gMin---
--- 48.7990078926 seconds for finding gMin---
--- 56.1340670586 seconds for finding gMin---
--- 48.4936151505 seconds for finding gMin---
--- 31.6281459332 seconds for finding gMin---
--- 26.1754128933 seconds for finding gMin---
--- 22.9355609417 seconds for finding gMin---
--- 44.8594079018 seconds for finding gMin---
--- 17.9239091873 seconds for finding gMin---
Average gMin is 0.283533044118
Average tComp is 12.4392053884
Average optimal tEvol is 29.8256238104
Used 27 Hp instances with unique solutions to average out of 300 total instances tried.
--- 997.218802452 seconds used calculating gMin---
--- 120.185797453 seconds used to build Hp instances for this alpha iteration---
--- 1117.40582085 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 38
--- 33.4193189144 seconds for finding gMin---
--- 21.7086751461 seconds for finding gMin---
--- 29.5516319275 seconds for finding gMin---
--- 23.9643521309 seconds for finding gMin---
--- 23.4993178844 seconds for finding gMin---
--- 25.0887610912 seconds for finding gMin---
--- 17.2984819412 seconds for finding gMin---
--- 30.0823860168 seconds for finding gMin---
--- 25.7478318214 seconds for finding gMin---
--- 34.5982999802 seconds for finding gMin---
--- 27.1357798576 seconds for finding gMin---
--- 29.3229999542 seconds for finding gMin---
--- 42.3432841301 seconds for finding gMin---
--- 24.8533520699 seconds for finding gMin---
--- 27.440279007 seconds for finding gMin---
--- 34.0692660809 seconds for finding gMin---
--- 32.9755620956 seconds for finding gMin---
--- 35.8431251049 seconds for finding gMin---
--- 39.1551399231 seconds for finding gMin---
--- 31.9388530254 seconds for finding gMin---
--- 25.2168419361 seconds for finding gMin---
--- 19.086853981 seconds for finding gMin---
--- 29.2351038456 seconds for finding gMin---
Average gMin is 0.387337871125
Average tComp is 6.66530577087
Average optimal tEvol is 4.36202503441
Used 23 Hp instances with unique solutions to average out of 300 total instances tried.
--- 665.436798811 seconds used calculating gMin---
--- 123.214678049 seconds used to build Hp instances for this alpha iteration---
--- 788.652685881 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 39
--- 42.7363929749 seconds for finding gMin---
--- 39.0427458286 seconds for finding gMin---
--- 17.6428029537 seconds for finding gMin---
--- 20.2914750576 seconds for finding gMin---
--- 28.3065450191 seconds for finding gMin---
--- 36.200772047 seconds for finding gMin---
--- 34.6866190434 seconds for finding gMin---
--- 29.8193099499 seconds for finding gMin---
--- 33.1127490997 seconds for finding gMin---
--- 47.5917980671 seconds for finding gMin---
--- 26.9652228355 seconds for finding gMin---
--- 28.2276849747 seconds for finding gMin---
--- 42.212515831 seconds for finding gMin---
--- 21.6532838345 seconds for finding gMin---
--- 32.5483291149 seconds for finding gMin---
--- 31.4518790245 seconds for finding gMin---
--- 28.2971968651 seconds for finding gMin---
--- 32.8622310162 seconds for finding gMin---
--- 23.9128680229 seconds for finding gMin---
--- 40.4621701241 seconds for finding gMin---
--- 39.2424869537 seconds for finding gMin---
--- 60.6671879292 seconds for finding gMin---
--- 32.1292829514 seconds for finding gMin---
--- 22.6758918762 seconds for finding gMin---
--- 28.3679227829 seconds for finding gMin---
--- 34.0879170895 seconds for finding gMin---
--- 19.8222789764 seconds for finding gMin---
--- 12.4050879478 seconds for finding gMin---
--- 21.9607579708 seconds for finding gMin---
--- 24.9078810215 seconds for finding gMin---
Average gMin is 0.280905086143
Average tComp is 12.6730396764
Average optimal tEvol is 9.84258329191
Used 30 Hp instances with unique solutions to average out of 299 total instances tried.
--- 936.742018461 seconds used calculating gMin---
--- 125.807589769 seconds used to build Hp instances for this alpha iteration---
--- 1062.56063795 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 40
--- 45.2515940666 seconds for finding gMin---
--- 28.9128611088 seconds for finding gMin---
--- 36.2096781731 seconds for finding gMin---
--- 41.4372661114 seconds for finding gMin---
--- 16.2820031643 seconds for finding gMin---
--- 20.6028420925 seconds for finding gMin---
--- 48.55291605 seconds for finding gMin---
--- 34.9784679413 seconds for finding gMin---
--- 39.6733939648 seconds for finding gMin---
--- 25.9476521015 seconds for finding gMin---
--- 26.5953638554 seconds for finding gMin---
--- 37.5672068596 seconds for finding gMin---
--- 40.522274971 seconds for finding gMin---
--- 27.6886529922 seconds for finding gMin---
--- 27.8633790016 seconds for finding gMin---
--- 25.6272740364 seconds for finding gMin---
--- 54.902351141 seconds for finding gMin---
--- 33.8081889153 seconds for finding gMin---
--- 29.0714828968 seconds for finding gMin---
--- 49.2906889915 seconds for finding gMin---
--- 29.7060499191 seconds for finding gMin---
--- 25.3092019558 seconds for finding gMin---
--- 25.3994848728 seconds for finding gMin---
--- 29.3282370567 seconds for finding gMin---
Average gMin is 0.297313500579
Average tComp is 11.3128164121
Average optimal tEvol is 6.72417067486
Used 24 Hp instances with unique solutions to average out of 300 total instances tried.
--- 802.486835241 seconds used calculating gMin---
--- 129.107407808 seconds used to build Hp instances for this alpha iteration---
--- 931.595499992 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 41
--- 24.4416270256 seconds for finding gMin---
--- 40.2828989029 seconds for finding gMin---
--- 32.6985971928 seconds for finding gMin---
--- 30.8291518688 seconds for finding gMin---
--- 30.2557280064 seconds for finding gMin---
--- 34.8414950371 seconds for finding gMin---
--- 30.5851151943 seconds for finding gMin---
--- 25.6239540577 seconds for finding gMin---
--- 30.7543299198 seconds for finding gMin---
--- 27.419344902 seconds for finding gMin---
--- 24.3746170998 seconds for finding gMin---
--- 41.5084431171 seconds for finding gMin---
--- 38.5511710644 seconds for finding gMin---
--- 25.2874898911 seconds for finding gMin---
--- 36.3978970051 seconds for finding gMin---
--- 26.9288539886 seconds for finding gMin---
--- 17.8177809715 seconds for finding gMin---
--- 14.1623311043 seconds for finding gMin---
--- 31.990844965 seconds for finding gMin---
--- 28.5388760567 seconds for finding gMin---
--- 27.4116928577 seconds for finding gMin---
--- 45.3734819889 seconds for finding gMin---
--- 39.8960402012 seconds for finding gMin---
--- 25.0419499874 seconds for finding gMin---
--- 44.4775660038 seconds for finding gMin---
--- 23.3861398697 seconds for finding gMin---
--- 39.9533560276 seconds for finding gMin---
--- 31.4969980717 seconds for finding gMin---
--- 30.6635148525 seconds for finding gMin---
--- 25.9213130474 seconds for finding gMin---
Average gMin is 0.310513987628
Average tComp is 10.3714066569
Average optimal tEvol is 10.165754854
Used 30 Hp instances with unique solutions to average out of 265 total instances tried.
--- 929.320623636 seconds used calculating gMin---
--- 116.370414257 seconds used to build Hp instances for this alpha iteration---
--- 1045.69222093 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 42
--- 31.062005043 seconds for finding gMin---
--- 26.0433249474 seconds for finding gMin---
--- 33.3886990547 seconds for finding gMin---
--- 34.105836153 seconds for finding gMin---
--- 42.1234631538 seconds for finding gMin---
--- 35.6952688694 seconds for finding gMin---
--- 26.5930421352 seconds for finding gMin---
--- 39.3648500443 seconds for finding gMin---
--- 30.9545018673 seconds for finding gMin---
--- 30.2025051117 seconds for finding gMin---
--- 22.8775119781 seconds for finding gMin---
--- 21.8845849037 seconds for finding gMin---
--- 40.6246709824 seconds for finding gMin---
--- 38.968462944 seconds for finding gMin---
--- 25.0791730881 seconds for finding gMin---
--- 32.678527832 seconds for finding gMin---
--- 21.99289608 seconds for finding gMin---
--- 27.0414750576 seconds for finding gMin---
--- 21.7634510994 seconds for finding gMin---
--- 31.316202879 seconds for finding gMin---
--- 24.3447599411 seconds for finding gMin---
--- 19.4237570763 seconds for finding gMin---
--- 21.9317390919 seconds for finding gMin---
--- 40.8184008598 seconds for finding gMin---
--- 22.443035841 seconds for finding gMin---
--- 27.6773130894 seconds for finding gMin---
--- 27.0799300671 seconds for finding gMin---
--- 50.2722349167 seconds for finding gMin---
--- 14.9427640438 seconds for finding gMin---
--- 23.0965850353 seconds for finding gMin---
Average gMin is 0.36727376423
Average tComp is 7.41344701135
Average optimal tEvol is 4.76842602426
Used 30 Hp instances with unique solutions to average out of 261 total instances tried.
--- 888.201626778 seconds used calculating gMin---
--- 117.77797389 seconds used to build Hp instances for this alpha iteration---
--- 1005.98073888 seconds to complete this alpha iteration---

#Particles = 14
#Clauses = 43
--- 23.7479629517 seconds for finding gMin---
--- 39.8398311138 seconds for finding gMin---
--- 23.0889849663 seconds for finding gMin---
--- 14.9275839329 seconds for finding gMin---
--- 22.9113018513 seconds for finding gMin---
convergence exception occurred and overrided
Traceback (most recent call last):
  File "/home/qmechanic/Documents/PyCode/aqosim/NEWESThamEvoSparseAvgGminUniSolHpOnly.py", line 301, in <module>
    plotGminVsAlpha(minNumParticles,maxNumParticles)
  File "/home/qmechanic/Documents/PyCode/aqosim/NEWESThamEvoSparseAvgGminUniSolHpOnly.py", line 179, in plotGminVsAlpha
    gMin,optTEvol = getGMin(Hp,Hd) # Get gMin for current Hp instance.
  File "/home/qmechanic/Documents/PyCode/aqosim/NEWESThamEvoSparseAvgGminUniSolHpOnly.py", line 82, in getGMin
    eValGrnd, eValExc = spla.ArpackNoConvergence.eigenvalues
AttributeError: type object 'ArpackNoConvergence' has no attribute 'eigenvalues'

Process finished with exit code 1

